#!/usr/bin/env python3
"""
Lab29 Objects 2
CS 210, Introduction to Programming
"""

__author__ = "Firstname Lastname"
__instructor__ = "Rank Lastname"
__date__ = "dd mmm yyyy"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
-	Read Lesson 29: Classes & Objects from our online textbook

Lesson Objectives
-	Enhance the simple classes with multiple attributes from previous lesson
-	Write and use a constructor with parameters
	Write and use additional class methods


"""

import math
import random
import turtle

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960  # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 // 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = WIDTH // 30  # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = MARGIN // 2  # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = False  # Set to True for fast, non-animated turtle movement.

COLORS = ["red", "green", "blue", "yellow", "cyan", "magenta", "white", "black"]


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    exercise0()
    exercise1()
    exercise2()


def exercise0():
    """
    Point Class Demo – Work with a partner to read, discuss, and understand the given code.
    Be sure to ask other classmates and/or your instructor if anything is unclear.

    a.	In the space "TODO 0a", is a definition of the Point class' __init__ method with
        two additional parameters for the x and y attributes.  Note the parameter names can
        be the same as the attribute names since they are distinguished by the object reference
        self, which is always the first parameter to a class method and is an explicit reference
        to the object itself.  Also note the parameters are given default values of zero.
        Read, discuss, and understand this code.

    b.	In the space "TODO 0b", is code that creates a list of Point objects, passing actual
        parameter values for the x and y attributes to the Point constructor.
        Read, discuss, and understand this code.

    c.	In the space "TODO 0c", is code that implements a draw method in the Point object.
        The method has two parameters; the first is self and is the explicit reference to the
        object itself, and the second is a turtle to be used to draw the Point object.
        Read, discuss, and understand this code.

    d.	In the space "TODO 0d", is code that loops through the previously created list of
        Point objects, calling the draw method of each.  Notice there is only one actual
        parameter in the line of code, p.draw( artist ).  The object instance p is implicitly
        passed as the first actual parameter and is assigned to the formal parameter self.
        Read, discuss, and understand this code.
    """
    print_exercise_name()

    # Create the turtle screen and two turtles (leave this as the first line).
    screen, artist, writer = turtle_setup()
    writer.write("Creating and drawing Point objects...",
                 align="center", font=("Times", FONT_SIZE, "bold"))

    # TODO 0b: Read, discuss, and understand the following code.
    points = []  # An empty list to be filled with Point objects.
    y = HEIGHT // 4  # Start the y-coordinate one-quarter screen height above the x-axis.
    # Loop through evenly spaced x-coordinates.
    for x in range(-WIDTH // 2 + MARGIN, WIDTH // 2 + MARGIN, (WIDTH - MARGIN * 2) // 8):
        p = Point(x, y)  # Use the values of x and y to create a Point object.
        points.append(p)  # Appends the point to the list of point objects.
        y *= -1  # Modify y so the points alternate above and below the x-axis.

    # TODO 0d: Read, discuss, and understand the following code.
    # Loop through the list of Point objects and tell each to draw itself.
    for p in points:
        print(p, flush=True)  # Print the Point object, which automatically calls the __str__ method.
        p.draw(artist)  # Tell the Point object to draw itself using the artist turtle.

    # Wait for the user to click before closing the window (leave this as the last line).
    artist.home()
    screen.exitonclick()


class Point:
    """Point class for representing (x,y) coordinates."""

    # TODO 0a: Read, discuss, and understand the following code.
    def __init__(self, x=0, y=0):
        """Create a new Point with the given x and y values.

        :param int x: The x-coordinate; default is zero.
        :param int y: The y-coordinate; default is zero.
        """
        # Assign the x and y values passed as parameters as attributes of self.
        self.x = x
        self.y = y
        self.size = 4

    def __str__(self):
        """Build and return a string representation of the object.

        :return: A string representation of this Point in the format "(x,y)".
        :rtype: str
        """
        return "({},{})".format(self.x, self.y)

    # TODO 0c: Read, discuss, and understand the following code.
    def draw(self, art):
        """Draw this Point object using the given turtle.

        :param turtle.Turtle art: The turtle to use to draw this Point object.
        :return: None
        """
        # Use the self object's x and y values to set the heading.
        art.setheading(art.towards(self.x, self.y))
        # Use the self object's x and y values to move the turtle.
        art.setposition(self.x, self.y)
        # Draw a dot at the point.
        art.dot(self.size)


def exercise1():
    """
    Spot Class – In this exercise you will enhance the Spot class from our previous lesson.

    a.	In the space "TODO 1a", define a Spot class with an __init__ method and a draw method.
        The __init__ method accepts parameters for the x, y, and color attributes and the draw
        method accepts a turtle as a parameter.  Both methods must have the explicit object
        reference self as the first parameter (the self parameter is not included in the
        docstring comment).

    b.	In the space "TODO 1b", write code that first creates a list of Spot objects, passing
        actual parameter values to the Spot constructor to produce the Spot objects shown in the
        image below. (Hint: Use the COLORS list defined at the top of the file.)  After creating
        the Spot objects, loop through the list calling the draw method of each object, passing
        the artist turtle.

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |  Creating and drawing Spot objects... |
            |                                       |
            |                                   o   |
            |                               o       |
            |                           o           |    (picture the circles with different colors)
            |                       o               |
            |                   o                   |
            |               o                       |
            |           o                           |
            |       o                               |
            |   o                                   |
            |                                       |
            +---------------------------------------+

c.	Finally, modify the draw method in the Spot class to add an artistic flair to the spots.
    One possibility is to have four solid circles drawn in a plus shape and a fifth circle traced
    on top with a different color.  Notice the encapsulation of the drawing of a Spot object
    in this one method; why is this good design?
    """
    print_exercise_name()

    # Create the turtle screen and two turtles (leave this as the first line).
    screen, artist, writer = turtle_setup()
    writer.write("Creating and drawing Spot objects...",
                 align="center", font=("Times", FONT_SIZE, "bold"))

    # TODO 1b: In the space below, use the class as described
    dx = (WIDTH - MARGIN * 2) // len(COLORS)
    dy = (HEIGHT - MARGIN * 2) // len(COLORS)
    x = -WIDTH // 2 + MARGIN + dx // 2
    y = -HEIGHT // 2 + MARGIN + dy // 2

    spots = []  # An empty list to be filled with Spot objects.
    for color in COLORS:
        s = Spot(x, y, color)  # Use the calculated values of x and y to create a Spot object.
        spots.append(s)  # Appends the point to the list of point objects.
        x += dx
        y += dy

    # Loop through the list of Point objects and tell each to draw itself.
    for s in spots:
        print(s, flush=True)  # Print the Spot object, which automatically calls the __str__ method.
        s.draw(artist)  # Tell the Spot object to draw itself using the artist turtle.

    # Wait for the user to click before closing the window (leave this as the last line).
    artist.color("black")
    artist.home()
    screen.exitonclick()


# TODO 1a: In the space below this comment, write the class as described
class Spot:
    """Spot class for representing a spot with (x,y) coordinates and color."""

    DEFAULT_SIZE = 32  # This is a class attribute, available to all instances.

    def __init__(self, x=0, y=0, c="black"):
        """Create a new Spot with the given x, y, and color values.

        :param int x: The x-coordinate; default is zero.
        :param int y: The y-coordinate; default is zero.
        :param str c: The color; default is black.
        """
        # Assign the x, y, and c values passed as parameters as attributes of self.
        self.x = x
        self.y = y
        self.c = c
        self.size = Spot.DEFAULT_SIZE

    def __str__(self):
        """Build and return a string representation of the object.

        :return: A string representation of this Spot in the format "(x,y):color".
        :rtype: str
        """
        return "({},{}):{}".format(self.x, self.y, self.c)

    def draw(self, art):
        """Draw this Spot object using the given turtle.

        :param turtle.Turtle art: The turtle to use to draw this Spot object.
        :return: None
        """
        # Use the self object's x and y values to set the heading and position, then draw a dot.
        art.setheading(art.towards(self.x, self.y))
        art.setposition(self.x, self.y)
        art.dot(self.size, self.c)

        # Move the turtle in each of the four directions and draw additional, overlapping dots.
        d = self.size // 2  # The amount to offset for each bit of flair.
        for dx, dy in [(-d, 0), (d, 0), (0, d), (0, -d)]:
            art.setposition(self.x + dx, self.y + dy)
            art.dot(self.size * 3 // 4, self.c)

        # Outline the center dot.
        art.color("black" if self.c not in ["black", "blue"] else "white")
        art.pendown()
        art.setheading(0)
        art.circle(self.size // 2)
        art.penup()


def exercise2():
    """
    Raindrop Class – In this exercise you will enhance the Raindrop class from our previous lesson.

    a.	In the space "TODO 2a", define a Raindrop class with an __init__ method that defines
        x and y attributes and also a radius attribute.  Use the same random values from our
        previous lab; the __init__ method in the Raindrop class does not need to accept additional
        parameters (i.e., Raindrops are still random).

        Include a draw method in the Raindrop class.  This method should accept one additional
        parameter, beyond the explicit self parameter, that is a turtle object to be used to
        draw the Raindrop.

        Include an area method in the Raindrop class.  This method does not accept any additional
        parameters beyond the explicit self parameter.  The method calculates and returns the area
        of the Raindrop object.

        Include an overlaps method in the Raindrop class.  This method accepts one additional
        parameter that is a second Raindrop object.  It is quite common to have a parameter that
        is a second instance of the same class; such a parameter is usually named other.  The function
        determines if the Raindrop object referenced by the explicit self parameter overlaps the
        Raindrop object referenced by the other parameter, returning True if it does, False if
        it does not.

        Include an expand method in the Raindrop class.  This method accepts two additional
        parameters; a second Raindrop object (named other) and a turtle to draw the expanded
        Raindrop.  The method updates the radius of the Raindrop object referenced by the explicit
        self parameter such that its area is increased by the area of the other object.  The method
        should then call the draw method of the Raindrop object referenced by the explicit self
        parameter, passing the turtle to do the drawing.

    b.	In the space "TODO 2b", re-write the same raindrop application from our previous lesson
        using the new methods in the Raindrop class.
    """
    print_exercise_name()

    # Create the turtle screen and two turtles (leave this as the first line).
    screen, artist, writer = turtle_setup()
    writer.write("Creating and drawing Raindrop objects...",
                 align="center", font=("Times", FONT_SIZE, "bold"))

    # Make the artist turtle a blue circle for this application.
    # artist.color( 'blue' )
    # artist.shape( "circle" )

    # TODO 2b: In the space below, use the class as described
    raindrops = []  # An empty list to be filled with Raindrop objects.

    # Continue until the total area of all Raindrop objects exceeds the area of the
    # screen (though the entire screen will not be covered as raindrops will overlap).
    total_area = 0.0
    while total_area < WIDTH * HEIGHT:
        # Create a new Raindrop object and tell it to draw itself using the artist turtle.
        new_drop = Raindrop()
        print("Drawing new drop {}.".format(new_drop), flush=True)
        new_drop.draw(artist)

        # Append the new Raindrop object to the list and update the total area.
        raindrops.append(new_drop)
        total_area += new_drop.area()

        # Loop through all of the old Raindrop objects looking for overlapping Raindrops.
        for old_drop in raindrops[:-1]:
            if new_drop.overlaps(old_drop):
                # Expand the old drop and add the new drop's area a second time.
                print("Expanding drop {}.".format(old_drop), flush=True)
                old_drop.expand(new_drop, artist)
                total_area += new_drop.area()

    # Wait for the user to click before closing the window (leave this as the last line).
    artist.home()
    screen.exitonclick()


# TODO 2a: In the space below this comment, write the class as described
class Raindrop:
    """Raindrop class for representing a drop with (x,y) coordinates and radius."""

    def __init__(self, x=None, y=None, r=None):
        """
        Create a new Raindrop with the given x, y, and radius values.

        :param int x: The x-coordinate; default is None in which case a random value is used.
        :param int y: The y-coordinate; default is None in which case a random value is used.
        :param int r: The radius; default is None in which case a random value is used.
        """
        # Set the radius of the raindrop first so it can be used when calculating x and y.
        self.r = r if r is not None else random.randint(MARGIN, MARGIN * 2)
        # Uses random.randint to create values within the margins of the turtle screen.
        self.x = x if x is not None else random.randint(-WIDTH // 2 + MARGIN + self.r, WIDTH // 2 - MARGIN - self.r)
        self.y = y if y is not None else random.randint(-HEIGHT // 2 + MARGIN + self.r, HEIGHT // 2 - MARGIN - self.r)

    def __str__(self):
        """
        Build and return a string representation of the object.

        :return: A string representation of this Raindrop in the format "(x,y):r".
        :rtype: str
        """
        return "({},{}):{:.2f}".format(self.x, self.y, self.r)

    def draw(self, art):
        """
        Draw this Raindrop object using the given turtle.

        :param turtle.Turtle art: The turtle to use to draw this Raindrop object.
        :return: None
        """
        # Use the self object's x and y values to set the heading and position, then draw a dot.
        art.setheading(art.towards(self.x, self.y))
        art.setposition(self.x, self.y)
        art.dot(self.r * 2, "blue")

    def area(self):
        """
        Calculate and return the area of this Raindrop object.

        :return: The area of this Raindrop object.
        :rtype: float
        """
        return math.pi * self.r ** 2

    def overlaps(self, other):
        """
        Determines if this Raindrop object overlaps another Raindrop object.

        :param Raindrop other: The other Raindrop object.
        :return: True if the Raindrop objects overlap; False otherwise.
        :rtype: bool
        """
        return math.hypot(self.x - other.x, self.y - other.y) <= self.r + other.r

    def expand(self, other, art):
        """
        Expands this Raindrop by adding the area of another Raindrop and draws the expansion with a turtle.

        :param Raindrop other: The other Raindrop object.
        :param turtle.Turtle art: The turtle to use to draw the Raindrop object.
        :return: None
        """
        # Update the radius and re-draw the Raindrop.
        self.r = math.sqrt(self.r ** 2 + other.r ** 2)
        self.draw(art)


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def turtle_setup():
    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    #  ___   ___     _  _  ___ _____    __  __  ___  ___ ___ _____   __
    # |   \ / _ \   | \| |/ _ \_   _|  |  \/  |/ _ \|   \_ _| __\ \ / /
    # | |) | (_) |  | .` | (_) || |    | |\/| | (_) | |) | || _| \ V /
    # |___/ \___/   |_|\_|\___/ |_|    |_|  |_|\___/|___/___|_|   |_|
    #  _____ _  _ ___ ___    ___ _   _ _  _  ___ _____ ___ ___  _  _
    # |_   _| || |_ _/ __|  | __| | | | \| |/ __|_   _|_ _/ _ \| \| |
    #   | | | __ || |\__ \  | _|| |_| | .` | (__  | |  | | (_) | .` |
    #   |_| |_||_|___|___/  |_|  \___/|_|\_|\___| |_| |___\___/|_|\_|
    #
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup(WIDTH, HEIGHT, MARGIN, MARGIN)
    screen.bgcolor("SkyBlue")

    # Create two turtles, one for drawing and one for writing.
    turtle.TurtleScreen._RUNNING = True  # Get around bug in v3.5.2 http://bugs.python.org/issue26571
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape("turtle")
    # Lift the artist's pen and slow it down to see the movements from object to object.
    artist.penup()
    artist.speed("slowest")

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay(0)
        artist.hideturtle()
        artist.speed("fastest")
        writer.hideturtle()
        writer.speed("fastest")

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading(90)  # Straight up, which makes it look sort of like a cursor.
    writer.penup()  # A turtle's pen does not have to be down to write text.
    writer.setposition(0, HEIGHT // 2 - FONT_SIZE * 2)  # Centered at top of the screen.

    return screen, artist, writer


def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
