#!/usr/bin/env python3
"""
Lab08 Functions
CS 210, Introduction to Programming
"""

__author__ = "Firstname Lastname"
__instructor__ = "Rank Lastname"
__date__ = "dd mmm yyyy"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
-	Read Lesson 8: Functions from our online textbook
-	Watch the embedded video on the Accumulator Pattern

Lesson Objectives
-	Reinforce functions, parameters, and return values
-	Introduce Program Decomposition and the Accumulator Pattern


"""

# Import specific functions from the math library, just to demonstrate how to do it.
from math import atan2, sqrt, degrees
import easygui
import turtle

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 640  # A smaller window for this problem.
HEIGHT = WIDTH  # A square window for this problem.
MARGIN = 32  # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = 16  # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = True  # Set to True for fast, non-animated turtle movement.


def main():
    """Main program to test solutions for each problem."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    exercise0()
    exercise1()
    exercise2()
    exercise3()


def exercise0():
    """Example code."""
    print_exercise_name()

    your_name = easygui.enterbox("What is your name?")  # For strings
    your_age = easygui.integerbox("What is your age?", "Age", 18, 1, 99)  # For integers, also with min/max
    prompt = "Welcome, {}.\nAt {} years old, you must feel old.".format(your_name, your_age)
    easygui.msgbox(prompt)


"""
Before You Begin

Exercise 00.

The first few exercises in this lab require writing functions
to perform calculations using the Accumulator Pattern.
Begin your work by using pencil and paper to calculate a
few simple results. It is always useful to have a few known
results when writing such functions.

First, calculate the sum of the odd numbers between 1 and 20,
inclusive. Next, calculate the sum of the even numbers between
1 and 20, inclusive. You will use these values in exercise 1.

The formulas for calculating the result of the summations below
have been proven correct by mathematicians.

    Sum of i:

     n                                         n(n+1)
     Σ  i = 1 + 2 + 3 + 4 + ... + (n-1) + n =  -------
    n=1                                           2

    Sum of i squared:

     n   2   2   2   2   2             2   2     n(n+1)(2n+1)
     Σ  i = 1 + 2 + 3 + 4 + ... + (n-1) + n   =  ------------
    n=1                                               6

    Sum of i cubed:
                                                       2              2
     n   3   3   3   3   3             3   3    (  n  )     ( n(n+1) )
     Σ  i = 1 + 2 + 3 + 4 + ... + (n-1) + n   = (  Σ  )  =  ( ------ )
    n=1                                         ( n=1 )     (    2   )


Calculate the result of each summation with n = 5. Do this by
writing out the summation and also calculating the result with
the formula. For example, the first summation would be
	1 + 2 + 3 + 4 + 5 = 15
and the result from the formula would be
	5 * ( 5 + 1 ) / 2 = 5 * 6 / 2 = 30 / 2 = 15

Do these calculation by hand for the second and third
summations above; you will use these values in exercise 2.
"""


def exercise1():
    """
    Odd and Even Sums exercise.

    a.	In the space "TODO 1a", write a function named sum_odds that receives
        an integer value as a parameter. The function should use the
        accumulator pattern to calculate and return the sum of the odd
        numbers between 1 and the given value, inclusive.

    b.	In the space "TODO 1b", write a function named sum_evens that receives
        an integer value as a parameter. The function should use the
        accumulator pattern to calculate and return the sum of the even
        numbers between 1 and the given value, inclusive.

    c.	In the space "TODO 1c", write code that uses the easygui.integerbox to
        obtain an integer value from the user. Use the value entered as the
        parameter to both the sum_odds and sum_evens functions and then display
        the results in an easygui.msgbox as shown:

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |          n = 20                       |
            |          sum of odds = 100            |
            |          sum of evens = 110           |
            |             +------+                  |
            |             |  OK  |                  |
            |             +------+                  |
            +---------------------------------------+

    """
    print_exercise_name()

    # TODO 1c: Write code to use the sum_odds and sum_evens functions
    n = easygui.integerbox("Exercise 1\nEnter n:", "Input", lowerbound=0, upperbound=2 ** 31)
    odd = sum_odds(n)
    even = sum_evens(n)
    easygui.msgbox("n = {}\nsum of odds = {}\nsum of evens = {}".format(n, odd, even))


# TODO 1a: In the space below, write the sum_odds function
def sum_odds(n):
    """
    Calculate and return the sum of the odd numbers between 1 and n, inclusive.

    :param int n: The upper bound of the series to sum.
    :return: The sum of the odd numbers between 1 and n, inclusive.
    :rtype: int
    """
    result = 0
    for value in range(1, n + 1, 2):
        result += value
    return result


# TODO 1b: In the space below, write the sum_evens function
def sum_evens(n):
    """
    Calculate and return the sum of the even numbers between 1 and n, inclusive.

    :param int n: The upper bound of the series to sum.
    :return: The sum of the even numbers between 1 and n, inclusive.
    :rtype: int
    """
    result = 0
    for value in range(2, n + 1, 2):
        result += value
    return result


def exercise2():
    """
    Write a function that sums up a series of integers raised to a power, and compare
    your result to the "shortcut" formula given earlier in this file.

    a.	In the space "TODO 2a", write a function named summation that receives
        two integer parameters, one specifying the value of n for the summation and
        the other specifying the exponent for the summation (the only difference between
        the summations is the exponent). The function should use the accumulator pattern
        (not the formula) to calculate and return the result of the summation.

    b.	In the space "TODO 2b", write code that uses the easygui.integerbox to obtain
        an integer value from the user to be used as the n parameter to your summation
        function. With this value, display three successive easygui.msgbox dialogs. An
        example of the first with exponent = 1 is shown below.

    For example, for the first series above with exponent 1 (no exponent, essentially)
    you might write code like this:

        n = easygui.integerbox("Enter n:", "Input", lowerbound=0, upperbound=2 ** 31)
        s = summation(n, 1)
        f = n * (n+1) // 2
        easygui.msgbox( "n = {}, summation( n, 1 ) = {}, formula result = {}".format( n, s, f ) )


            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |          n = 32                       |
            |          summation(n, 1) = 528        |
            |          formula result = 528         |
            |             +------+                  |
            |             |  OK  |                  |
            |             +------+                  |
            +---------------------------------------+
    """
    print_exercise_name()

    # TODO 2b: Write code to use the summation function
    n = easygui.integerbox("Exercise 2\nEnter n:", "Input", lowerbound=0, upperbound=2 ** 31)

    s = summation(n, 1)
    f = n * (n + 1) // 2
    easygui.msgbox("n = {}\nsummation(n, 1) = {}\nformula result = {}".format(n, s, f))

    s = summation(n, 2)
    f = n * (n + 1) * (2 * n + 1) // 6
    easygui.msgbox("n = {}\nsummation(n, 2) = {}\nformula result = {}".format(n, s, f))

    s = summation(n, 3)
    f = (n * (n + 1) // 2) ** 2
    easygui.msgbox("n = {}\nsummation(n, 3) = {}\nformula result = {}".format(n, s, f))


# TODO 2a: In the space below, write the summation function
def summation(n, exponent):
    """Calculation and return the summation of the series 1**exponent + 2**exponent + ... + n**exponent.

    :param int n: The upper bound of the series to sum.
    :param int exponent: The exponent for each term in the series.
    :return: The summation of the series.
    :rtype: int
    """
    result = 0
    for value in range(1, n + 1):
        result += value ** exponent
    return result


def exercise3():
    """
    You will need to retrieve the Lab08_exercise_3.pdf file for detailed instructions
    and detailed screenshots of the images you will be producing in this exercise.
    """
    print_exercise_name()

    # Create the turtle screen and two turtles (leave this as the first line).
    screen, artist, writer = turtle_setup()

    # TODO 3b: Write code to use the draw_inner_square function
    draw_square(artist, 200)
    draw_inner_square(artist, 200, 0.25)

    # Students will comment/un-comment each part; no need to pause.
    easygui.msgbox("Click OK to continue...")
    artist.clear()

    # TODO 3d: Write code to use the draw_inner_squares function
    draw_square(artist, 200)
    draw_inner_squares(artist, 200, 9)

    artist.penup()
    artist.setposition(-120, 0)
    artist.pendown()
    draw_square(artist, 100)
    draw_inner_squares(artist, 100, 4)

    artist.penup()
    artist.setposition(-120, -300)
    artist.pendown()
    draw_square(artist, 280)
    draw_inner_squares(artist, 280, 2)

    # Students will comment/un-comment each part; no need to pause.
    easygui.msgbox("Click OK to continue...")
    artist.clear()

    # TODO 3f: Write code to use the draw_art function
    draw_art(artist, WIDTH - MARGIN * 2)

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


# TODO 3a: In the space below, write the draw_inner_square function
def draw_inner_square(art, outer_size, percent):
    """Use the given turtle to draw the inner square as described in Lab 08, Problem 3, Part a.

    :param turtle.Turtle art: The turtle to do the drawing.
    :param int outer_size: The length of one side of the outer square.
    :param float percent: The percentage of one side of the outer square to place the corner of the inner square.
    """
    a = outer_size * percent
    b = outer_size - a
    alpha = degrees(atan2(a, b))
    inner_size = int(sqrt(a ** 2 + b ** 2))

    # Print some debugging information.
    # print( "a =", a )
    # print( "b =", b )
    # print( "alpha =", alpha )
    # print( "inner_size =", inner_size )
    # print( flush=True )

    # Move tom to a corner of the inner square.
    art.penup()
    art.forward(b)
    art.pendown()

    # Turn to the correct angle and draw the inner square.
    art.left(90 - alpha)
    draw_square(art, inner_size)

    # Put tom back in the previous location.
    art.penup()
    art.right(90 - alpha)
    art.back(b)
    art.pendown()


# TODO 3c: In the space below, write the draw_inner_squares function
def draw_inner_squares(art, outer_size, how_many):
    """Use the given turtle to draw multiple inner squares as described in Lab 08, Problem 3, Part b.

    :param turtle.Turtle art: The turtle to do the drawing.
    :param int outer_size: The length of one side of the outer square.
    :param int how_many: How many inner squares to draw.
    """
    for square in range(1, how_many + 1):
        draw_inner_square(art, outer_size, square / (how_many + 1))


# TODO 3e: In the space below, write the draw_art function
def draw_art(art, outer_size):
    """Use the given turtle to draw the modern art as described in Lab 08, Problem 3, Part c.

    :param turtle.Turtle art: The turtle to do the drawing.
    :param int outer_size: The length of one side of the entire image (which consists of four inner squares).
    """
    # I do not yet expect students to use a list in this manner, but soon! (Lesson 11)
    positions = [(0, 0), (0, -outer_size / 2), (-outer_size / 2, 0), (-outer_size / 2, -outer_size / 2)]
    for position in positions:
        art.penup()
        art.setposition(position)
        art.pendown()
        draw_square(art, outer_size / 2)
        draw_inner_squares(art, outer_size / 2, 14)


def draw_square(art, size):
    """Use the given turtle to draw a square with one corner at the turtle's current location.

    :param turtle.Turtle art: The turtle to do the drawing.
    :param int size: The length of one side of the square.
    """
    for _ in range(4):
        art.forward(size)
        art.left(90)


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def turtle_setup():
    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup(WIDTH, HEIGHT, MARGIN, MARGIN)
    screen.bgcolor("SkyBlue")

    # Create two turtles, one for drawing and one for writing.
    turtle.TurtleScreen._RUNNING = True  # Get around bug in v3.5.2 http://bugs.python.org/issue26571
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape("turtle")

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay(0)
        artist.hideturtle()
        artist.speed("fastest")
        writer.hideturtle()
        writer.speed("fastest")

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading(90)  # Straight up, which makes it look sort of like a cursor.
    writer.penup()  # A turtle's pen does not have to be down to write text.
    writer.setposition(0, HEIGHT // 2 - FONT_SIZE * 2)  # Centered at top of the screen.

    return screen, artist, writer


def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
