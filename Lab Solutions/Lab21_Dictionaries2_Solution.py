#!/usr/bin/env python3
"""
Lab 21 Dictionaries 2  Solution
CS 210, Introduction to Programming
"""
import csv
import json

import os
from pprint import pprint

__author__ = "Firstname Lastname"
__instructor__ = "Rank Lastname"
__date__ = "dd mmm yyyy"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
-	Read Lesson 21: Dictionaries from our online textbook

Lesson Objectives
-	Use dictionaries for information storage and retrieval.
-	Add, delete, modify, and test dictionary entries.
-	Iterate over dictionary keys, values, and/or items (key/value pairs).
"""


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    exercise0()
    exercise1()
    exercise2()


def exercise0():
    """
    Dictionaries can contain other dictionaries or even lists -- anything.
    Observe the structure of the 'courses' variable below.
    """
    print_exercise_name()

    c1 = {"course": "CS 110", "name": "Introduction to Programming"}
    c2 = {"course": "Math 472", "name": "Introduction to Number Theory"}
    c3 = {"course": "Chem 350", "name": "Chemistry of Weapons"}

    courses = dict()  # type: dict[str, list]  # Provide hints about 'courses'
    courses["f17"] = [c1, c2]
    courses["s18"] = [c3]
    courses["s18"]  # Type a period after the closing bracket -- see the hints

    print(courses)


def exercise1():
    """
    Load Comma Separated Values
    In the space "TODO 1a", complete the load_csv_file function such that it loads
    a Comma Separated Values (CSV) file from disk (use FakeCadets.txt as an example)
    and returns a list of dictionaries where each dictionary has keys that are the
    column headers (first line of the file) and the values are fields from each line.

    Looking at the FakeCadets.txt file, your list would begin like so:

    [ {'first_name': 'Anthony', 'last_name': 'Patterson', 'squadron': '10',
       'email': 'apatterson0@tamu.edu', 'gender': 'M', 'city': 'Phoenix', 'state': 'AZ'},
       {'first_name': 'Pamela', 'last_name': 'Marshall', 'squadron': '25',
       'email': 'pmarshall1@list-manage.com', .....

    Note: JSON stands for JavaScript Object Notation, and it is a standard way to
    transmit data on the world wide web.  Python dictionaries and lists complement
    JSON well, and you can output Python data in JSON form with the json.dumps(..)
    funciton.  You can also load JSON data with json.loads(..).
    """
    print_exercise_name()

    # Test the load_csv_file function with FakeCadets.txt
    filename = "../Data/FakeCadets.txt"
    data = load_csv_file(filename)
    print(json.dumps(data, indent=2))  # Comment this out after you have verified that group_by works

    # TODO 1b
    base, ext = os.path.splitext(filename)
    json_file = base + ".json"
    with open(json_file, "w") as f:
        f.write(json.dumps(data, indent=2))


def load_csv_file(filename):
    """
    Loads a comma separated values (CSV) file and returns a list with
    each line as a dictionary in the list.  The dictionary keys are the
    column headers, taken from the first line of the file.  The dictionary
    values are the field values from each line.

        Hint: You can either use split(",") to split each row on the commas
        or you can look up the csv module and use a csv.reader to parse
        the lines in a more robust fashion.  The FakeCadets.txt file is simple
        enough that it will parse OK with split(","), but more complex CSV
        files in your future may require csv.reader.

        https://docs.python.org/3/library/csv.html

    :param str filename: The file name of the CSV file
    :return: A list of dictionaries with key/value pairs from the file.
    :rtype: list[dict[str, str]]
    """

    # Start with empty list
    contents = []  # type: list[dict[str, str]]  # Detailed hints as to what is in 'contents'

    # TODO 1a: In the space below, complete the function as described in the lab document.

    # Version 1, using split(",")
    # with open(filename, "r") as f:
    #     lines = f.read().splitlines()
    #
    # headers = lines.pop(0).split(",")  # Keep the headers around
    #
    # for line in lines:
    #     fields = line.split(",")
    #     person = {}
    #     for i in range(len(headers)):
    #         person[headers[i]] = fields[i]
    #     contents.append(person)

    # Version 2, using csv.reader
    # with open(filename, "r") as f:
    #     csvreader = csv.reader(f)
    #
    #     headers = next(csvreader)
    #     for row in csvreader:
    #         person = {}
    #         for i in range(len(headers)):
    #             person[headers[i]] = row[i]
    #         contents.append(person)

    # Version3, use csv.DictReader
    with open(filename, "r") as f:
        csvdreader = csv.DictReader(f)
        for row in csvdreader:
            contents.append(row)

    return contents


def exercise2():
    """
    Group Data
    In the space "TODO 2a", complete the group_by function such that it receives
    a list of dictionaries (such as was created by load_csv_file) and groups the
    data based on a key provided.  See the group_by docstring for more information.

    In the space "TODO 2b", write the grouped data to a JSON file with the file
    extension .grouped.json

    In the space "TODO 2c", complete the exercise so that you load the FakeCadets.txt
    data, group it by state, and then print out a listing of each state and the total
    number of cadets in each state.  The beginning of your output should look like this:

        AK: 13
        AL: 78
        AR: 20
        AZ: 93
        CA: 435
        ...

    """
    print_exercise_name()

    # Test the load_csv_file function with FakeCadets.txt
    filename = "../Data/FakeCadets.txt"
    data = load_csv_file(filename)
    grouped = group_by(data, "state")
    # print(json.dumps(grouped, indent=2))  # Comment this out after you have verified that group_by works

    # TODO 2b
    base, ext = os.path.splitext(filename)
    json_file = base + ".grouped.json"
    with open(json_file, "w") as f:
        f.write(json.dumps(grouped, indent=2))

    # TODO 2c
    for k in sorted(grouped.keys()):
        v = grouped[k]
        print("{}: {}".format(k, len(v)))


def group_by(data, grouping_key):
    """
    Takes a list of dictionaries and groups the dictionaries based on the value
    of a particular key that they all share.  For example if you had a list of
    dictionaries where each dictionary had address information about an individual,
    you might group all the addresses by state.

    The returned data will be a dictionary where each key in the dictionary is
    the value that's being grouped, eg, CO, CA, NV, FL, etc.  The value stored
    associated with these keys is a list that contains all the dictionaries that
    match that grouping, eg, all the addresses in Colorado.  If a dictionary does
    not have an entry for the item in question, eg, state, then group it in a special
    entry called "UNKNOWN."

    Example:
    data = [ {"color": "blue", "type": "Corvette"},
             {"color": "red", "type": "Porsche"},
             {"color": "blue", "type": "F150"} ]
    grouped = group_by(data, "color")
    print(grouped)

    { "blue": [ {"color": "blue", "type": "Corvette"},
                {"color": "blue", "type": "F150"} ],
      "red": [ {"color": "red", "type": "Porsche"} ]
    }

    :param list[dict[str,str]] data: the source data
    :param str grouping_key: the key by which to group the original data
    :return: the grouped data
    :rtype: dict[str,list[dict[str,str]]]
    """
    grouped = {}  # type: dict[str, list[dict[str, str]]]  # Detailed hints as to what is in 'grouped'

    # TODO 2a
    for record in data:
        group_val = record[grouping_key]
        if group_val == "":
            group_val = "UNKNOWN"
        if group_val not in grouped:
            grouped[group_val] = []
        grouped[group_val].append(record)

    return grouped


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
