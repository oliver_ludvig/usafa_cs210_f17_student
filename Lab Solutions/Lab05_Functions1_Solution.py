#!/usr/bin/env python3
"""
Lab05 Functions with Turtle Graphics
CS 210, Introduction to Programming
"""

import math
import turtle

__author__ = "Firstname Lastname"
__instructor__ = "Rank Lastname"
__date__ = "dd mmm yyyy"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
- Read Lesson 5: Functions from our online textbook
- Watch the video on functions embedded in the above reading

Lesson Objectives
- Introduce functions and parameters
- Reinforce the range function and Python turtle graphics


"""

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960  # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 / 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = 32  # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = 16  # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = True  # Set to True for fast, stealthy turtles.


def main():
    """Main program to call each individual lab exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    exercise0()
    exercise1()
    exercise1_scaled()
    exercise2a()
    exercise2b()
    exercise2c()
    exercise3()
    exercise3_v2()
    exercise4()


def exercise0():
    """Example code."""
    print_exercise_name()

    # Create the turtle screen and two turtles.
    screen, artist, writer = turtle_setup()

    # Use the writer turtle to display a message at the top of the window.
    writer.write("Draw Eyes", align="center", font=("Arial", FONT_SIZE, "bold"))

    # Draw dots like eyes
    draw_dot(artist, -50, 0, 100, "white")  # Left eye
    draw_dot(artist, -50, -20, 30, "black")  # Left pupil
    draw_dot(artist, +50, 0, 100, "white")  # Right eye
    draw_dot(artist, +50, -20, 30, "black")  # Right pupil

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


def draw_dot(tom, x, y, size, color):
    """
    Use the given turtle to draw a dot at a coordinate.

    Note that the parameter named "tom" does not need to have the same name
    as what is passed in from elsewhere in the program ("artist" in this case).

    :param turtle.Turtle tom: The turtle to do the drawing.
    :param int x: The x-coordinate of the center of the dot.
    :param int y: The y-coordinate of the center of the dot.
    :param int size: The radius of the dot.
    :param str color: The color of the dot.
    """
    # Lift the pen and move to the indicated position.
    tom.penup()
    tom.setposition(x, y)
    tom.pendown()

    # Draw the dot.
    tom.dot(size, color)


def exercise1():
    """
    Use the draw_square function to draw the image shown below,
    which is made entirely of squares.

    a. Only draw the squares; do not write the numbers.

    b. The size of each square relative to the others should be as
       indicated by the numbers.  The small squares with no number are a 1.

    c. The exact placement of the image on the screen does not matter.

    d. The squares of size one do not need to be drawn as they will be
       completed by the drawing of other squares.


    + - - - - - - + - - - - + - - - - - +
    |             |         |           |
    |             |    4    |           |
    |      6      |         |     5     |
    |             + - - - + +           |
    |             |       + + - - - - - +
    + - - - - - + +   3   |             |
    |           + + - - - +             |
    |     5     |         |      6      |
    |           |    4    |             |
    |           |         |             |
    + - - - - - - + - - - - + - - - - - +
    """
    print_exercise_name()

    # Create the turtle screen and two turtles.
    screen, artist, writer = turtle_setup()

    # Use the writer turtle to display a message at the top of the window.
    writer.write("Square of Squares", align="center", font=("Arial", FONT_SIZE, "bold"))

    # TODO 1: Delete the following two lines and draw the specified image using the draw_square function.
    # Draw the image with the lower-left corner at (0,0) and dimensions scaled by 10.
    # Note: The squares of size one are completed by the drawing of other squares.
    draw_square(artist, 0, 0, 50)
    draw_square(artist, 50, 0, 40)
    draw_square(artist, 90, 0, 60)
    draw_square(artist, 0, 50, 60)
    draw_square(artist, 60, 70, 40)
    draw_square(artist, 100, 60, 50)
    draw_square(artist, 60, 40, 30)

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


def draw_square(tom, x, y, size):
    """
    Use the given turtle to draw a square with one corner at coordinate (x,y).

    Note: The orientation of the square is dependent on the heading of the turtle.
    If the turtle's heading is zero, the (x,y) coordinate is the lower-left corner.

    :param turtle.Turtle tom: The turtle to do the drawing.
    :param int x: The x-coordinate of one corner of the square.
    :param int y: The y-coordinate of one corner of the square.
    :param int size: The length of one side of the square.
    """
    # Lift the pen and move to the indicated position.
    tom.penup()
    tom.setposition(x, y)
    tom.pendown()

    # Draw the square.
    for _ in range(4):
        tom.forward(size)
        tom.left(90)


def exercise1_scaled():
    """Use the draw_square function to draw a larger square of smaller squares."""
    print_exercise_name()

    # Create the turtle screen and two turtles.
    screen, artist, writer = turtle_setup()

    # Use the writer turtle to display a message at the top of the window.
    writer.write("Square of Squares", align="center", font=("Arial", FONT_SIZE, "bold"))

    # Use scale and delta variables to move the drawing to the center of the window.
    scale = 40
    dx = 15 * scale // 2
    dy = 11 * scale // 2

    # Note: The squares of size one are completed by the drawing of other squares.
    draw_square(artist, 0 - dx, 0 - dy, 5 * scale)
    draw_square(artist, 5 * scale - dx, 0 - dy, 4 * scale)
    draw_square(artist, 9 * scale - dx, 0 - dy, 6 * scale)
    draw_square(artist, 0 - dx, 5 * scale - dy, 6 * scale)
    draw_square(artist, 6 * scale - dx, 7 * scale - dy, 4 * scale)
    draw_square(artist, 10 * scale - dx, 6 * scale - dy, 5 * scale)
    draw_square(artist, 6 * scale - dx, 4 * scale - dy, 3 * scale)

    # Draw the numbers in each square; again just for the screen capture.
    writer.setposition(0 - dx + 5 * scale // 2, 0 - dy + 5 * scale // 2 - FONT_SIZE)
    writer.write("5", align="center", font=("Arial", FONT_SIZE, "bold"))
    writer.setposition(5 * scale - dx + 4 * scale // 2, 0 - dy + 4 * scale // 2 - FONT_SIZE)
    writer.write("4", align="center", font=("Arial", FONT_SIZE, "bold"))
    writer.setposition(9 * scale - dx + 6 * scale // 2, 0 - dy + 6 * scale // 2 - FONT_SIZE)
    writer.write("6", align="center", font=("Arial", FONT_SIZE, "bold"))
    writer.setposition(0 - dx + 6 * scale // 2, 5 * scale - dy + 6 * scale // 2 - FONT_SIZE)
    writer.write("6", align="center", font=("Arial", FONT_SIZE, "bold"))
    writer.setposition(6 * scale - dx + 4 * scale // 2, 7 * scale - dy + 4 * scale // 2 - FONT_SIZE)
    writer.write("4", align="center", font=("Arial", FONT_SIZE, "bold"))
    writer.setposition(10 * scale - dx + 5 * scale // 2, 6 * scale - dy + 5 * scale // 2 - FONT_SIZE)
    writer.write("5", align="center", font=("Arial", FONT_SIZE, "bold"))
    writer.setposition(6 * scale - dx + 3 * scale // 2, 4 * scale - dy + 3 * scale // 2 - FONT_SIZE)
    writer.write("3", align="center", font=("Arial", FONT_SIZE, "bold"))

    # A little hacking on the font size of these last two since they are smaller.
    writer.setposition(5 * scale - dx + 1 * scale // 2, 4 * scale - dy + 1 * scale // 2 - FONT_SIZE + 2)
    writer.write("1", align="center", font=("Arial", FONT_SIZE - 4, "bold"))
    writer.setposition(9 * scale - dx + 1 * scale // 2, 6 * scale - dy + 1 * scale // 2 - FONT_SIZE + 2)
    writer.write("1", align="center", font=("Arial", FONT_SIZE - 4, "bold"))

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


def exercise2_bad():
    """Use the draw_square function to draw seven squares across the screen."""
    print_exercise_name()

    # Create the turtle screen and two turtles.
    screen, artist, writer = turtle_setup()

    # Use the writer turtle to display a message at the top of the window.
    writer.write("Seven Squares", align="center", font=("Arial", FONT_SIZE, "bold"))

    # Draw seven squares of 50 pixels each with 50 pixels in between.
    for _ in range(7):
        # Draw the first square at (-350, 0) with size 50.
        draw_square(artist, -350, 0, 50)
        # Move the turtle so it's ready to draw the next square.
        artist.penup()
        artist.forward(100)  # Move 50 to pass the square drawn and 50 for the blank space between.
        artist.pendown()

        # Q: Why doesn't this work?
        # A: Because the draw_square function doesn't have a memory of the previous (x,y) coordinate.


def exercise2a():
    """
    Use the draw_square function to draw a series of seven squares
    equally spaced across the screen, as shown in the image at right.

    a. The space between squares is the same as the size of a square.

    b. The exact size of the squares does not matter.

    c. The exact placement of the image on the screen does not matter.

    +---+    +---+    +---+    +---+    +---+    +---+    +---+
    |   |    |   |    |   |    |   |    |   |    |   |    |   |
    +---+    +---+    +---+    +---+    +---+    +---+    +---+
    """
    print_exercise_name()

    # Create the turtle screen and two turtles.
    screen, artist, writer = turtle_setup()

    # Use the writer turtle to display a message at the top of the window.
    writer.write("Seven Squares", align="center", font=("Arial", FONT_SIZE, "bold"))

    # Set the size of each square and the x-coordinate of the first square.
    size = 32
    x = -size * 7  # Q: Why 7?  A: Three squares plus three spaces left of zero, plus one to center things.

    # Draw the squares.
    for _ in range(7):
        # Make the y-coordinate negative so the squares are centered.
        draw_square(artist, x, -size // 2, size)
        # Move the x-coordinate for the next square.
        x += size * 2

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


def exercise2b():
    """
    Use the draw_square function to draw a series of seven squares
    equally spaced across the screen, as shown in the image at right.

    a. The space between squares is the same as the size of a square.

    b. The exact size of the squares does not matter.

    c. The exact placement of the image on the screen does not matter.

    +---+    +---+    +---+    +---+    +---+    +---+    +---+
    |   |    |   |    |   |    |   |    |   |    |   |    |   |
    +---+    +---+    +---+    +---+    +---+    +---+    +---+
    """
    print_exercise_name()

    # Create the turtle screen and two turtles.
    screen, artist, writer = turtle_setup()

    # Use the writer turtle to display a message at the top of the window.
    writer.write("Seven Squares", align="center", font=("Arial", FONT_SIZE, "bold"))

    # Set the size of each square.
    size = 48

    # Draw the squares using the range to set the x-coordinates.
    for x in range(-size * 7, size * 7, size * 2):
        # Make the y-coordinate negative so the squares are centered.
        draw_square(artist, x, -size // 2, size)

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


def exercise2c():
    """
    Use the draw_square function to draw a series of seven squares
    equally spaced across the screen, as shown in the image at right.

    a. The space between squares is the same as the size of a square.

    b. The exact size of the squares does not matter.

    c. The exact placement of the image on the screen does not matter.

    +---+    +---+    +---+    +---+    +---+    +---+    +---+
    |   |    |   |    |   |    |   |    |   |    |   |    |   |
    +---+    +---+    +---+    +---+    +---+    +---+    +---+
    """
    print_exercise_name()

    # Create the turtle screen and two turtles.
    screen, artist, writer = turtle_setup()

    # Use the writer turtle to display a message at the top of the window.
    writer.write("Seven Squares", align="center", font=("Arial", FONT_SIZE, "bold"))

    # Calculate the size of each square, with a MARGIN on each side of the outer squares.
    size = (WIDTH - MARGIN * 2) // 13  # Q: Why 13?  A: Seven squares plus six spaces.

    # Draw the squares using the range to set the x-coordinates.
    for x in range(-WIDTH // 2 + MARGIN, WIDTH // 2 - MARGIN, size * 2):
        # Make the y-coordinate negative so the squares are centered.
        draw_square(artist, x, -size // 2, size)

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


def exercise3():
    """
    Create a draw_triangle function and use it to draw a hexagram,
    as shown in this example: http://www.wpclipart.com/education/geometry/hexagram.png

    a. Your draw_triangle function should have the same four parameters
       as the draw_square function. (In fact, you may wish to copy and
       paste the draw_square function, but be sure you change the docstring
       comment appropriately!)

    b. The provided code introduces the turtle screen's numinput function
       to obtain user input in a Python turtle graphics program. Read the
       comment carefully to understand the parameters given to the function,
       asking questions if necessary.

    c. You will need to calculate the height of the equilateral triangle in
       order to properly position the two triangles. This can be done using the
       Pythagorean Theorem.
    """
    print_exercise_name()

    # Create the turtle screen and two turtles.
    screen, artist, writer = turtle_setup()

    # Use the writer turtle to display a message at the top of the window.
    writer.write("Hexagram", align="center", font=("Arial", FONT_SIZE, "bold"))

    # Prompt the user for the size of the triangle.
    size = int(screen.numinput("Input", "Enter size of each triangle:", 128, 2, 256))

    # TODO 3b: Use the draw_triangle function to draw the hexagram.
    # Be sure to do TODO 3a first (below)

    # Calculate the height of the equilateral triangle.
    height = int(math.sqrt(size ** 2 - (size // 2) ** 2))

    # Draw the first triangle, which will be pointing up.
    draw_triangle(artist, -size // 2, -height // 3, size)
    # Turn the turtle so the second triangle will be pointing down.
    artist.setheading(180)
    # Draw the second triangle, which will be pointing down.
    draw_triangle(artist, size // 2, height // 3, size)

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


# TODO 3a: Define a draw_triangle function below this comment, as specified in the lab document.

def draw_triangle(tom, x, y, size):
    """Use the given turtle to draw a triangle with one corner at coordinate (x,y).

    Note: The orientation of the triangle is dependent on the heading of the turtle.
    If the turtle's heading is zero, the (x,y) coordinate is the lower-left corner.

    :param turtle.Turtle tom: The turtle to do the drawing.
    :param int x: The x-coordinate of one corner of the triangle.
    :param int y: The y-coordinate of one corner of the triangle.
    :param int size: The length of one side of the triangle.
    """
    # Lift the pen and move to the indicated position.
    tom.penup()
    tom.setposition(x, y)
    tom.pendown()

    # Draw the triangle.
    for _ in range(3):
        tom.forward(size)
        tom.left(120)


# Below is an alternate version of Exercise 3 with the turtle's heading
# as an additional parameter to the draw_triangle function.

def exercise3_v2():
    """Use the draw_triangle function to draw a hexagram.

    http://www.wpclipart.com/education/geometry/hexagram.png
    """
    print_exercise_name()

    # Create the turtle screen and two turtles.
    screen, artist, writer = turtle_setup()

    # Use the writer turtle to display a message at the top of the window.
    writer.write("Hexagram", align="center", font=("Arial", FONT_SIZE, "bold"))

    # Prompt the user for the size of the triangle.
    size = int(screen.numinput("Input", "Enter size of each triangle:", 128, 2, 256))

    # Calculate the height of the equilateral triangle.
    height = int(math.sqrt(size ** 2 - (size // 2) ** 2))

    # Draw the first triangle pointing up, second triangle pointing down.
    draw_triangle_v2(artist, -size // 2, -height // 3, 0, size)
    draw_triangle_v2(artist, size // 2, height // 3, 180, size)

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


def draw_triangle_v2(tom, x, y, heading, size):
    """Use the given turtle to draw a triangle with one corner at coordinate (x,y).

    Note: The orientation of the triangle is dependent on the heading of the turtle.
    If the turtle's heading is zero, the (x,y) coordinate is the lower-left corner.

    :param turtle.Turtle tom: The turtle to do the drawing.
    :param int x: The x-coordinate of one corner of the triangle.
    :param int y: The y-coordinate of one corner of the triangle.
    :param int heading: The heading prior to drawing the triangle.
    :param int size: The length of one side of the triangle.
    """
    # Lift the pen and move to the indicated position.
    tom.penup()
    tom.setposition(x, y)
    tom.pendown()
    tom.setheading(heading)

    # Draw the triangle.
    for _ in range(3):
        tom.forward(size)
        tom.left(120)


def exercise4():
    """
    Draw a square and a triangle evenly spaced on the screen.
    """
    print_exercise_name()

    # Create the turtle screen and two turtles.
    screen, artist, writer = turtle_setup()

    # TODO 4: Use the draw_square and draw_triangle functions to draw a square and a triangle
    # TODO 4: evenly spaced on the screen, as described in the lab document.

    # Calculate the size of the square/triangle leaving a margin on each side and in between.
    size = (WIDTH - MARGIN * 3) // 2

    # Draw the square and triangle positioned using WIDTH, MARGIN, and size.
    draw_square(artist, -WIDTH // 2 + MARGIN, -size // 2, size)
    draw_triangle(artist, WIDTH // 2 - MARGIN - size, -size // 2, size)

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def turtle_setup():
    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup(WIDTH, HEIGHT, MARGIN, MARGIN)
    screen.bgcolor("SkyBlue")

    # Create two turtles, one for drawing and one for writing.
    turtle.TurtleScreen._RUNNING = True  # Get around bug in v3.5.2 http://bugs.python.org/issue26571
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape("turtle")

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay(0)
        artist.hideturtle()
        artist.speed("fastest")
        writer.hideturtle()
        writer.speed("fastest")

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading(90)  # Straight up, which makes it look sort of like a cursor.
    writer.penup()  # A turtle's pen does not have to be down to write text.
    writer.setposition(0, HEIGHT // 2 - FONT_SIZE * 2)  # Centered at top of the screen.

    return screen, artist, writer


def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
