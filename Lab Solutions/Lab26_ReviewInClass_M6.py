#!/usr/bin/env python3
"""
CS 210, Review for GR 2, Period M6.
"""

__author__ = "M6"
__instructor__ = "Dr. Bower"
__date__ = "24 Oct 2017"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs


def main():
    """Main program to call each individual function."""
    question1()
    question2()


def question1():
    """How do you read a file by lines, but then process individual words or characters?"""

    # Read the entire file into a list of strings, one string for each line of data.
    with open( "../Data/Test.txt", "r" ) as data_file:
        data_lines = data_file.read().splitlines()

    # The variable data_lines is a list of strings,
    # one string for each line in the data file.
    print( data_lines )

    # Loop through each full line of text in the data file.
    for line in data_lines:
        # Print each full line of text from the data file.
        print( line )

    # To process each word in the data, use nested loops.
    # Start by again looping through each line of data.
    for line in data_lines:
        # Now loop through each word in the line.
        # Note the line is split into words in this loop.
        for word in line.split():
            # Print each individual word on a separate line.
            print( word )
        # Print a blank line to indicate the end of a line from the data file.
        print()

    # To process each character in the data, use nested loops.
    # Start by again looping through each line of data.
    for line in data_lines:
        # Now loop through each character in the line.
        # Note the line is NOT split in this loop.
        for c in line:
            # Print each individual character on a separate line.
            print( c )
        # Print a blank line to indicate the end of a line from the data file.
        print()


def question2():
    """How do you deal with a list of tuples?"""

    # Here's a list of tuples.
    stuff = [ ( "Fred", 42 ), ( "Barney", 37 ), ( "Pebbles", 3 ) ]

    # Each item in the list is a tuple.
    for t in stuff:
        # Print the complete tuple.
        print( t )

    # Extract the pieces of the tuple into separate variables.
    for t in stuff:
        name = t[ 0 ]
        age = t[ 1 ]
        print( name, age )

    # For convenience, the extraction can be accomplished on one line.
    for t in stuff:
        name, age = t
        print( name, age )

    # For convenience, the elements of a tuple can be extracted in the for loop.
    for name, age in stuff:
        print( name, age )


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
