#!/usr/bin/env python3
"""
Review in class for GR2.
"""
__author__ = "Robert Harder"

# String manipulation
# File I/O
# Lists
# Nested Lists
# Recursion
# Dictionaries
import json
import string


def load_word_groups(filename):
    with open(filename, "r") as f:
        words = f.read().split()

    d = {}  # type: dict[str, list[str]]
    for word in words:
        if len(word) > 0:
            first_letter = word[0].lower()
            if first_letter in string.ascii_letters:  # letters only
                if first_letter not in d:  # First time seeing letter
                    d[first_letter] = []

                # Strip out punctuation
                word = word.lower()
                new_word = ""
                for c in word:
                    if c in string.ascii_letters:
                        new_word = new_word + c
                word = new_word

                d[first_letter].append(word)  # Save the word
    return d


d = load_word_groups("../Data/Address.txt")
# print(json.dumps(d, indent=2))
print("Number of first letters:", len(d))


def summarize_data(filename, data):
    with open(filename, "w") as f:
        # print(d, file=f)

        for letter in string.ascii_lowercase:
            print(letter.upper(), file=f)

            if letter in data:
                for word in sorted(set(data[letter])):
                    print("\t", word, file=f)

                    # for word in data.get(letter, []):
                    #     print("\t", word, file=f)


summarize_data("../Data/Address_summary.txt", d)


def change_pi(text):
    if len(text) <= 1:
        return text
    else:
        if text.startswith("pi"):
            return "3.14" + change_pi(text[2:])
        else:
            return text[0] + change_pi(text[1:])


text = "happiness"
print(change_pi(text))

with open("../Data/Address.txt", "r") as f:
    lines = f.read().splitlines()

for line in lines:
    print(change_pi(line))


def all_star(text):
    if len(text) <= 1:
        return text
    else:
        if text[0] in string.ascii_letters and text[1] in string.ascii_letters:
            return text[0] + "*" + all_star(text[1:])
        else:
            return text[0] + all_star(text[1:])


print(all_star("hello world"))


def count_pairs(text):
    if len(text) <= 2:
        return 0
    else:
        if text[0] == text[2]:
            return 1 + count_pairs(text[1:])
        else:
            return count_pairs(text[1:])


def count_pairs_loop(text):
    count = 0
    for i in range(len(text) - 2):
        if text[i] == text[i + 2]:
            count += 1
    return count


tests = [
    ("AxA", 1),
    ("axax", 2),
    ("AxAxA", 3),
    ("axbx", 1),
    ("cat", 0),
    ("", 0)
]
for text, right_answer in tests:
    check = count_pairs_loop(text)
    print(text, check, "Correct?", check == right_answer)
