#!/usr/bin/env python3
"""
Lab17 Lists 2 Solution
CS 210, Introduction to Programming
"""

__author__ = "Firstname Lastname"
__instructor__ = "Rank Lastname"
__date__ = "dd mmm yyyy"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
-	Read Lesson 17: Lists from our online textbook

Lesson Objectives
-	Reinforce functions, parameters, return values, selection, and iteration.
-	Reinforce string attributes and operations.
-	Introduce more in-depth list attributes and operations.

Pair Programming
For this lab, your instructor may have you work with a partner using the pair programming technique.
In the pair programming technique, two programmers work together on one computer.
One is designated as the driver and writes the code; the other is designated as the
navigator and reviews each line of code as it is typed.  The two programmers switch
roles frequently.  For this lab, the programmers should switch roles at least every
five minutes.

    http://en.wikipedia.org/wiki/Pair_programming
"""

import easygui
import random
import string


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.

    exercise0()
    exercise1()
    exercise2()
    exercise3()
    exercise4()
    exercise5()  # Advanced testing!

    challenge1()
    challenge2()


def exercise0():
    """
    Demonstrate some basic list functionality.

    List Demo – Work with a partner and spend at least ten minutes but no more than
    fifteen minutes running and discussing the given code.  If necessary, refer to the
    table of list methods in our text, the table of common sequence operations, the
    table of mutable sequence operations, and the More on Lists page in the Python
    documentation.

        https://docs.python.org/3/library/stdtypes.html#common-sequence-operations
        https://docs.python.org/3/library/stdtypes.html#mutable-sequence-types
        https://docs.python.org/3/tutorial/datastructures.html#more-on-lists

    Be sure to ask other classmates and/or your instructor if anything is unclear.
    """
    print_exercise_name()

    # TODO 0: Read, discuss, and understand the following code.

    # Create a list of nine arbitrary values.
    a_list = [37, 86, 42, 51, 99, 13, 67, 75, 29]
    easygui.msgbox("Original list:\n\na_list = {}".format(a_list), "List - Original")

    # The len() function works the same with lists as it does with strings.
    easygui.msgbox("There are {} items in a_list.\n\na_list = {}".format(len(a_list), a_list), "List - Length")

    # The index() function is similar to find() in a string, but the item must exist or there is an error.
    easygui.msgbox("{} is at index {}\n{} is at index {}\n{} is at index {}\n\na_list = {}".format(
        37, a_list.index(37), 99, a_list.index(99), 29, a_list.index(29), a_list), "List - Index")

    # The "in" membership operator works the same with lists as it does with strings.
    easygui.msgbox("'{} in a_list' is {}.\n'{} in a_list' is {}.\n\na_list = {}".format(
        42, 42 in a_list, 64, 64 in a_list, a_list), "List - Membership")

    # The count() function works the same with lists as it does with strings.
    easygui.msgbox("a_list.count( {} ) = {}\na_list.count( {} ) = {}\n\na_list = {}".format(
        42, a_list.count(42), 64, a_list.count(64), a_list), "List - Count")

    # Python has several built-in functions that work with lists as parameters.
    easygui.msgbox("min( a_list ) = {}\nmax( a_list ) = {}\nsum( a_list ) = {}\n\na_list = {}".format(
        min(a_list), max(a_list), sum(a_list), a_list), "List - Min, Max, Sum")

    # Create two identical lists to demonstrate various operations that modify the list.
    a_list = [37, 86, 42, 51, 99, 13, 67, 75, 29]
    b_list = [37, 86, 42, 51, 99, 13, 67, 75, 29]
    easygui.msgbox("Original lists:\n\na_list = {}\n\nb_list = {}".format(
        a_list, b_list), "Lists - Original")

    # Lists are mutable! (Recall this did not work with strings.)
    b_list[4] = 11  # Replace the 99 with 11; note index 4 is the fifth item in the list.
    easygui.msgbox("Modified b_list; replaced 99 with 11:\n\na_list = {}\n\nb_list = {}".format(
        a_list, b_list), "Lists - Modify")

    # Lists can be sliced and concatenated with the same notation that slices strings.
    # Note the 99 being put back in the middle must be enclosed in [ and ] to make it a list!
    b_list = b_list[:4] + [99] + b_list[5:]  # Slice first and last four items, put 99 in the middle.
    easygui.msgbox("Sliced b_list and put 99 back in the middle:\n\na_list = {}\n\nb_list = {}".format(
        a_list, b_list), "Lists - Slice and Concatenate")

    # Items can be removed from a list using pop.
    item = b_list.pop()  # By default this pops the last item in the list.
    easygui.msgbox("Popped {} from the end of b_list.\n\na_list = {}\n\nb_list = {}".format(
        item, a_list, b_list), "Lists - Pop End")
    item = b_list.pop(0)  # The parameter can specify an index to pop; in this case the first item.
    easygui.msgbox("Popped {} from the front of b_list.\n\na_list = {}\n\nb_list = {}".format(
        item, a_list, b_list), "Lists - Pop Front")

    # Items can be inserted at a specific index and appended to the end of a list.
    b_list.insert(0, 37)  # Inserts the 37 in the front of the list.
    b_list.append(29)  # Appends the 29 onto the end of the list.
    easygui.msgbox("Inserted 37 in front, appended 29 to end of b_list:\n\na_list = {}\n\nb_list = {}".format(
        a_list, b_list), "Lists - Insert/Append")

    # Items with a specific value can be removed from a list.
    b_list.remove(99)
    easygui.msgbox("Removed 99 from b_list:\n\na_list = {}\n\nb_list = {}".format(
        a_list, b_list), "Lists - Remove")

    # Items in a specific location can be deleted from a list.
    del b_list[2]  # Removes the item in location 2 (third item in list); yes, this notation is unusual.
    easygui.msgbox("Deleted item in location 2 from b_list:\n\na_list = {}\n\nb_list = {}".format(
        a_list, b_list), "Lists - Del")

    # Put the removed/deleted values back into b_list before demonstrating sort.
    b_list.insert(2, 42)  # Inserts 42 in location 2 in the list.
    b_list.insert(4, 99)  # Inserts 99 in location 4 in the list.
    easygui.msgbox("Put 42 and 99 back into b_list:\n\na_list = {}\n\nb_list = {}".format(
        a_list, b_list), "Lists - Original")

    # Lists can be sorted, which changes the list (remember, lists are mutable).
    b_list.sort()  # Sorted in place (mutable)
    easygui.msgbox("Sorted b_list:\n\na_list = {}\n\nb_list = {}".format(
        a_list, b_list), "Lists - Sort")

    # Lists can also be reversed, which also changes the list.
    b_list.reverse()
    easygui.msgbox("Reversed b_list:\n\na_list = {}\n\nb_list = {}".format(
        a_list, b_list), "Lists - Reverse")

    # Sorted and reversed versions of a list can be created without modifying the original list.
    c_list = sorted(a_list)  # Creates a new list that is sorted - original is unchanged
    d_list = sorted(a_list, reverse=True)
    easygui.msgbox("Notice a_list remains unchanged!\n\na_list = {}\n\nc_list = {}\n\nd_list = {}".format(
        a_list, c_list, d_list), "Lists - Sorted and Reversed")

    # Finally, demonstrate the String To Integer List Translator function.
    s = "37 86 42 51 99 13 67 75 29"
    easygui.msgbox("Converting a string to a list of integers:\n\ns = {}\n\nstilt( s ) = {}".format(
        s, stilt(s)), "S.T.I.L.T.")


def stilt(s):
    """
    String To Integer List Translator; converts a list of whitespace delimited digits
    to a list of integers.

    This function is convenient for creating a list of values from a string of user input.
    More importantly, it demonstrates the accumulator pattern with lists.

    Note: The expected input is a string such as "37 86 42 51 99 13 67 75 29", but the function
    also strips punctuation so this would also work, "37, 86, 42, 51, 99, 13, 67, 75, 29".

    :param str s: A string of integer values separated by spaces.
    :return: A list of integer values.
    :rtype: list[int]
    """
    # Start with an empty list and use the accumulator pattern.
    result = []
    # Split the input string and loop through each individual string value.
    for str_value in s.split():
        # Strip punctuation from the ends of the string value, convert
        # it to an integer value, and append it to the result list.
        result.append(int(str_value.strip(string.punctuation)))
    return result


def exercise1():
    """
    Mean – In the space "TODO 1", complete the mean function such that it calculates
    the arithmetic mean of a list of numeric values.  Specifically, the function is
    passed a list of numeric values and returns a float value that is the arithmetic
    mean of the values.

        https://en.wikipedia.org/wiki/Arithmetic_mean

    Note: This function should not modify the actual list passed as a parameter.
    """
    print_exercise_name()

    s = easygui.enterbox("Enter a list of numbers (Cancel to quit):", "Input", "37 86 42 51 99 13 67 75 29")
    while s is not None and len(s) > 0:
        int_list = stilt(s)
        easygui.msgbox("The mean of {} is {:.2f}.".format(int_list, mean(int_list)), "Result")
        s = easygui.enterbox("Enter a list (Cancel to quit):", "Input")


def mean(data):
    """
    Calculates and returns the mean of a list of values.

    Note: This function DOES NOT modify the list passed as a parameter.

    :param list data: The list of values for which the mean is to be calculated.
    :return: The mean of the values.
    :rtype: float
    """
    # TODO 1: Remove the line below and complete the function as described in the lab document.
    # return 0.0

    return sum(data) / len(data)


def exercise2():
    """
    Median – In the space "TODO 2", complete the median function such that it determines
    the median value of a list of numeric values.  Specifically, the function is passed
    a list of numeric values and returns a float value that is the median of the values.

        https://en.wikipedia.org/wiki/Median

    Note: This function should not modify the actual list passed as a parameter.
    """
    print_exercise_name()

    s = easygui.enterbox("Enter a list of numbers (Cancel to quit):", "Input", "37 86 42 51 99 13 67 75 29")
    while s is not None and len(s) > 0:
        int_list = stilt(s)
        easygui.msgbox("The median of {} is {:.2f}.".format(int_list, median(int_list)), "Result")
        s = easygui.enterbox("Enter a list (Cancel to quit):", "Input")


def median(data):
    """
    Calculates and returns the median of a list of values.

    Note: This function DOES NOT modify the list passed as a parameter.

    :param list data: The list of values for which the median is to be calculated.
    :return: The median of the values.
    :rtype: float
    """
    # TODO 2: Remove the line below and complete the function as described in the lab document.
    # return 0.0

    if len(data) % 2 == 1:
        return sorted(data)[len(data) // 2]
    else:
        return (sorted(data)[len(data) // 2] + sorted(data)[len(data) // 2 - 1]) / 2


def exercise3():
    """
    Mode – In the space "TODO 3", complete the mode function such that it determines
    the mode value of a list of values.  Specifically, the function is passed a list
    of values and returns the value that occurs most often in the list.

        https://en.wikipedia.org/wiki/Mode_(statistics)

    Note: This function should not modify the actual list passed as a parameter.
    """
    print_exercise_name()

    s = easygui.enterbox("Enter a list of numbers (Cancel to quit):", "Input", "1 2 3 2 3 4 5 4 3 2 3 4 3 2 1 2 3 2 1")
    while s is not None and len(s) > 0:
        int_list = stilt(s)
        easygui.msgbox("The mode of {} is {}.".format(int_list, mode(int_list)), "Result")
        s = easygui.enterbox("Enter a list (Cancel to quit):", "Input")


def mode(data):
    """
    Calculates and returns the mode of a list of values.

    Note: This function DOES NOT modify the list passed as a parameter.

    :param list data: The list of values for which the mode is to be calculated.
    :return: The mode of the values.
    """
    # TODO 3: Remove the line below and complete the function as described in the lab document.
    # return data[ 0 ]

    # Start with the first value in the list as the mode.
    a_la_mode = data[0]  # Pie a la mode ... get it? (Couldn't use 'mode' as a variable and function name.)
    mode_count = data.count(a_la_mode)

    # Go through all remaining values and see if one occurs more often.
    for value in data[1:]:
        if data.count(value) > mode_count:
            a_la_mode = value
            mode_count = data.count(value)
    return a_la_mode


def exercise4():
    """
    Random list of numbers

    a.	In the space "TODO 4a", write a function named rand_list that creates and
        returns a list of random integer values.  Specifically, the function is passed
        three integer parameters; the first specifies how many list elements to create,
        the second specifies the lower bound of the random values (inclusive), and the
        third specifies the upper bound of the random values (inclusive).

    b.	In the space "TODO 4b", write code that uses your rand_list function to create
        several random lists (varying the number of elements and lower/upper bounds) and
        then applies your mean, median, and mode functions to the lists.  You may
        display your results in an easygui.msgbox or print them to the console window.
    """
    print_exercise_name()

    # TODO 4b: Write code to use the function as described in the lab document.
    pass  # Remove the pass statement (and this comment) when writing your own code.

    for magnitude in range(4):
        # Build a random list with 8-16 elements; one-digit, two-digit, three-digit, then four-digit values.
        data = rand_list(random.randint(8, 16), 10 ** magnitude, 10 ** (magnitude + 1) - 1)
        easygui.msgbox("{}\n{}\n\nmean = {:.2f}\nmedian = {}\nmode = {}\niqm = {:.2f}".format(
            data, sorted(data), mean(data), median(data), mode(data), iqm(data)), "Result")


# TODO 4a: In the space below, write the function as described in the lab document.
def rand_list(how_many, lower_bound, upper_bound):
    """
    Build and return a list with the given number of random values in the specified range.

    :param int how_many: How many random values to include in the list.
    :param int lower_bound: The lower bound of the random values, inclusive.
    :param int upper_bound: The upper bound of the random values, inclusive.
    :return: A list with the indicated number of random values.
    :rtype: list[int]
    """
    r_list = []
    for i in range(how_many):
        r_list.append(random.randint(lower_bound, upper_bound))
    return r_list


def exercise5():
    """
    Swap Smallest – In this exercise you will write a function that accepts a list as a
    parameter and modifies the list.

    a.  In the space "TODO 5a", complete the move_smallest function such that it modifies the
        list passed as a parameter by moving the smallest item in the list to location zero
        (SWAPPING it with the item currently in location zero).

        For example, consider the following code:
            a_list = [ 42, 86, 37, 68 ]
            print( a_list )
            move_smallest( a_list )
            print( a_list )

        The output of the above code would be:
            [ 42, 86, 37, 68 ]
            [ 37, 86, 42, 68 ]

    b.  In the space "TODO 5b", add three more tests to those provided that show
        that your function works as expected.

        Note: Because the function modifies the list, we have to make some copies
        in order to accurately compare the original list to the modified one.
    """
    print_exercise_name()

    # TODO 5b: Add three more examples to the two below that test your function
    tests = [
        # Note how using tuples to group tests provides a nicer delineation
        #   Numbers         Expected Values
        ([42, 86, 37, 68], [37, 86, 42, 68]),  # Test 1
        ([25, 20, 30, 7], [7, 20, 30, 25]),  # Test 2
        ([98, 12, 97, 34, 63, 48, 13, 58], [12, 98, 97, 34, 63, 48, 13, 58]),  # Test 3
        ([30, 99, 87, 51, 34, 76, 22, 58], [22, 99, 87, 51, 34, 76, 30, 58]),  # Test 4
        ([96, 63, 36, 39, 14, 82, 61, 44], [14, 63, 36, 39, 96, 82, 61, 44])  # Test 5
    ]
    for numbers, expected_value in tests:  # Pull out a single test
        original = numbers[:]
        swap_smallest(numbers)  # See what our function gets
        actual_value = numbers
        if actual_value != expected_value:  # Was it right?
            print("Incorrect: {}. Expected: {}. Got: {}".format(
                original, expected_value, actual_value))


# TODO 5a: In the space below, write the function as described
def swap_smallest(data):
    """
    This function moves the smallest item in a list of data to location 0.

    The item in location 0 is moved to the location where the smallest item is found.

    :param list data: A list of data items.
    :return: None
    """
    # TODO 5a: In the space below, complete the function as described
    # Pythonically find the index of the smallest item.
    index_of_smallest = data.index(min(data))

    # Save a copy of the two values to swap
    first_value = data[0]
    smallest_value = data[index_of_smallest]

    # Replace the values
    data[0] = smallest_value
    data[index_of_smallest] = first_value


"""
Challenge Exercises

Challenge 1 and Challenge 2 are the challenge exercises

"""


def challenge1():
    """
    Finding Duplicates

    a.	In the space "TODO c1a", write a function named has_duplicates that determines
        if a list of values contains duplicates.  Specifically, the function is passed a
        list of values and returns True if the list contains any duplicate values; False
        if it does not.

        Note: This function should not modify the actual list passed as a parameter.

    b.	In the space "TODO c1b", write code that uses your rand_list function to create
        several random lists (varying the number of elements and lower/upper bounds) and
        uses the lists to test your has_duplicates function.  You may print your results
        to the console window or display them in an easygui.msgbox.
    """
    print_exercise_name()

    # TODO c1b: Write code to use the function as described in the lab document.
    # pass  # Remove the pass statement (and this comment) when writing your own code.

    for magnitude in range(4):
        # Build a random list with 16-3 elements; one-digit, two-digit, three-digit, then four-digit values.
        data = rand_list(random.randint(16, 32), 10 ** magnitude, 10 ** (magnitude + 1) - 1)
        easygui.msgbox("{}\n{}\n\nhas_duplicates = {}\nhas_duplicates = {}\nhas_duplicates = {}".format(
            data, sorted(data), has_duplicates(data),
            has_duplicates_v2(data), has_duplicates_v3(data)), "Result")


# TODO c1a: In the space below, write the function as described in the lab document.
def has_duplicates(a_list):
    """Determines if a given list contains duplicate values.

    Note: This function DOES NOT modify the list passed as a parameter.

    :param list a_list: A list of values.
    :return: True if the list contains duplicate values; False otherwise.
    :rtype: bool
    """
    # Solution using nested loops to compare every value to every other value.
    for i in range(len(a_list)):
        for j in range(i + 1, len(a_list)):
            if a_list[i] == a_list[j]:
                # Found a pair that match, so immediately return True and quit looking.
                return True

    # Made it through all comparisons without finding a match, so no duplicates.
    return False


def has_duplicates_v2(a_list):
    """Determines if a given list contains duplicate values.

    Note: This function DOES NOT modify the list passed as a parameter.

    :param list a_list: A list of values.
    :return: True if the list contains duplicate values; False otherwise.
    :rtype: bool
    """
    # Solution using a sorted list and checking for equal neighbors.
    a_list = sorted(a_list)  # Make a sorted copy without modify original list.
    for index in range(len(a_list) - 1):
        if a_list[index] == a_list[index + 1]:
            return True

    # Made it through all comparisons without finding a match, so no duplicates.
    return False


def has_duplicates_v3(a_list):
    """Determines if a given list contains duplicate values.

    Note: This function DOES NOT modify the list passed as a parameter.

    :param list a_list: A list of values.
    :return: True if the list contains duplicate values; False otherwise.
    :rtype: bool
    """
    # Solution using the list's count method.
    for a_value in a_list:
        if a_list.count(a_value) > 1:
            return True

    # Made it through all comparisons without finding a match, so no duplicates.
    return False


def challenge2():
    """
    Removing Duplicates

    a.	In the space "TODO c2a", write a function named remove_duplicates that removes
        duplicate values from a list.  Specifically, the function is passed a list of
        values and it modifies that list such that it does not contain duplicate values.
        Further, the function returns True if any duplicate values were removed;
        False if not.

    b.	In the space "TODO c2b", write code that uses your rand_list function to create
        several random lists (varying the number of elements and lower/upper bounds)
        and uses the lists to test your remove_duplicates function. You may print your
        results to the console window or display them in an easygui.msgbox.
    """
    print_exercise_name()

    # TODO c2b: Write code to use the function as described in the lab document.
    pass  # Remove the pass statement (and this comment) when writing your own code.

    for magnitude in range(4):
        # Build a random list with 16-3 elements; one-digit, two-digit, three-digit, then four-digit values.
        data = rand_list(random.randint(16, 32), 10 ** magnitude, 10 ** (magnitude + 1) - 1)
        for func in [remove_duplicates, remove_duplicates_v2, remove_duplicates_v3]:
            a_list = data.copy()
            print(a_list)
            print(func(a_list))
            print(a_list)
            print()
        print()


# TODO c2a: In the space below, write the function as described in the lab document.
def remove_duplicates(a_list):
    """Removes any duplicate values from the list passed as a parameter.

    Note: This function DOES modify the list passed as a parameter if it removes a duplicate.

    :param list a_list: A list of values.
    :return: True if the list contains duplicate values; False otherwise.
    :rtype: bool
    """
    # Make a copy of the original list, then empty the original list.
    b_list = a_list.copy()
    a_list.clear()

    for value in b_list:
        if value not in a_list:
            a_list.append(value)

    # Return true if there were duplicates removed.
    return len(a_list) != len(b_list)


def remove_duplicates_v2(a_list):
    """Removes any duplicate values from the list passed as a parameter.

    Note: This function DOES modify the list passed as a parameter if it removes a duplicate.

    :param list a_list: A list of values.
    :return: True if the list contains duplicate values; False otherwise.
    :rtype: bool
    """
    # Make a list of the duplicates.
    duplicates = []
    for index in range(len(a_list)):
        if a_list[index] in a_list[index + 1:]:
            duplicates.append(a_list[index])

    # Now remove all the duplicates from a_list.
    for number in duplicates:
        a_list.remove(number)

    # Return true if there were duplicates removed.
    return len(duplicates) > 0


def remove_duplicates_v3(a_list):
    """Removes any duplicate values from the list passed as a parameter.

    Note: This function DOES modify the list passed as a parameter if it removes a duplicate.

    :param list a_list: A list of values.
    :return: True if the list contains duplicate values; False otherwise.
    :rtype: bool
    """
    original_length = len(a_list)

    # Sort the list and remove adjacent, duplicate values.
    a_list.sort()
    index = 0
    while index < len(a_list) - 1:
        if a_list[index] == a_list[index + 1]:
            a_list.pop(index + 1)
        else:
            index += 1

    # Return true if there were duplicates removed.
    return len(a_list) != original_length


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
