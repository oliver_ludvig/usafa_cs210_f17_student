#!/usr/bin/env python3
"""
Lab10 Iteration
CS 210, Introduction to Programming
"""

import easygui
import random
import turtle

__author__ = "Firstname Lastname"
__instructor__ = "Rank Lastname"
__date__ = "dd mmm yyyy"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
-	Read Lesson 11: Iteration from our online textbook
-	Watch the embedded video on the while statement
Lesson Objectives
-	Reinforce functions, parameters, return values, and selection statements
-	Reinforce definite iteration with the for loop
-	Introduce indefinite iteration with the while loop


"""

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960  # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 // 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = WIDTH // 30  # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = MARGIN // 2  # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = False  # Set to True for fast, non-animated turtle movement.


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    exercise0()
    exercise1()
    exercise2()
    exercise3()
    exercise4()
    exercise5()


def exercise0():
    """Example code."""
    print_exercise_name()

    # Count by 3s with for loop
    for i in range(3, 20, 3):
        print(i, " ", end="")
    print()

    # Equivalent count by 3s with while loop
    n = 3
    while n < 20:
        print(n, " ", end="")
        n = n + 3
    print()


def exercise1():
    """
    Interact with the user and test the sum_odds and sum_evens functions.

    a.  In the space "TODO 1a", add four more tests to those provided that show
        that your function will work as expected.
        NOTE: Run the exercise1() function, which calls the existing sum_odds and
        sum_evens functions (known to work properly) to generate good test numbers.

    b.	In the space "TODO 1b", re-write the indicated code using a while loop
        instead of a for loop.

    c.	In the space "TODO 1c", re-write the indicated code using a while loop
        instead of a for loop.
    """
    print_exercise_name()

    # Test your function
    if sum_odds(10) != 25:
        print("Incorrect: 10 odds")
    if sum_evens(10) != 30:
        print("Incorrect: 10 evens")
    # TODO 1a: Add four more examples to the two above that test your function
    if sum_odds(42) != 441:
        print("Incorrect: 42 odds")
    if sum_evens(42) != 462:
        print("Incorrect: 42 evens")
    if sum_odds(100) != 2500:
        print("Incorrect: 100 odds")
    if sum_evens(100) != 2550:
        print("Incorrect: 100 evens")

    # You _DO_NOT_ need to modify this code for Lab 10.
    n = easygui.integerbox("Enter n:", "Input", lowerbound=0, upperbound=2 ** 31)
    odd = sum_odds(n)
    even = sum_evens(n)
    easygui.msgbox("n = {}\nsum of odds = {}\nsum of evens = {}".format(n, odd, even))


def sum_odds(n):
    """Calculate and return the sum of the odd numbers between 1 and n, inclusive.

    :param int n: The upper bound of the series to sum.
    :return: The sum of the odd numbers between 1 and n, inclusive.
    :rtype: int
    """
    # TODO 1b: Re-write the code below using a while loop instead of a for loop.
    # result = 0
    # for value in range( 1, n + 1, 2 ):
    #     result += value

    result = 0
    value = 1
    while value <= n:
        result += value
        value += 2
    return result


def sum_evens(n):
    """Calculate and return the sum of the even numbers between 1 and n, inclusive.

    :param int n: The upper bound of the series to sum.
    :return: The sum of the even numbers between 1 and n, inclusive.
    :rtype: int
    """
    # TODO 1c: Re-write the code below using a while loop instead of a for loop.
    # result = 0
    # for value in range( 2, n + 1, 2 ):
    #     result += value

    result = 0
    value = 2
    while value <= n:
        result += value
        value += 2
    return result


def exercise2():
    """
    Interact with the user and test the summation function (from Lab08).

    a.  In the space "TODO 2a", add three more tests to those provided that show
        that your function will work as expected.
        NOTE: Run the exercise2() function, which calls the existing summation
        function (known to work properly) to generate good test numbers.

    b.	In the space "TODO 2b", re-write the indicated code using a while loop
        instead of a for loop.

    """
    print_exercise_name()

    # Test your function
    if summation(10, 1) != 55:
        print("Incorrect: n=10, i^1")
    if summation(10, 2) != 385:
        print("Incorrect: n=10, i^2")
    if summation(10, 3) != 3025:
        print("Incorrect: n=10, i^3")
    # TODO 2a: Add three more examples to the three above that test your function
    if summation(42, 1) != 903:
        print("Incorrect: n=42, i^1")
    if summation(42, 2) != 25585:
        print("Incorrect: n=42, i^2")
    if summation(42, 3) != 815409:
        print("Incorrect: n=42, i^3")

    # You _DO_NOT_ need to modify this code
    n = easygui.integerbox("Exercise 2\nEnter n:", "Input", lowerbound=0, upperbound=2 ** 31)

    s = summation(n, 1)
    f = n * (n + 1) // 2
    easygui.msgbox("n = {}\nsummation(n, 1) = {}\nformula result = {}".format(n, s, f))

    s = summation(n, 2)
    f = n * (n + 1) * (2 * n + 1) // 6
    easygui.msgbox("n = {}\nsummation(n, 2) = {}\nformula result = {}".format(n, s, f))

    s = summation(n, 3)
    f = (n * (n + 1) // 2) ** 2
    easygui.msgbox("n = {}\nsummation(n, 3) = {}\nformula result = {}".format(n, s, f))


def summation(n, exponent):
    """Calculation and return the summation of the series 1**exponent + 2**exponent + ... + n**exponent.

    :param int n: The upper bound of the series to sum.
    :param int exponent: The exponent for each term in the series.
    :return: The summation of the series.
    :rtype: int
    """
    # TODO 2b: Re-write the code below using a while loop instead of a for loop.
    # result = 0
    # for value in range( 1, n + 1 ):
    #     result += value ** exponent

    result = 0
    value = 1
    while value <= n:
        result += value ** exponent
        value += 1
    return result


def exercise3():
    """
    Interact with the user and test the count_multiples function (from Lab08).

    a.  In the space "TODO 3a", add three more tests to those provided that show
        that your function will work as expected.
        NOTE: Run the exercise3() function, which calls the existing count_multiples
        function (known to work properly) to generate good test numbers.

    b.	In the space "TODO 3b", re-write the indicated code using a while loop
        instead of a for loop.
    """
    print_exercise_name()

    # Test your function
    if count_multiples(10, 20, 3) != 3:
        print("Incorrect: (10, 20, 3)")
    if count_multiples(1947, 2017, 4) != 18:
        print("Incorrect: (1947, 2017, 4)")
    # TODO 3a: Add three more examples to the three above that test your function
    if count_multiples(1000, 1200, 23) != 9:
        print("Incorrect: (1000, 1200, 23)")
    if count_multiples(30, 40, 1) != 11:
        print("Incorrect: (30, 40, 14)")
    if count_multiples(23, 42, 9) != 2:
        print("Incorrect: (23, 42, 9)")

    # You _DO_NOT_ need to modify this code for Lab 10.
    start = easygui.integerbox("Enter start value:", "Input", lowerbound=-2 ** 31, upperbound=2 ** 31)
    stop = easygui.integerbox("Enter stop value:", "Input", lowerbound=-2 ** 31, upperbound=2 ** 31)
    step = easygui.integerbox("Enter divisor value:", "Input", lowerbound=-2 ** 31, upperbound=2 ** 31)
    easygui.msgbox("There are {} multiples of {} in the range [{},{}].".format(
        count_multiples(start, stop, step), step, start, stop), "Result")


def count_multiples(start, stop, divisor):
    """Count and return the number of values between start and stop, inclusive, evenly divisible by divisor.

    :param int start: The start value for the range, inclusive.
    :param int stop: The stop value for the range, inclusive.
    :param int divisor: The divisor to be counted.
    :return: The number of values in the range [start, stop] evenly divisible by divisor.
    :rtype: int
    """
    # TODO 3b: Re-write the code below using a while loop instead of a for loop.
    # count = 0
    # for value in range( start, stop + 1 ):
    #     if value % divisor == 0:
    #         count += 1

    count = 0
    value = start
    while value <= stop:
        if value % divisor == 0:
            count += 1
        value += 1
    return count


def exercise4():
    """
    Interact with the user and test the count_sevens function.

    a.	In the space "TODO 4a", write a function named roll_sevens that
        simulates repeatedly rolling two six-sided dice. The function receives
        an integer value as a parameter specifying the number of times the function
        should roll the value 7. When the value 7 has been rolled the specified number
        of times, the function should return the total number of times the dice were rolled.

        Also, to avoid the possibility (however remote) of an infinite loop, the function
        should also stop rolling the dice if the total number of rolls ever exceeds twice
        the EXPECTED number of rolls to reach the requested number of 7s. If this happens,
        it still returns the total number of rolls as normal (but the results shown by
        the exercise4 function will be invalid as the requested number of 7s was not reached.

    Hint: How many rolls would you expect it to take to roll a single 7?
    How many to roll ten 7s? One-hundred?

    Background: http://www.mastersetter.com/dice/rolling/probability/

    b.	In the space "TODO 4b", write code that uses the easygui.integerbox
        to obtain an integer value from the user for the desired number of 7s.
        Use the value entered as the parameter to the roll_sevens function and
        then display the results in an easygui.msgbox, including all of the
        information in the result dialog as shown below.

    Hint: The roll_sevens function does not contain any calls to the easygui module;
	all user interaction is done in the exercise4 function.

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |          How many 7s:                 |
            |  [ 1000                          ]    |
            |        +------+       +--------+      |
            |        |  OK  |       | Cancel |      |
            |        +------+       +--------+      |
            +---------------------------------------+

            +------------------------------------------+
            |                              [_] [ ] [X] |
            +------------------------------------------+
            |  1000 out of 5995 rolls (16.68%) were 7. |
            |             +------+                     |
            |             |  OK  |                     |
            |             +------+                     |
            +------------------------------------------+
    """
    print_exercise_name()

    # TODO 4b: Write code to use the roll_sevens function as described
    pass  # Remove the pass statement (and this comment) when writing your own code.

    sevens = easygui.integerbox("How many 7s:", "Input", default=1000, lowerbound=0, upperbound=2 ** 31)
    rolls = roll_sevens(sevens)
    easygui.msgbox("{} out of {} rolls ({:.2f}%) were 7.".format(sevens, rolls, 100 * sevens / rolls))


# TODO 4a: In the space below, write the roll_sevens function as described
def roll_sevens(how_many):
    """Simulates rolling two separate dice until 7 is rolled the given number of times.

    :param int how_many: The desired number of times to roll the value 7.
    :return: The total number of dice rolls required to roll the specified number of 7s.
    :rtype: int
    """
    # Initialize counter variables.
    seven_count = 0
    total_rolls = 0
    expected_number = how_many * 6
    # Continue until the value 7 has been rolled the desired number of times
    # or the total number of rolls has exceeded twice the expected required number.
    while seven_count < how_many and total_rolls < expected_number * 2:
        # Simulate rolling two separate six-sided dice.
        die1 = random.randint(1, 6)
        die2 = random.randint(1, 6)
        # If the roll is a seven, increment the seven count.
        if die1 + die2 == 7:
            seven_count += 1
        # Always increment the total number of rolls.
        total_rolls += 1

    # Return the total number of rolls required to achieve the desired number of 7s.
    return total_rolls


def exercise5():
    """
    Score Keeper – Games such as racquetball, table tennis, and volleyball involve
    two players (or teams) earning points in a race to the winning score. However,
    these games include the caveat that the winner must win by at least two points.
    For example, if the winning score in a racquetball game is 15 and the players
    are tied at 14, the next point does not win the game. Instead, the game continues
    until one of the players has earned two points more than the opponent.

    a.	In the space "TODO 5a", write a function named scorekeeper that uses an
        easygui.buttonbox to keep score for a game such as racquetball or volleyball,
        as described above. The function receives three parameters: a string for the
        first player's name, a string for the second player's name, and an integer for
        the winning score of the game being played. The function returns a string
        indicating the winning player's name.

    b.	In the space "TODO 5b", write code that uses the easygui.enterbox (twice)
        to obtain the two player names and an easygui.integerbox to obtain an integer
        value for the winning score of the game. Use the values entered as the parameters
        to the scorekeeper function and then display the results in an easygui.msgbox.
        A series of interactions with the program is shown below.


            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |         Enter first player name:      |
            |  [ Scooby                        ]    |
            |        +------+       +--------+      |
            |        |  OK  |       | Cancel |      |
            |        +------+       +--------+      |
            +---------------------------------------+

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |         Enter second player name:     |
            |  [ Shaggy                        ]    |
            |        +------+       +--------+      |
            |        |  OK  |       | Cancel |      |
            |        +------+       +--------+      |
            +---------------------------------------+

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |      Enter required winning score:    |
            |  [ 3                             ]    |
            |        +------+       +--------+      |
            |        |  OK  |       | Cancel |      |
            |        +------+       +--------+      |
            +---------------------------------------+

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |      Scooby: 0                        |
            |        vs.                            |
            |       Shaggy: 0                       |
            |      Who wins the current point?      |
            |      +--------+       +--------+      |
            |      | Scooby |       | Shaggy |      |
            |      +--------+       +--------+      |
            +---------------------------------------+

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |      Scooby: 1                        |
            |        vs.                            |
            |       Shaggy: 0                       |
            |      Who wins the current point?      |
            |      +--------+       +--------+      |
            |      | Scooby |       | Shaggy |      |
            |      +--------+       +--------+      |
            +---------------------------------------+

            ... and so forth until one of them wins (by two) ...

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |       Scooby Wins!                    |
            |             +------+                  |
            |             |  OK  |                  |
            |             +------+                  |
            +---------------------------------------+


    """
    print_exercise_name()

    # TODO 5b: Write code to use the scorekeeper function as described
    pass  # Remove the pass statement (and this comment) when writing your own code.

    name1 = easygui.enterbox("Enter first player name:", "Input", "Scooby")
    name2 = easygui.enterbox("Enter second player name:", "Input", "Shaggy")
    score = easygui.integerbox("Enter required winning score:", "Input",
                               default=3, lowerbound=3, upperbound=2 ** 31)
    winner = scorekeeper(name1, name2, score)
    easygui.msgbox("{} wins!".format(winner), "Result")


# TODO 5a: In the space below, write the scorekeeper function as described
def scorekeeper(player1, player2, limit):
    """Implements a simple scorekeeper for a two-player game in which the winner must win by two or more.

    :param str player1: The first player's name.
    :param str player2: The second player's name.
    :param int limit: The winning score for the game.
    :return: The winning player.
    :rtype: str
    """
    # Initialize the variables for a new game.
    score1 = 0
    score2 = 0

    # Continue until one player has reached the limit and is ahead by two.
    while score1 < limit and score2 < limit or abs(score1 - score2) < 2:

        # Use a buttonbox to get the winner of the next point.
        winner = easygui.buttonbox("{} : {}\n    vs.\n{} : {}\n\nWho wins current point?".format(
            player1, score1, player2, score2), "Input", choices=[player1, player2])

        # Update the appropriate score based on the winner.
        if winner == player1:
            score1 += 1
        else:
            score2 += 1

    # After the loop, the player with the higher score is the winner.
    if score1 > score2:
        return player1
    else:
        return player2





# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def turtle_setup():
    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    #  ___   ___     _  _  ___ _____    __  __  ___  ___ ___ _____   __
    # |   \ / _ \   | \| |/ _ \_   _|  |  \/  |/ _ \|   \_ _| __\ \ / /
    # | |) | (_) |  | .` | (_) || |    | |\/| | (_) | |) | || _| \ V /
    # |___/ \___/   |_|\_|\___/ |_|    |_|  |_|\___/|___/___|_|   |_|
    #  _____ _  _ ___ ___    ___ _   _ _  _  ___ _____ ___ ___  _  _
    # |_   _| || |_ _/ __|  | __| | | | \| |/ __|_   _|_ _/ _ \| \| |
    #   | | | __ || |\__ \  | _|| |_| | .` | (__  | |  | | (_) | .` |
    #   |_| |_||_|___|___/  |_|  \___/|_|\_|\___| |_| |___\___/|_|\_|
    #
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup(WIDTH, HEIGHT, MARGIN, MARGIN)
    screen.bgcolor("SkyBlue")

    # Create two turtles, one for drawing and one for writing.
    turtle.TurtleScreen._RUNNING = True  # Get around bug in v3.5.2 http://bugs.python.org/issue26571
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape("turtle")

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay(0)
        artist.hideturtle()
        artist.speed("fastest")
        writer.hideturtle()
        writer.speed("fastest")

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading(90)  # Straight up, which makes it look sort of like a cursor.
    writer.penup()  # A turtle's pen does not have to be down to write text.
    writer.setposition(0, HEIGHT // 2 - FONT_SIZE * 2)  # Centered at top of the screen.

    return screen, artist, writer


def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
