#!/usr/bin/env python3
"""
Lab16 Strings 2 Solution
CS 210, Introduction to Programming
"""

import string

import easygui
from Labs.Lab13_NestedIteration_Solution import print_bar_chart

__author__ = "Firstname Lastname"
__instructor__ = "Rank Lastname"
__date__ = "dd mmm yyyy"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
-	Read Lesson 16: Strings from our online textbook

Lesson Objectives
-	Reinforce functions, parameters, return values, selection, and iteration
-	Introduce more in-depth string attributes and operations.



Pair Programming
For this lab, you will work with a partner using the pair programming technique.
In the pair programming technique, two programmers work together on one computer.
One is designated as the driver and writes the code; the other is designated as the
navigator and reviews each line of code as it is typed.  The two programmers switch
roles frequently.  For this lab, the programmers should switch roles at least every
five minutes.

    http://en.wikipedia.org/wiki/Pair_programming
"""

# Constant definition for use in the Pig Latin and Rovarspraket exercises.
VOWELS = "AEIOUaeiou"


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    exercise0()
    exercise1()
    exercise2()
    exercise3()
    exercise4()
    exercise5()
    exercise2_challenge()


def exercise0():
    """
    Demonstrate some basic string functionality.

    String Demo – Work with a partner and spend at least five minutes but no more
    than ten minutes running and discussing the given code.  If necessary, refer
    to the string method documentation and the string constants documentation.
    Be sure to ask other classmates and/or your instructor if anything is unclear.

        https://docs.python.org/3/library/stdtypes.html#string-methods

        https://docs.python.org/3/library/string.html#string-constants
    """
    print_exercise_name()

    # TODO 0: Read, discuss, and understand the following code.

    # A string of well known text to do some testing with the string methods.
    core_values = "Integrity first,\nService before self,\nExcellence in all we do."
    easygui.msgbox(core_values, "Core Values - Original")

    # Strings are immutable. What does this mean?
    easygui.msgbox(core_values.upper(), "CORE VALUES - upper()")
    easygui.msgbox(core_values, "Core Values - Original has not changed!")

    # Besides upper(), there are a lot more useful string methods:
    # https://docs.python.org/3/library/stdtypes.html#string-methods
    easygui.msgbox(core_values.lower(), "core values - lower()")
    easygui.msgbox(core_values.title(), "Core Values - title()")
    easygui.msgbox(core_values.replace(",", "!").replace(".", "!"), "Core Values - replace()")
    easygui.msgbox(core_values, "Core Values - Original still has not changed!")

    # Strings can be sliced, and slices can be concatenated together.
    index = core_values.find("\n")  # Find the index of the first newline character.
    easygui.msgbox(core_values[index + 1:] + "\n" + core_values[:index], "Core Values - Sliced")
    easygui.msgbox(core_values, "Core Values - Original STILL has not changed!")

    # More useful stuff - counting and searching for things in a string.
    easygui.msgbox("The letter 'i' occurs {} times in the test string:\n\n{}".format(
        core_values.count('i'), core_values), "Core Values - count()")
    easygui.msgbox("The word 'self' starts in position {} in the test string:\n\n{}".format(
        core_values.find("self"), core_values), "Core Values - find()")

    # The string module defines some useful constants:
    # https://docs.python.org/3/library/string.html#string-constants
    string_constants = "{}\n\n{}\n\n{}\n\n{}".format(string.ascii_uppercase, string.ascii_lowercase,
                                                     string.digits, string.punctuation)
    easygui.msgbox(string_constants, "String Constants")

    # Another useful string constant is string.whitespace. What does this code do?
    result = ""  # Start with an empty string.
    for character in core_values:  # Loop through each character of the test string.
        if character not in string.whitespace:  # If it is not a whitespace character,
            result += character  # append it to the result.
    easygui.msgbox(result, "Core Values - Without Whitespace")


def exercise1():
    """
    Three-peat – In the space "TODO 1", complete the three_peat function such
    that it returns a string that contains three copies of the parameter, separated
    by newline characters, with the first copy being lowercase, the second being title
    case, and the third being uppercase.

            +------------------------+
            |            [_] [ ] [X] |
            +------------------------+
            |     this is a test.    |
            |     This Is A Test.    |
            |     THIS IS A TEST.    |
            |         +------+       |
            |         |  OK  |       |
            |         +------+       |
            +------------------------+
    """
    print_exercise_name()

    s = easygui.enterbox("Enter a string (Cancel to quit):", "Three-Peat - Input", "This is a test.")
    while s is not None:
        easygui.msgbox(three_peat(s), "Three-Peat - Result")
        s = easygui.enterbox("Enter a string (Cancel to quit):", "Three-Peat - Input")


def three_peat(s):
    """
    Repeats a string three times in lower case, title case, and upper case.

    :param s: The string to be repeated.
    :return: The three strings concatenated.
    :rtype: str
    """
    # TODO 1: Remove the line below and complete the function as described in the lab document.
    # return s

    return s.lower() + "\n" + s.title() + "\n" + s.upper()


def exercise2():
    """
    Character Frequency – In the space "TODO 2", complete the character_frequency
    function such that it accurately calculates the character frequency.  Discuss with
    your programming partner and other classmates why the given code is not accurate
    and how it might be improved.

    For reference, the average relative frequency in the English language of the
    letter 'e' is a bit over 12% and the letter 't' is about 9%.  Specifically, in
    your WarAndPeace.txt and MobyDick.txt data files, the frequency of the letter 'e'
    is 12.45% and 12.29%, respectively.  The frequency of the letter 't' in the same
    two files is 8.94% and 9.25%, respectively.

        https://en.wikipedia.org/wiki/Letter_frequency#Relative_frequencies_of_letters_in_the_English_language
    """
    print_exercise_name()

    # Get the first file name before testing the loop condition.
    filename = easygui.fileopenbox(default="../Data/*.txt", title="Character Frequency - File Open")
    # A valid filename (i.e., user did not click Cancel) is longer than one character.
    while filename is not None:
        # Read the contents of the file as a string.
        with open(filename) as data_file:
            data = data_file.read()
        # Show a message box with the base file name and letter frequency.
        char = "e"
        freq = character_frequency(char, data)
        easygui.msgbox("Frequency of the character '{}' in {}: {:.2%}.".format(
            char, filename, freq), "Character Frequency - Result")
        # Get another file name before testing the loop condition.
        filename = easygui.fileopenbox(default="../Data/*.txt", title="Character Frequency - File Open")


def character_frequency(char, data):
    """Calculates and returns the frequency of the given letter in the given data string.

    :param str char: The character to be counted.
    :param str data: The data in which to count the character.
    :return: The frequency of the character in the data, as a percentage.
    :rtype: float
    """
    # TODO 2: This is not accurate ... why? Can you fix it?
    # return data.count( char ) / len( data )

    # Two primary issues; upper vs. lower case instances of the character
    # and whitespace and punctuation inflating the length of the data.
    # Two basic approaches to solving the second part of the problem;
    # count only letters or ignore whitespace and punctuation.

    # First approach - count only letters in the data string.
    letter_count = 0
    for ch in data:
        if ch in string.ascii_letters:
            letter_count += 1
    return data.lower().count(char.lower()) / letter_count

    # Alternate approach - count the characters to be ignored:
    # ignore_count = 0
    # for char_to_ignore in string.whitespace + string.punctuation + string.digits:
    #     ignore_count += data.count( char_to_ignore )
    # return data.count( char ) / ( len( data ) - ignore_count )


def exercise3():
    """
    Pig Latin – For this exercise you will write a function that translates an English
    word to Pig Latin.  To simplify, implement only the first two rules on the
    wikihow.com page.

        https://en.wikipedia.org/wiki/Pig_Latin

        http://www.wikihow.com/Speak-Pig-Latin

    a.	In the space "TODO 3a", write a function named pig_latin that translates a
        single English word to Pig Latin.  Specifically, the function accepts a
        string parameter that is the English word to be translated and returns a
        string that is the Pig Latin equivalent.

    b.	In the space "TODO 3b", write code that repeatedly prompts the user for an
        English word using the easygui.enterbox and then displays an easygui.msgbox
        with the Pig Latin equivalent received from the pig_latin function.  This
        should continue until the user clicks the Cancel button on the easygui.enterbox.

    """
    print_exercise_name()

    # TODO 3b: Write code to use the function as described in the lab document.
    pass  # Remove the pass statement (and this comment) when writing your own code.

    word = easygui.enterbox("Enter a word (Cancel to quit):", "Pig Latin - Input")
    while word is not None:
        easygui.msgbox(pig_latin(word), "Pig Latin - Result")
        word = easygui.enterbox("Enter a word (Cancel to quit):", "Pig Latin - Input")


# TODO 3a: In the space below, write the function as described in the lab document.
def pig_latin(word):
    """
    Translates an English word to Pig Latin.

    Implements the first two rules from http://www.wikihow.com/Speak-Pig-Latin

    :param word: The English word to be translated.
    :return: The Pig Latin equivalent.
    :rtype: str
    """
    # return word

    # Strip any superfluous spaces or punctuation from the ends of the word before translating.
    word = word.strip(string.whitespace + string.punctuation)

    if len(word) > 0 and word[0] in VOWELS:
        # Words that start with a vowel just have "-yay" added to the end.
        return word + "-yay"
    else:
        if len(word) > 1 and word[1] in VOWELS:
            # Words that start with a single consonant move that consonant to the end and add "ay".
            return word[1:] + "-" + word[0] + "ay"
        else:
            # Words that begin with a consonant cluster (max two) move both consonants to the end.
            return word[2:] + "-" + word[:2] + "ay"


def exercise4():
    """
    Rövarspråket – For this exercise you will write a function that translates an
    English word to Rövarspråket.

        https://en.wikipedia.org/wiki/R%C3%B6varspr%C3%A5ket

    a.	In the space "TODO 4a", write a function named rovarspraket that translates
        a single English word to Rövarspråket.  Specifically, the function accepts a
        string parameter that is the English word to be translated and returns a string
        that is the Rövarspråket equivalent.

    b.	In the space "TODO 4b", write code that repeatedly prompts the user for an English
        word using the easygui.enterbox and then displays an easygui.msgbox with the
        Rövarspråket equivalent received from the rovarspraket function.  This should
        continue until the user clicks the Cancel button on the easygui.enterbox.
    """
    print_exercise_name()

    # TODO 4b: Write code to use the function as described in the lab document.
    pass  # Remove the pass statement (and this comment) when writing your own code.

    word = easygui.enterbox("Enter a word (Cancel to quit):", "Rovarspraket - Input")
    while word is not None:
        easygui.msgbox(rovarspraket(word), "Rovarspraket - Result")
        word = easygui.enterbox("Enter a word (Cancel to quit):", "Rovarspraket - Input")


# TODO 4a: In the space below, write the function as described in the lab document.
def rovarspraket(word):
    """
    Translates an English word to Rovarspraket.

    https://en.wikipedia.org/wiki/R%C3%B6varspr%C3%A5ket

    :param word: The English word to be translated.
    :return: The Rovarspraket equivalent.
    :rtype: str
    """
    # return word

    # Use the accumulator pattern to build the result.
    result = ""
    for char in word.lower():
        if char in VOWELS:
            # Vowels are left intact.
            result += char
        else:
            # Consonants are doubled with an "o" inserted between.
            result += char + "o" + char
    return result


def exercise5():
    """
    ROT13 – For this exercise you will write a function that encrypts a text string
    using a ROT 13 substitution cipher.  Since this algorithm is known to provide
    virtually no cryptographic security, and to expedite testing, your code should
    keep all non-alphabetic characters intact and preserve upper and lower case letters
    (that is, leave spaces and punctuation from the plain text in the cipher text,
    upper case letters in the plain text will encrypt to upper case letters in the
    cipher text, and lower case letters in the plain text will encrypt to lower case
    letters in the cipher text).

        https://en.wikipedia.org/wiki/ROT13

    a.	In the space "TODO 5a", write a function named rot13 that encrypts text using
        the ROT 13 substitution cipher.  Specifically, the function accepts a string
        as parameter that is the text to be encrypted and returns a string that is the
        result of applying the ROT 13 encryption algorithm.

    b.	In the space "TODO 5b", write code that that repeatedly prompts the user for a
        string using the easygui.enterbox and then displays an easygui.msgbox with the
        cipher text received from the rot13 function.  Additionally, show a second
        easygui.msgbox with the plain text resulting from applying the same rot13
        function to the cipher text (the ROT 13 encryption algorithm is its own
        inverse, so applying it a second time returns the original plain text).
        This should continue until the user clicks the Cancel button on the easyg.enterbox.
    """
    print_exercise_name()

    # TODO 5b: Write code to use the function as described in the lab document.
    pass  # Remove the pass statement (and this comment) when writing your own code.

    s = easygui.enterbox("Enter a string (Cancel to quit):", "ROT13 - Input", "Why did the chicken cross the road?")
    while s is not None:
        cipher_text = rot13(s)
        easygui.msgbox(cipher_text, "ROT13 - Encrypted")
        plain_text = rot13_v2(cipher_text)
        easygui.msgbox(plain_text, "ROT13 - Decrypted")
        s = easygui.enterbox("Enter a string (Cancel to quit):", "ROT13 - Input", "To get to the other side!")


# TODO 5a: In the space below, write the function as described in the lab document.
def rot13(s):
    """Encrypts a string using a ROT13 cipher.

    https://en.wikipedia.org/wiki/ROT13

    :param s: The string to be encrypted.
    :return: The encrypted string.
    :rtype: str
    """
    # Create lookup strings.
    rot13_upper = string.ascii_uppercase[13:] + string.ascii_uppercase[:13]
    rot13_lower = string.ascii_lowercase[13:] + string.ascii_lowercase[:13]

    # Use the accumulator pattern to build the result.
    result = ""
    for c in s:
        if c in string.ascii_uppercase:
            result += rot13_upper[string.ascii_uppercase.find(c)]
        elif c in string.ascii_lowercase:
            result += rot13_lower[string.ascii_lowercase.find(c)]
        else:
            result += c
    return result


def rot13_v2(s):
    """Encrypts a string using a ROT13 cipher.

    https://en.wikipedia.org/wiki/ROT13

    :param s: The string to be encrypted.
    :return: The encrypted string.
    :rtype: str
    """
    # Use the accumulator pattern to build the result.
    result = ""
    for c in s:
        unicode = ord(c)
        if c in string.ascii_uppercase:
            if unicode + 13 < ord(string.ascii_uppercase[-1]):
                unicode += 13
            else:
                unicode -= 13
        elif c in string.ascii_lowercase:
            if unicode + 13 < ord(string.ascii_lowercase[-1]):
                unicode += 13
            else:
                unicode -= 13
        result += chr(unicode)
    return result


"""
Challenge Exercises:

1.  Using the print_bar_chart function from Lab13, build a character frequency chart
    for all the letters in the alphabet.

    Note: If your Lab13 was working properly, you ought to be able to import it
    into this lab like so:

        from Lab13_NestedIteration import print_bar_chart
    
    or possibly:
    
        from Labs.Lab13_NestedIteration_Solution import print_bar_chart
    
    But you may prefer to simply copy and paste function into this file.

2.	Implement the remaining rules in your Pig Latin function.
"""


def exercise2_challenge():
    """
    Make a bar chart of the whole alphabet frequency
    """
    print_exercise_name()

    # Get the first file name before testing the loop condition.
    filename = easygui.fileopenbox(default="../Data/*.txt", title="Character Frequency - File Open")
    # A valid filename (i.e., user did not click Cancel) is longer than one character.
    while filename is not None:
        # Read the contents of the file as a string.
        with open(filename) as data_file:
            data = data_file.read()

        freqs = []
        for char in string.ascii_lowercase:
            freq = character_frequency(char, data)
            freqs.append(int(freq * 100))
        print_bar_chart(freqs)

        # Get another file name before testing the loop condition.
        filename = easygui.fileopenbox(default="../Data/*.txt", title="Character Frequency - File Open")


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
