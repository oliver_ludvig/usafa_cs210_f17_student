#!/usr/bin/env python3
"""
Lab12 Iteration 2 Solution
CS 210, Introduction to Programming
"""

import easygui
import random
import turtle

__author__ = "Firstname Lastname"
__instructor__ = "Rank Lastname"
__date__ = "dd mmm yyyy"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
-	Read Lesson 11: Iteration from our online textbook
-	Watch the embedded video on the while statement
Lesson Objectives
-	Reinforce functions, parameters, return values, and selection statements
-	Reinforce definite iteration with the for loop
-	Introduce indefinite iteration with the while loop
"""

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960  # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 // 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = WIDTH // 30  # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = MARGIN // 2  # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = False  # Set to True for fast, non-animated turtle movement.


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print( __author__, __doc__ )

    # Call each individual exercise; comment/un-comment these lines as you work.
    # exercise0()
    # high_low()
    turtle_race()
    # olympic_rings()


def exercise0():
    """
    Example code.

    Refer to the previous lesson for a review of for loops and while loops.
    """
    print_exercise_name()

    # Count by 3s with for loop
    for i in range( 3, 20, 3 ):
        print( i, " ", end="" )
    print()

    # Equivalent count by 3s with while loop
    n = 3
    while n < 20:
        print( n, " ", end="" )
        n = n + 3
    print()

    # Three ways to iterate over a list
    phonetic = [ "alpha", "bravo", "charlie", "delta", "echo" ]
    print( "Contents of phonetic:", phonetic )
    for sound in phonetic:  # Variable sound takes on each value in the list
        print( sound )
    for i in range( len( phonetic ) ):  # Variable i takes on the position in the list
        sound = phonetic[ i ]  # Extract a particular sound from the list
        print( sound )
    i = 0
    while i < len( phonetic ):  # Another way of marching through the position in the list
        sound = phonetic[ i ]
        print( sound )
        i = i + 1
    while len( phonetic ) > 0:
        sound = phonetic.pop( 0 )  # Actually remove the first item from the list
        print( sound )
    print( "What's left in phonetic:", phonetic )


def high_low():
    """
    High Low – For this exercise you do not need to write an additional function;
    all of the necessary code is to be written directly in the existing high_low
    function, in the location indicated by the comment "TODO".

    a.	Add a condition to the while loop so the game stops when the user's
        guess is correct.

    b.	Add a selection statement after the user enters their guess so they are told
        if the guess is too high or too low.

    c.	Add code after the loop to tell the user they have won the game and show
        the secret number.

    d.	Add code to count the number of guesses and show this information to the user.

    e.	Add to the loop condition so the user only has seven guesses to find a value
        in the range [0,100].

    f.	Add code after the loop to tell the user they have won or lost the game
        (be careful to properly handle the situation where the user is correct on
        their last allowed guess).

    g.	Add code to keep track of the two numbers most closely bounding what the user has guessed
        so far, showing this information each time the user is prompted for their next guess.

    h.	A complete interaction with the program is shown below.

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |    Enter a guess between 1 and 100:   |
            |  [ 50                            ]    |
            |        +------+       +--------+      |
            |        |  OK  |       | Cancel |      |
            |        +------+       +--------+      |
            +---------------------------------------+

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |   Your guess of 50 is too high.       |
            |             +------+                  |
            |             |  OK  |                  |
            |             +------+                  |
            +---------------------------------------+

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |    Enter a guess between 1 and 49:    |
            |  [ 25                            ]    |
            |        +------+       +--------+      |
            |        |  OK  |       | Cancel |      |
            |        +------+       +--------+      |
            +---------------------------------------+

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |   Your guess of 25 is too low.        |
            |             +------+                  |
            |             |  OK  |                  |
            |             +------+                  |
            +---------------------------------------+

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |    Enter a guess between 26 and 49:   |
            |  [ 37                            ]    |
            |        +------+       +--------+      |
            |        |  OK  |       | Cancel |      |
            |        +------+       +--------+      |
            +---------------------------------------+

            +-----------------------------------------+
            |                             [_] [ ] [X] |
            +-----------------------------------------+
            |  You win! You guessed 37 in 3 guesses.  |
            |             +------+                    |
            |             |  OK  |                    |
            |             +------+                    |
            +-----------------------------------------+

    """
    print_exercise_name()

    # Create a random number in the range [1,100] for the user to guess.
    secret_number = random.randint( 1, 100 )
    # For debugging purposes only, it's nice to know the secret.
    print( "Sshhh...secret number:", secret_number, flush=True )  # Add the flush to ensure there's no buffering.

    # TODO: Implement the High Low guessing game as described
    # while True:
    #     guess = easygui.integerbox("Enter a guess:", "Input", lowerbound=1, upperbound=100)
    #     if guess is None:
    #         break  # Exit while loop
    #     easygui.msgbox("Your guess is {}.".format(guess), "Result")

    guess = 0  # Anything that can't be the secret number, just to get the loop started.
    guess_count = 0
    lower = 1
    upper = 100
    while guess != secret_number and guess_count < 7:
        # Get a guess and count it.
        guess = easygui.integerbox( "Enter a guess between {} and {}:".format( lower, upper ),
                                    "Input", lowerbound=lower, upperbound=upper )
        if guess is None:
            break  # Exit while loop

        guess_count += 1

        # Respond appropriately to this guess.
        if guess < secret_number:
            lower = guess + 1
            easygui.msgbox( "Your guess of {} is too low.".format( guess ), "Result" )
        elif guess > secret_number:
            upper = guess - 1
            easygui.msgbox( "Your guess of {} is too high.".format( guess ), "Result" )

    # After the loop terminates, determine if the user won or lost the game.
    if guess == secret_number:
        easygui.msgbox( "You win! You guessed {} in {} guesses.".format( guess, guess_count ), "Result" )
    else:
        easygui.msgbox( "Too many guesses, you lose.\n\nThe number was {}.".format( secret_number ), "Result" )


def turtle_race():
    """Implement a simple turtle race from left to right across the screen."""
    # Create the turtle screen and two turtles (leave this as the first line).
    screen, artist, writer = turtle_setup()

    # Rename the artist turtle and move her to the left, above the x-axis.
    flojo = artist  # Flo-Jo, https://en.wikipedia.org/wiki/Florence_Griffith_Joyner
    flojo.shape( "turtle" )
    flojo.color( "blue" )  # USA!
    flojo.penup()
    flojo.setposition( -WIDTH // 2 + MARGIN, MARGIN * 2 )
    flojo.setheading( 0 )
    flojo.pendown()

    # Create a new turtle, below the x-axis, to race against the turtle formerly known as artist.
    usain = turtle.Turtle()  # Usain Bolt, https://en.wikipedia.org/wiki/Usain_Bolt
    usain.shape( "turtle" )
    usain.color( "green" )  # Jamaica
    usain.penup()
    usain.setposition( -WIDTH // 2 + MARGIN, -MARGIN * 2 )
    usain.setheading( 0 )
    usain.pendown()

    # TODO: Implement the turtle race as described
    writer.write( "And they're off . . .", align="center", font=("Times", FONT_SIZE, "bold") )
    # while True:
    #     flojo.forward( random.randint( MARGIN // 4, MARGIN ) )
    #     usain.forward( random.randint( MARGIN // 4, MARGIN ) )

    # Calculate the finish line and get the turtles' initial positions.
    finish_line = WIDTH // 2 - MARGIN
    flojo_x, flojo_y = flojo.position()
    usain_x, usain_y = usain.position()

    # Loop until the race is won or lost.
    while flojo_x < finish_line and usain_x < finish_line and \
            abs( flojo_y ) <= HEIGHT // 2 - MARGIN and abs( usain_y ) <= HEIGHT // 2 - MARGIN:
        # Move the turtles.
        flojo.forward( random.randint( MARGIN // 4, MARGIN ) )
        usain.forward( random.randint( MARGIN // 4, MARGIN ) )

        # Randomly have the turtles turn a few degrees.
        flojo.right( random.choice( [ -5, 0, 0, 0, 5 ] ) )
        usain.right( random.choice( [ -5, 0, 0, 0, 5 ] ) )

        # Update the turtle positions.
        flojo_x, flojo_y = flojo.position()
        usain_x, usain_y = usain.position()

    writer.clear()
    if flojo_x > usain_x and abs( flojo_y ) <= HEIGHT // 2 - MARGIN or abs( usain_y ) > HEIGHT // 2 - MARGIN:
        writer.write( "FloJo wins!", align="center", font=("Times", FONT_SIZE, "bold") )
    else:
        writer.write( "Usain wins!", align="center", font=("Times", FONT_SIZE, "bold") )

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


def olympic_rings():
    """
    You will need to retrieve the Lab12_exercise_3.pdf file for detailed instructions
    and detailed screenshots of the images you will be producing in this exercise.
    """
    print_exercise_name()

    # Create the turtle screen and two turtles (leave this as the first line).
    screen, artist, writer = turtle_setup()

    # TODO 3b
    writer.write( "Olympic Rings", align="center", font=("Courier", FONT_SIZE, "bold") )

    # There are three circles and four margins in the top row.
    diameter = ( WIDTH - MARGIN * 4 ) // 3

    # Draw the Olympic rings.
    artist.pensize( 2 )  # Not required; just looks better for the screen capture.
    r = diameter // 2
    x = -r * 2 - MARGIN
    y = r // 3
    for color in [ "blue", "yellow", "black", "green", "red" ]:  # This line given.
        artist.color( color )  # This line given.
        draw_circle( artist, x, y, r )  # This line given.
        x += r + MARGIN // 2
        y *= -1  # Students will likely use an if-else statement.

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


# TODO 3a
def draw_circle( tom, x, y, radius ):
    """Draws a circle with the given radius centered at the given (x,y) coordinate.

    :param turtle.Turtle tom: The turtle to do the drawing.
    :param int x: The x-coordinate of the center of the circle.
    :param int y: The y-coordinate of the center of the circle.
    :param int radius: The radius of the circle.
    :return: None
    """
    tom.penup()
    tom.setposition( x, y - radius )
    tom.pendown()
    tom.circle( radius )


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def turtle_setup():
    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    #  ___   ___     _  _  ___ _____    __  __  ___  ___ ___ _____   __
    # |   \ / _ \   | \| |/ _ \_   _|  |  \/  |/ _ \|   \_ _| __\ \ / /
    # | |) | (_) |  | .` | (_) || |    | |\/| | (_) | |) | || _| \ V /
    # |___/ \___/   |_|\_|\___/ |_|    |_|  |_|\___/|___/___|_|   |_|
    #  _____ _  _ ___ ___    ___ _   _ _  _  ___ _____ ___ ___  _  _
    # |_   _| || |_ _/ __|  | __| | | | \| |/ __|_   _|_ _/ _ \| \| |
    #   | | | __ || |\__ \  | _|| |_| | .` | (__  | |  | | (_) | .` |
    #   |_| |_||_|___|___/  |_|  \___/|_|\_|\___| |_| |___\___/|_|\_|
    #
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup( WIDTH, HEIGHT, MARGIN, MARGIN )
    screen.bgcolor( "SkyBlue" )

    # Create two turtles, one for drawing and one for writing.
    turtle.TurtleScreen._RUNNING = True  # Get around bug in v3.5.2 http://bugs.python.org/issue26571
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape( "turtle" )

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay( 0 )
        artist.hideturtle()
        artist.speed( "fastest" )
        writer.hideturtle()
        writer.speed( "fastest" )

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading( 90 )  # Straight up, which makes it look sort of like a cursor.
    writer.penup()  # A turtle's pen does not have to be down to write text.
    writer.setposition( 0, HEIGHT // 2 - FONT_SIZE * 2 )  # Centered at top of the screen.

    return screen, artist, writer


def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo( inspect.currentframe().f_back ).function
        doc = inspect.getdoc( globals()[ name ] )
        print( '\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format( name, "=" * len( name ), doc ), flush=True )
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
