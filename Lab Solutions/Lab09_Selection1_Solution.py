#!/usr/bin/env python3
"""
Lab09 Selection
CS 210, Introduction to Programming
"""

__author__ = "Firstname Lastname"
__instructor__ = "Rank Lastname"
__date__ = "dd mmm yyyy"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
-	Read Lesson 9: Selection from our online textbook
-	Watch the embedded videos on Boolean Expressions, Binary Selection, and Unary Selection

Lesson Objectives
-	Reinforce functions, parameters, and return values
-	Introduce Boolean expressions, logical operators, and selection


"""

# Use some alternate import statements just to say we did.
from easygui import integerbox, msgbox
from math import hypot
import random
import turtle

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960  # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 // 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = WIDTH // 30  # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = MARGIN // 2  # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = True  # Set to True for fast, non-animated turtle movement.


def main():
    """Main program to test solutions for each problem."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    exercise0()
    exercise1()
    exercise2()
    exercise3()
    exercise4()
    exercise5()
    exercise6()
    exercise7()


def exercise0():
    """Example code."""
    print_exercise_name()

    answer = random.randint(1, 10)  # Get random answer
    print("Pssst....the secret answer is {}.".format(answer))  # Print to console for testing purposes
    n = integerbox("Guess a number between 1 and 10:", "Guess", None, 1, 10)  # Get user's guess

    # Set up a different prompt depending on whether or not they got the answer right.
    if answer == n:
        prompt = "You guessed right! The answer was {}.".format(answer)
    else:
        prompt = "Sorry, you guessed {}, but the answer was {}.".format(n, answer)

    msgbox(prompt)  # Show the prompt


def exercise1():
    """
    Interact with the user and test the even_odd function.

    a.	In the space "TODO 1a", write a function named even_odd that receives
        an integer value as a parameter and returns one of the strings "Even"
        or "Odd", based on whether the parameter value is even or odd.

    b.  In the space "TODO 1b", add three more tests to those provided that show
        that your function works as expected.

        Note: This kind of testing is called Unit Testing in computer science
        and is a great way to verify that your code works the way it is supposed
        to work.  When working on your own projects, you may want to write these
        unit tests before you even start writing your actual functions.  They can
        guide your devleopment and debugging as you write new code.

    c.	In the space "TODO 1c", write code that uses the easygui.integerbox
        to obtain an integer value from the user (be sure to use reasonable values
        for the lower and upper bound parameters). Use the value entered as the
        parameter to the even_odd function and then display the results in an
        easygui.msgbox as shown below.

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |          Enter number:                |
            |  [ 42                            ]    |
            |        +------+       +--------+      |
            |        |  OK  |       | Cancel |      |
            |        +------+       +--------+      |
            +---------------------------------------+

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |         The number 42 is Even.        |
            |             +------+                  |
            |             |  OK  |                  |
            |             +------+                  |
            +---------------------------------------+

    """
    print_exercise_name()

    # Test your function
    if even_odd(2) != "Even":
        print("Incorrect: 2")
    if even_odd(9999) != "Odd":
        print("Incorrect: 9999")
    # TODO 1b: Add three more examples to the two above that test your function
    if even_odd(0) != "Even":
        print("Incorrect: 0")
    if even_odd(-2) != "Even":
        print("Incorrect: -2")
    if even_odd(-99) != "Odd":
        print("Incorrect: -99")

    # TODO 1c: Write code to use the even_odd function as described
    n = integerbox("Enter number:", "Input", None, -2 ** 31, 2 ** 31)
    msgbox("The number {} is {}.".format(n, even_odd(n)), "Result")


# TODO 1a: In the space below, write the even_odd function as described
def even_odd(number):
    """
    Determine if the given number is even or odd.

    :param int number: The number to be tested for oddness.
    :return: "Even" or "Odd".
    :rtype: str
    """
    if number % 2 == 0:
        return "Even"
    else:
        return "Odd"


def exercise2():
    """
    Interact with the user and test the pass_fail function.

    a.	In the space "TODO 2a", write a function named pass_fail that receives
        an integer value as a parameter and returns one of the strings "Pass"
        or "Fail", based on whether the parameter value is greater than or
        equal to 70 (i.e., 70 and above is a passing grade).
    b.  In the space "TODO 2b", add three more tests to those provided that show
        that your function works as expected.
    c.	In the space "TODO 2c", write code that uses the easygui.integerbox
        to obtain an integer value from the user (be sure to use reasonable
        values for the lower and upper bound parameters). Use the value entered
        as the parameter to the pass_fail function and then display the results
        in an easygui.msgbox as shown below.

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |          Enter number:                |
            |  [ 86                            ]    |
            |        +------+       +--------+      |
            |        |  OK  |       | Cancel |      |
            |        +------+       +--------+      |
            +---------------------------------------+


            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |      86 earns a grade of Pass.        |
            |             +------+                  |
            |             |  OK  |                  |
            |             +------+                  |
            +---------------------------------------+
    """
    print_exercise_name()

    # Test your function
    if pass_fail(10) != "Fail":
        print("Incorrect: 10")
    if pass_fail(100) != "Pass":
        print("Incorrect: 100")
    # TODO 2b: Add three more examples to the two above that test your function
    if pass_fail(69) != "Fail":
        print("Incorrect: 69")
    if pass_fail(70) != "Pass":
        print("Incorrect: 70")
    if pass_fail(71) != "Pass":
        print("Incorrect: 71")

    # TODO 2c: Write code to use the pass_fail function as described
    n = integerbox("Enter score:", "Input", None, 0, 100)
    msgbox("{} earns a grade of {}.".format(n, pass_fail(n)), "Result")


# TODO 2a: In the space below, write the pass_fail function as described
def pass_fail(score):
    """Determine if the given score is a pass or fail.

    :param int score: The score to be tested.
    :return: "Pass" or "Fail".
    :rtype: str
    """
    if score >= 70:
        return "Pass"
    else:
        return "Fail"


def exercise3():
    """
    Interact with the user and test the residence_hall function.

    a.	In the space "TODO 3a", write a function named residence_hall that receives
        an integer value as a parameter and returns one of the strings "Sijan" or
        "Vandy", based on where that squad lives.
    b.  In the space "TODO 3b", add three more tests to those provided that show
        that your function works as expected.
    c.	In the space "TODO 3c", write code that uses the easygui.integerbox
        to obtain an integer value from the user (be sure to use reasonable
        values for the lower and upper bound parameters). Use the value entered
        as the parameter to the residence_hall function and then display the
        results in an easygui.msgbox as shown below.

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |     Enter a squad number:             |
            |  [ 37                            ]    |
            |        +------+       +--------+      |
            |        |  OK  |       | Cancel |      |
            |        +------+       +--------+      |
            +---------------------------------------+

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |      Squad 37 lives in Sijan.         |
            |             +------+                  |
            |             |  OK  |                  |
            |             +------+                  |
            +---------------------------------------+
    """
    print_exercise_name()

    # Test your function
    if residence_hall(1) != "Vandy":
        print("Incorrect: 1")
    if residence_hall(40) != "Sijan":
        print("Incorrect: 40")
    # TODO 3b: Add three more examples to the two above that test your function
    if residence_hall(22) != "Vandy":
        print("Incorrect: 22")
    if residence_hall(23) != "Vandy":
        print("Incorrect: 23")
    if residence_hall(24) != "Sijan":  # Or so the cadets have told me...
        print("Incorrect: 24")

    # TODO 3c: Write code to use the residence_hall function as described
    squad = integerbox("Enter squad number:", "Input", None, 1, 40)
    msgbox("Squad {} lives in {}.".format(squad, residence_hall(squad)), "Result")


# TODO 3a: In the space below, write the residence_hall function as described
def residence_hall(squad_number):
    """Determine the dorm where the given squad lives.

    :param int squad_number: The squad number, [1,40].
    :return: The dorm where the squad lives, "Sijan" or "Vandy".
    :rtype: str
    """
    # Why 23? Because the cadets told me so.
    if squad_number <= 23:
        return "Vandy"
    else:
        return "Sijan"


def exercise4():
    """
    Interact with the user and test the days_in_year function.

    a.	In the space "TODO 4a", write a function named days_in_year that receives
        an integer value as a parameter and returns an integer values based on
        whether the given year is a leap year (i.e., a leap year has 366 days
        while a non-leap year has 365 days).  To learn about leap years, go to
        http://www.timeanddate.com/date/leapyear.html
    b.  In the space "TODO 4b", add three more tests to those provided that show
        that your function works as expected.
    c.	In the space "TODO 4c", write code that uses the easygui.integerbox
        to obtain an integer value from the user (be sure to use reasonable
        values for the lower and upper bound parameters). Use the value entered
        as the parameter to the days_in_year function and then display the
        results in an easygui.msgbox as shown below.

            +---------------------------------------+
            |  Input                    [_] [ ] [X] |
            +---------------------------------------+
            |     Enter year:                       |
            |  [ 37                            ]    |
            |        +------+       +--------+      |
            |        |  OK  |       | Cancel |      |
            |        +------+       +--------+      |
            +---------------------------------------+

            +---------------------------------------+
            |  Result                   [_] [ ] [X] |
            +---------------------------------------+
            |  The year 2017 has 365 days.          |
            |             +------+                  |
            |             |  OK  |                  |
            |             +------+                  |
            +---------------------------------------+
    """
    print_exercise_name()

    # Test your function
    if days_in_year(2016) != 366:
        print("Incorrect: 2016")
    if days_in_year(1900) != 365:
        print("Incorrect: 1900")
    # TODO 4b: Add three more examples to the two above that test your function
    if days_in_year(2000) != 366:
        print("Incorrect: 2000")
    if days_in_year(2017) != 365:
        print("Incorrect: 2017")
    if days_in_year(2020) != 366:
        print("Incorrect: 2020")

    # TODO 4c: Write code to use the days_in_year function as described
    year = integerbox("Enter year:", "Input", 2017, 1582, 2 ** 31)
    msgbox("The year {} has {} days.".format(year, days_in_year(year)), "Result")

    # The special case years on the page http://www.timeanddate.com/date/leapyear.html
    # for year in range( 1800, 2501, 100 ):
    #     print( "The year {} has {} days.".format( year, days_in_year( year ) ) )


# TODO 4a: In the space below, write the days_in_year function as described
def days_in_year(year):
    """Determines and returns the number of days in the year, based on standard or leap year.

    :param int year: The year for which the number of days is to be determined.
    :return: The number of days in the year, 365 or 366.
    :rtype: int
    """
    if year % 4 == 0 and (year % 100 != 0 or year % 400 == 0):
        return 366
    else:
        return 365


def exercise5():
    """
    Interact with the user and test the count_multiples function.

    a.	In the space "TODO 5a", write a function named count_multiples that receives
        three integer values as parameters: a start value, a stop value, and a divisor.
        The function then counts and returns the number of values between the start
        and stop values, inclusive, that are evenly divisible by the divisor (i.e., the
        number of multiples of the divisor in the given range).
    b.  In the space "TODO 5b", add three more tests to those provided that show
        that your function works as expected.
    c.	In the space "TODO 5c", write code that uses the easygui.integerbox
        to obtain integer values from the user for the start, stop, and divisor
        (be sure to use reasonable values for the lower and upper bound parameters).
        Use the values entered as the parameters to the count_multiples function and
        then display the results in an easygui.msgbox as shown below.

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |       Enter a start value:            |
            |  [ 4                            ]     |
            |        +------+       +--------+      |
            |        |  OK  |       | Cancel |      |
            |        +------+       +--------+      |
            +---------------------------------------+

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |       Enter a stop value:             |
            |  [ 19                           ]     |
            |        +------+       +--------+      |
            |        |  OK  |       | Cancel |      |
            |        +------+       +--------+      |
            +---------------------------------------+

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |       Enter a divisor value:          |
            |  [ 6                            ]     |
            |        +------+       +--------+      |
            |        |  OK  |       | Cancel |      |
            |        +------+       +--------+      |
            +---------------------------------------+

            +-----------------------------------------------------+
            |                                         [_] [ ] [X] |
            +-----------------------------------------------------+
            |   There are 3 multiples of 6 in the range [4, 19].  |
            |                    +------+                         |
            |                    |  OK  |                         |
            |                    +------+                         |
            +-----------------------------------------------------+
    """
    print_exercise_name()

    # Test your function
    if count_multiples(4, 19, 6) != 3:
        print("Incorrect: 4, 19, 6")
    if count_multiples(100, 200, 3) != 33:
        print("Incorrect: 100, 200, 3")
    # TODO 5b: Add three more examples to the two above that test your function
    if count_multiples(30, 50, 11) != 2:
        print("Incorrect: 30, 50, 11")
    if count_multiples(1, 10, 1) != 10:
        print("Incorrect: 1, 10, 1")
    if count_multiples(1, 10, 2) != 5:
        print("Incorrect: 1, 10, 2")

    # TODO 5b: Write code to use the count_multiples function as described
    start = integerbox("Enter start value:", "Input", None, -2 ** 31, 2 ** 31)
    stop = integerbox("Enter stop value:", "Input", None, -2 ** 31, 2 ** 31)
    step = integerbox("Enter divisor value:", "Input", None, -2 ** 31, 2 ** 31)
    msgbox("There are {} multiples of {} in the range [{},{}].".format(
        count_multiples(start, stop, step), step, start, stop), "Result")


# TODO 5a: In the space below, write the count_multiples function as described
def count_multiples(start, stop, divisor):
    """
    Count and return the number of values between start and stop, inclusive,
    evenly divisible by divisor.

    :param int start: The start value for the range, inclusive.
    :param int stop: The stop value for the range, inclusive.
    :param int divisor: The divisor to be counted.
    :return: The number of values in the range [start, stop] evenly divisible by divisor.
    :rtype: int
    """
    count = 0
    for value in range(start, stop + 1):
        if value % divisor == 0:
            count += 1
    return count


"""
Be sure to scroll down to exercise 6 and 7.
"""

# Define a random size box/target in a random location for use in the next exercise.
#      ___   ___     _  _  ___ _____    __  __  ___  ___ ___ _____   __
#     |   \ / _ \   | \| |/ _ \_   _|  |  \/  |/ _ \|   \_ _| __\ \ / /
#     | |) | (_) |  | .` | (_) || |    | |\/| | (_) | |) | || _| \ V /
#     |___/ \___/   |_|\_|\___/ |_|    |_|  |_|\___/|___/___|_|   |_|
#  _____ _  _ ___ ___ ___    ___  ___ ___ ___ _  _ ___ _____ ___ ___  _  _ ___
# |_   _| || | __/ __| __|  |   \| __| __|_ _| \| |_ _|_   _|_ _/ _ \| \| / __|
#   | | | __ | _|\__ \ _|   | |) | _|| _| | || .` || |  | |  | | (_) | .` \__ \
#   |_| |_||_|___|___/___|  |___/|___|_| |___|_|\_|___| |_| |___\___/|_|\_|___/
#
BOX_W = random.randint(WIDTH // 8, WIDTH // 4)  # The width of the box/target.
BOX_H = random.randint(HEIGHT // 8, HEIGHT // 4)  # The height of the box/target.
# The x-coordinate of the lower-left corner of the box/target.
BOX_X = random.randint(-WIDTH // 2 + MARGIN, WIDTH // 2 - BOX_W - MARGIN)
# The y-coordinate of the lower-left corner of the box/target.
BOX_Y = random.randint(-HEIGHT // 2 + MARGIN, HEIGHT // 2 - BOX_H - MARGIN)


def exercise6():
    """
    Use the screen and turtle defined below to solve the given exercise.

    a.	In the space "TODO 6", write the necessary Boolean condition to determine
        if the (x, y) coordinate passed to the box_click function is a Hit or Miss.
        A click anywhere within the boundaries of the box is a hit.

    Note: You do not need to add or modify any code in the exercise6() function
    and you only need to write the Boolean condition in the box_click function,
    just under the  "TODO 6"comment.

    Note: Use the values defined immediately before the exercise6() function; since
    they are random, you will not be able to write literal values and expect your
    code to work from one execution to the next.

    b.	Run the exercise6() function and click in several locations to verify your
        logic is correct, as shown below.


            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |               Hit!                    |    +-------------------+
            |             +------+                  |    |                   |
            |             |  OK  |                  |    |            x      |
            |             +------+                  |    +-------------------+
            +---------------------------------------+

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |               Miss!                   |    +-------------------+
            |             +------+                  |    |                   |
            |             |  OK  |                  |    |                   | x
            |             +------+                  |    +-------------------+
            +---------------------------------------+


    """
    print_exercise_name()
    #  ___   ___     _  _  ___ _____    __  __  ___  ___ ___ _____   __
    # |   \ / _ \   | \| |/ _ \_   _|  |  \/  |/ _ \|   \_ _| __\ \ / /
    # | |) | (_) |  | .` | (_) || |    | |\/| | (_) | |) | || _| \ V /
    # |___/ \___/   |_|\_|\___/ |_|    |_|  |_|\___/|___/___|_|   |_|
    #  _____ _  _ ___ ___    ___ _   _ _  _  ___ _____ ___ ___  _  _
    # |_   _| || |_ _/ __|  | __| | | | \| |/ __|_   _|_ _/ _ \| \| |
    #   | | | __ || |\__ \  | _|| |_| | .` | (__  | |  | | (_) | .` |
    #   |_| |_||_|___|___/  |_|  \___/|_|\_|\___| |_| |___\___/|_|\_|
    #
    # Create the turtle screen and two turtles (leave this as the first line).
    screen, artist, writer = turtle_setup()

    # Give the user some instructions.
    writer.write("Try to click in the box...", align="center", font=("Arial", FONT_SIZE, "bold"))

    # Draw a box/target in the middle of the screen.
    artist.color("blue")
    artist.penup()
    artist.setposition(BOX_X, BOX_Y)
    artist.pendown()
    artist.begin_fill()
    for _ in range(2):
        artist.forward(BOX_W)
        artist.left(90)
        artist.forward(BOX_H)
        artist.left(90)
    artist.end_fill()

    # Tell the screen to call the click() function when the user clicks on the screen.
    screen.onclick(box_click)

    # Rather than exitonclick(), enter the main event loop to listen for clicks.
    screen.mainloop()


def box_click(x, y):
    """Display a Hit or Miss message if the (x,y) coordinate is in or out of the box drawn in exercise6().

    Note: The "screen.onclick( box_click )" function call in exercise6() results in
    this function being called automatically when the turtle screen is clicked.

    :param int x: The x-coordinate of the click.
    :param int y: The y-coordinate of the click.
    """
    # TODO 6: Replace True in the line below with a single condition to display the appropriate message.
    # TODO 6: Use the existing definitions of BOX_X, BOX_Y, BOX_W, and BOX_H in your selection statement.
    # if True:

    # In the line below, PyCharm/Python suggests "simplifying the chained expression".
    # if x > BOX_X and x < BOX_X + BOX_W and y > BOX_Y and y < BOX_Y + BOX_H:

    # This is how PyCharm/Python prefers to see the expression (unique to Python?):
    if BOX_X < x < BOX_X + BOX_W and BOX_Y < y < BOX_Y + BOX_H:
        msgbox("Hit!", "Result")
    else:
        msgbox("Miss", "Result")


# Define random location/size for the earth and moon for use in the next exercise.
#      ___   ___     _  _  ___ _____    __  __  ___  ___ ___ _____   __
#     |   \ / _ \   | \| |/ _ \_   _|  |  \/  |/ _ \|   \_ _| __\ \ / /
#     | |) | (_) |  | .` | (_) || |    | |\/| | (_) | |) | || _| \ V /
#     |___/ \___/   |_|\_|\___/ |_|    |_|  |_|\___/|___/___|_|   |_|
#  _____ _  _ ___ ___ ___    ___  ___ ___ ___ _  _ ___ _____ ___ ___  _  _ ___
# |_   _| || | __/ __| __|  |   \| __| __|_ _| \| |_ _|_   _|_ _/ _ \| \| / __|
#   | | | __ | _|\__ \ _|   | |) | _|| _| | || .` || |  | |  | | (_) | .` \__ \
#   |_| |_||_|___|___/___|  |___/|___|_| |___|_|\_|___| |_| |___\___/|_|\_|___/
#
EARTH_R = random.randint(WIDTH // 16, WIDTH // 8)
EARTH_X = random.randint(-WIDTH // 2 + EARTH_R + MARGIN, -EARTH_R)  # Left half of the screen.
EARTH_Y = random.randint(-HEIGHT // 2 + EARTH_R + MARGIN, HEIGHT // 2 - EARTH_R - MARGIN)
MOON_R = int(EARTH_R * 0.2726)  # The moon is just over a quarter of the size of earth.
MOON_X = random.randint(0, WIDTH // 2 - MOON_R - MARGIN)  # Right half of the screen.
MOON_Y = random.randint(-HEIGHT // 2 + MOON_R + MARGIN, HEIGHT // 2 - MOON_R - MARGIN)


def exercise7():
    """
    Celestial Target: Use the screen and turtle defined below to solve the given exercise.

    a.	In the space in "TODO 7", write the necessary Boolean condition to determine
        if the (x, y) coordinate passed to the celestial_click function is a Hit or Miss.
        In this case, a click on either the Earth or the Moon is considered a hit.

    Note: You do not need to add or modify any code in the exercise7() function and you
    only need to write the Boolean condition in the celestial_click function, just under the
    "TODO 7"comment.

    Note: Use the values defined immediately before the exercise7() function; since
    they are random, you will not be able to write literal values and expect your code
    to work from one execution to the next.

    b.	Run the exercise7() function and click in several locations to verify your
        logic is correct, as shown below.

            +---------------------------------------+     ___
            |                           [_] [ ] [X] |   /    \
            +---------------------------------------+  |    x |
            |               Hit!                    |   \____/
            |             +------+                  |
            |             |  OK  |                  |          ( )
            |             +------+                  |
            +---------------------------------------+

            +---------------------------------------+     ___
            |                           [_] [ ] [X] |   /    \
            +---------------------------------------+  |      |
            |               Hit!                    |   \____/
            |             +------+                  |
            |             |  OK  |                  |          (x)
            |             +------+                  |
            +---------------------------------------+


            +---------------------------------------+     ___
            |                           [_] [ ] [X] |   /    \
            +---------------------------------------+  |      |
            |               Miss!                   |   \____/    x
            |             +------+                  |
            |             |  OK  |                  |          ( )
            |             +------+                  |
            +---------------------------------------+



    """
    print_exercise_name()
    #  ___   ___     _  _  ___ _____    __  __  ___  ___ ___ _____   __
    # |   \ / _ \   | \| |/ _ \_   _|  |  \/  |/ _ \|   \_ _| __\ \ / /
    # | |) | (_) |  | .` | (_) || |    | |\/| | (_) | |) | || _| \ V /
    # |___/ \___/   |_|\_|\___/ |_|    |_|  |_|\___/|___/___|_|   |_|
    #  _____ _  _ ___ ___    ___ _   _ _  _  ___ _____ ___ ___  _  _
    # |_   _| || |_ _/ __|  | __| | | | \| |/ __|_   _|_ _/ _ \| \| |
    #   | | | __ || |\__ \  | _|| |_| | .` | (__  | |  | | (_) | .` |
    #   |_| |_||_|___|___/  |_|  \___/|_|\_|\___| |_| |___\___/|_|\_|
    #
    # Create the turtle screen and two turtles (leave this as the first line).
    screen, artist, writer = turtle_setup()

    # Give the user some instructions.
    writer.write("Try to click on the Earth or the Moon...",
                 align="center", font=("Arial", FONT_SIZE, "bold"))

    # Draw a blue dot for the earth and a silver dot for the moon.
    artist.penup()
    artist.color("Blue")
    artist.setposition(EARTH_X, EARTH_Y)
    artist.dot(EARTH_R * 2)
    artist.color("LightGray")
    artist.setposition(MOON_X, MOON_Y)
    artist.dot(MOON_R * 2)

    # Tell the screen to call the click() function when the user clicks on the screen.
    screen.onclick(celestial_click)

    # Rather than exitonclick(), enter the main event loop to listen for clicks.
    screen.mainloop()


def celestial_click(x, y):
    """Display a Hit or Miss message if the (x,y) coordinate hit the earth or moon drawn in exercise7().

    Note: The "screen.onclick( planet_click )" function call in exercise7() results in
    this function being called automatically when the turtle screen is clicked.

    :param int x: The x-coordinate of the click.
    :param int y: The y-coordinate of the click.
    """
    # TODO 7: Replace True in the line below with a single condition to display the appropriate message.
    # TODO 7: Use the existing definitions of EARTH_X, EARTH_Y, EARTH_R, MOON_X, MOON_Y, and MOON_R.
    # if True:
    if hypot(x - EARTH_X, y - EARTH_Y) < EARTH_R or hypot(x - MOON_X, y - MOON_Y) < MOON_R:
        msgbox("Hit!", "Result")
    else:
        msgbox("Miss", "Result")


"""
Challenge Exercises:

1.	Complete unfinished exercises from the previous lab.
"""


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def turtle_setup():
    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    #  ___   ___     _  _  ___ _____    __  __  ___  ___ ___ _____   __
    # |   \ / _ \   | \| |/ _ \_   _|  |  \/  |/ _ \|   \_ _| __\ \ / /
    # | |) | (_) |  | .` | (_) || |    | |\/| | (_) | |) | || _| \ V /
    # |___/ \___/   |_|\_|\___/ |_|    |_|  |_|\___/|___/___|_|   |_|
    #  _____ _  _ ___ ___    ___ _   _ _  _  ___ _____ ___ ___  _  _
    # |_   _| || |_ _/ __|  | __| | | | \| |/ __|_   _|_ _/ _ \| \| |
    #   | | | __ || |\__ \  | _|| |_| | .` | (__  | |  | | (_) | .` |
    #   |_| |_||_|___|___/  |_|  \___/|_|\_|\___| |_| |___\___/|_|\_|
    #
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup(WIDTH, HEIGHT, MARGIN, MARGIN)
    screen.bgcolor("SkyBlue")

    # Create two turtles, one for drawing and one for writing.
    turtle.TurtleScreen._RUNNING = True  # Get around bug in v3.5.2 http://bugs.python.org/issue26571
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape("turtle")

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay(0)
        artist.hideturtle()
        artist.speed("fastest")
        writer.hideturtle()
        writer.speed("fastest")

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading(90)  # Straight up, which makes it look sort of like a cursor.
    writer.penup()  # A turtle's pen does not have to be down to write text.
    writer.setposition(0, HEIGHT // 2 - FONT_SIZE * 2)  # Centered at top of the screen.

    return screen, artist, writer


def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
