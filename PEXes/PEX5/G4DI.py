import requests
import urllib3

BASE = 'https://goodfordi.com'
urllib3.disable_warnings()


class G4DI:
    def __init__(self, username, password, loc=None, city=None, state=None, ptype=None, return_date=None,
                 return_time=None, comments=None, sca=None, disc_type=None):
        """
        sets up variables and logs in
        :param sesh: requests session
        :param username: users email
        :param password: users password
        :param loc: location name
        :param city: city
        :param state: state
        :param ptype: pass type
        :param return_date: expected
        :param return_time: expected
        :param comments: comments req for discretionary
        :param sca: sca #
        :param dtype: discretionary type, default is none
        """
        self.fdata = {'location': loc, 'city': city, 'state': state, 'signout_type': ptype,
                      'planned_return_date': return_date,
                      'planned_return_time': return_time, 'pass_comments': comments, 'sca_number': sca,
                      "disc_type": disc_type,
                      'signout_button': 'signout'}
        self.sesh = requests.session()
        self.username = username
        self.password = password
        self.signature = ""
        self.tempusername = self.username[3:]
        self.tempusername = self.tempusername[:-10]
        self.tempusername = self.tempusername.split(".")
        for x in range(len(self.tempusername)):
            upper = self.tempusername[x][0].upper()
            self.signature += upper + self.tempusername[x][1:] + " "
        self.signature = self.signature[:-1]

    def signout(self):
        """
        Signs out.
        :return: if they are signed out
        """
        self.login()
        out = self.sesh.post(BASE, data=self.fdata, verify=False)
        if "You are not currently signed out" in out.text:
            return False
        else:
            return True

    def login(self):
        """
        logs in to the site, no return
        :return:
        """
        self.sesh.post(BASE + '/login.php', data={'email': self.username, 'password': self.password, 'login': 'Login'},
                       verify=False)

    def go_to_form1(self):
        """
        goes to form 1 page
        :return:
        """
        self.login()
        self.sesh.get(BASE + '/form1')

    def sign_form_1(self):
        """
        makes fake form 1 sign to get signing id, then uses that to actually sign
        :return:
        """
        self.go_to_form1()
        fake = self.sesh.post(BASE + '/form1.php',
                              data={'form_1_signing_id': 'I am', 'signature': 'Hacking Good 4 DI',
                                    'action': 'form_1_sign_in',
                                    'submit': 'Sign the Form 1'}, verify=False)
        fake_lines = fake.text.splitlines()
        is_this_the_id = False
        length_of_id = 1
        signing_id = ''
        for char in fake_lines[238]:
            if is_this_the_id and length_of_id <= 5:
                signing_id += char
                length_of_id += 1
            if char == "'":
                is_this_the_id = True
        form = self.sesh.post(BASE + '/form1.php',
                       data={'form_1_signing_id': signing_id, 'signature': self.signature, 'action': 'form_1_sign_in',
                             'submit': 'Sign the Form 1'}, verify=False)
        if "You have no more Form 1s to sign at this time" in form.text:
            return True
        else:
            return False

    def signin(self):
        self.login()
        signed_in = self.sesh.get(BASE + '/signin.php')
        if "You are not currently signed out" in signed_in.text:
            return True
        else:
            return False
