Design Gate Check

C3C Matthew Kuhn
C3C Ludvig Oliver
Lt Col Harder
M3/M4 - PEX5: Design Gate Check
15 Nov 2017

1. Description

    This project will implement a GoodForDI process that will access Macro information from users of common places
they tend to sign out to. This would allow for quicker access to details and allow for a type of auto population
to desired locations.  This would include two different files, a user information file and a macro file.  Through a
GUI "sign in window" this will cross check user names to the user information file if they are already registered
or allow for registration to add to the user file.  Once signed in, a "main menu window" will populate with buttons
such as "View/Edit Macros", "Sign Out", and "Sign In/Form 1".  This would open up new windows that respectively
access the functions on the box titles.  The "View/Edit Macros" would allow users to view their macros along with add
new information or editing already existing information that will be later accessed.  The "Sign Out" window will
allow access to the macros through a drop down menu and also allow for new inputs if desired location is not listed.
The "Sign In" window will allow for users to sign in through an auto populated information in the Form 1.

2. Functional Requirements

    - The G4DI class shall include functionality to log in, sign out, sign in, and sign the form 1

    - The GUI class shall contain all the windows and buttons describe above
    - The GUI class shall have a user window
        -- Username
        -- Password
        -- Sign In
        -- Register
        -- Quit
    - The GUI class shall have a main_menu window
        -- View/Edit Macros
        -- Sign Out
        -- Sign In/Form 1
        -- Quit

    - This will integrate both GUI class and User class to access information accordingly dependent on the
        user interface and desire
    - This will also contain the read files of both users and macros and have a write function to make changes
        accordingly

3. Design Specification

    - The G4DI class will contain functions for use by the GUI

        -- Constructor: The class shall accept a session, and all of the user information as its arguments
        -- local variables for all arguments
            -Email (property)
            -Password (property)
            -Location (property)
            -Pass type (property)
            -Requests Session (property)
            -City (property)
            -State (property)
            -Pass type (property)
            -comments (property)
            -SCA # (property)
            -retrurn date (property)
            -return time (property)
        -- signout (function)
            - signs user out using necessary parameters based on pass type
        -- login (function)
            - first to be called, will sign user into good for di
        -- signin (function)
            -will sign user in
        -- go_to_form1 (function)
            - function to transfer to form1 page of website
        -- sign_form1 (function)
            - function to sign the form 1

    - The GUI class will appropriately create the buttons and windows for the GoodForDI program

        -- Constructor: Establishes the windows for each part of the program according to user interface
        -- user_window (GUI class)
            - User_Name Text Box (Entry)
            - Password Text Box (Entry)
            - Sign In Button (Button)
            - Register Button (Button)
            - Quit Button (Button)
        -- main_menu window (GUI class)
            - View/Edit Macros Button (Button)
                -- View/Edit Macros Window (Window)
                    - Edit Macros (Button)
                    - Add Macros (Button)
            - Sign Out Button (GUI class)
                -- Sign Out Window (Window)
                    - Macros Drop Down Box/Text Box (Allows for new user input if information is not in macros)
                    - Address Text Box (Entry)
                    - Date Text Box (Entry)
                    - Time Return Text Box (Entry)
                    - Sign Out Button (Button)
                    - Cancel Button (Button)
            - Sign In Button (GUI class)
                -- Sign In Window (Window)
                    - Sign In Button (Button)
                    - Sign Form 1 Button (Button)
            - Quit Button (Button)

    - Main will integrate both GUI and G4DI to allow for user interface containing the information that requires
        input and file checking that will be seen through reading/editing macro and uer files

        -- main: puts all the functions together
        -- read_users (function): reads the user file and creates a dictionary
        -- read_macros (function): reads the macro file and creates a dictionary
        -- write_users (function): allows for users to register a new account to the file
        -- write_macros (function): allows for users to make the changes or add to the macros file

