from GUI import *
import os

"""
Author: C3C Matthew Kuhn
        C3C Ludvig Oliver
Instructor: LtCol Harder
Date: 6 Dec 2017

Documentation: used this website for infor on frames https://stackoverflow.com/questions/7546050/switch-between-two-frames-in-tkinter
used this for info on windows https://stackoverflow.com/questions/15306631/how-tocreate-children-windows-using-python-tkinter

Specification:
1. Program shall create a GUI with 2 Frames
2. The first frame shall be the login screen, with fields for email and password, and enter and cancel buttons
3. The second frame shall be the main menu, with four buttons
    a. The first button shall open a new window, that displays previously stored macros for that user, with and empty field to
    edit a macro or make a new one, with a back button that saves changes
    b. The second button shall open a new window to sign out, with fields for all necessary data, that autofill when the 
    name of a macro is input, a window will pop up to notify of successful sign out
    c. The third button shall open a new window with buttons to sign in and to sign the Form 1, and a box will pop up to
    confirm whether or not the action is successful
    d. The last button shall close the application
4. The program shall read the macros from a file at the beginning of execution
5. The program shall write the macros back to the at the end of execution
6. The program shall use the requests library to access good for DI using the email and password provided by the user
"""

def main():
    path = os.path.dirname(os.path.realpath(__file__))
    macros_file = os.path.join(path, "macros.txt")
    macros = read_macros(macros_file)
    app = MainApp(macros)
    app.wm_title("Good 4 DI")
    app.mainloop()
    final_macros = []
    for x in macros:
        final_macros.append(macro_to_dict(x))
    write_macros(macros, macros_file)


def read_macros(filename):
    """
    will read in macros, return a list of dicts
    :param filename: file to get macros from
    :return: list of Macro objects
    """
    temp_dict = {}
    final = []
    real_final = []
    with open(filename) as f:
        macros = f.read().splitlines()
    for x in macros:
        x = x[:-1]
        temp_list = x.split(";")
        for y in temp_list:
            key = y[:y.index(":")]
            value = y[y.index(":") + 1:]
            temp_dict.update({(key, value)})
        final.append(temp_dict)
        temp_dict = {}
    for x in final:
        real_final.append(Macro(x))
    return real_final


def write_macros(macros, filename):
    """
    this will write the macros to a file if changed or added
    :param macros: list of dicts to write to file #type list[dict]
    :param filename: file to write macros to
    :return: none
    """
    print_str = ""
    for x in macros:
        print_str += "user:{};loc:{};city:{};state:{};ptype:{};\n".format(x.user, x.loc, x.city, x.state, x.ptype)

    with open(filename, "w") as f:
        f.write(print_str)


def make_username(email):
    """
    returns first inital and lastname given email
    :param email: email to extract username from
    :return: str that is first initial and last name
    """
    email = email.lower()
    first_initial = email[3]
    last_name = email[email.index(".") + 1:email.index("@")]
    return first_initial.lower() + last_name.lower()


def macro_to_dict(macro):
    """

    :param macro: macro object
    :return: dcitionary representation of that object
    """
    return {"user": macro.user, "loc": macro.loc, "city": macro.city, "state": macro.state, "ptype": macro.ptype}


class Macro:
    """
    takes dictionary and makes it a macro object
    """
    def __init__(self, macro):
        self.user = macro["user"]
        self.loc = macro["loc"]
        self.city = macro["city"]
        self.state = macro["state"]
        self.ptype = macro["ptype"]


if __name__ == "__main__":
    main()
