import requests
import urllib3

BASE = 'https://goodfordi.com'
session = requests.session()
usernames = 'c20matthew.kuhn@usafa.edu'
passwords = 'matthewwlk9'
signatures ='Matthew Kuhn'
urllib3.disable_warnings()


def main():
    while 20 == 20:
        # wait for an email
        # write text.txt from email to directory
        with open('text.txt') as raw:
            text = raw.read().splitlines()
        if text[0] == 'G':
            i = 0
        else:
            i = 1
        login(session, usernames[i], passwords[i])
        if text[1] == '1':
            sign_form_1(session, signatures[i])
        elif text[1] == 'out':
            if text[5] == 'SCA':
                signout(text[2], text[3], text[4], text[5], text[6], text[7], '', text[8], session)
            if text[5] == 'Discretionary':
                # add different discretionary types
                signout(text[2], text[3], text[4], text[5], text[6], text[7], text[8], '', session)
            if text[5] == 'Weekend':
                signout(text[2], text[3], text[4], text[5], text[6], text[7], '', '', session)
            if text[5] == 'ACQ':
                signout(text[2], text[3], text[4], text[5], text[6], text[7], '', '', session)
        elif text[1] == 'in':
            signin(session)
        break


def signout(loc, city, state, stype, return_date, return_time, comments, sca_number, sesh):
    """
    signs out
    :param loc: any string
    :param city: Colorado Springs Default
    :param state: Colorado Default
    :param stype: Pass type: Weekend, Discretionary, SCA
    :param return_date: YYYY-MM-DD
    :param return_time: mil time
    :param comments: none unless discretionary
    :param sca_number: ##-##
    :param sesh: session
    :return: none
    """
    fdata = {'location': loc, 'city': city, 'state': state, 'signout_type': stype, 'planned_return_date': return_date,
             'planned_return_time': return_time, 'comments': comments, 'sca_number': sca_number,
             'signout_button': 'signout'}
    sesh.post(BASE, data=fdata, verify=False)


def login(sesh, username, password):
    """

    :param sesh: session
    :param username: username from list
    :param password: password frome list
    :return: none
    """
    sesh.post(BASE + '/login.php',
              data={'email': username, 'password': password, 'login': 'Login'},
              verify=False)


def goto_form1(sesh):
    """

    :param sesh: session
    :return:
    """
    sesh.get(BASE + '/form1')


def sign_form_1(sesh, signature):
    """

    :param sesh: session
    :param signature: signature from list
    :return:
    """
    goto_form1(sesh)
    fake = sesh.post(BASE + '/form1.php',
                     data={'form_1_signing_id': 'I am', 'signature': 'Hacking Good 4 DI', 'action': 'form_1_sign',
                           'submit': 'Sign the Form 1'}, verify=False)
    fake_lines = fake.text.splitlines()
    is_this_the_id = False
    length_of_id = 1
    signing_id = ''
    for char in fake_lines[234]:
        if is_this_the_id and length_of_id <= 5:
            signing_id += char
            length_of_id += 1
        if char == "'":
            is_this_the_id = True

    sesh.post(BASE + '/form1.php',
              data={'form_1_signing_id': signing_id, 'signature': signature, 'action': 'form_1_sign',
                    'submit': 'Sign the Form 1'}, verify=False)


def signin(sesh):
    sesh.get(BASE + '/signin.php')


if __name__ == "__main__":
    main()
