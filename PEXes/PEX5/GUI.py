import tkinter as tk
from tkinter import font as tkfont
import re
from G4DI import *
from main import Macro

"""
fix functionality for discretionary sign outs = do this after turn in
"""


class MainApp(tk.Tk):
    """
    main app, has 2 Frames, loginpage, and mainscreen, accepts macros data structure to work with later
    """

    def __init__(self, macros, *args, **kwargs):
        """
        :param macros: lis of macro objects
        :param args:
        :param kwargs:
        """
        tk.Tk.__init__(self, *args, **kwargs)
        self.macros = macros
        self.email = tk.StringVar()
        self.password = tk.StringVar()
        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.config(bg="light blue")
        container.grid_rowconfigure(0, weight=1)
        self.frames = {}
        for F in (LoginPage, MainScreen):
            page_name = F.__name__
            frame = F(parent=container, controller=self)
            self.frames[page_name] = frame

            frame.grid(row=0, column=0, sticky="nsew")
        self.show_frame("LoginPage")

    def show_frame(self, page_name):
        """
        this is called to switch frames of the main window
        :param page_name: frame to be switched to
        :return:
        """
        frame = self.frames[page_name]
        frame.tkraise()


class LoginPage(tk.Frame):
    """
    Login page frame, with create widgets function, and will recieve username and password, assigning them to the email
    and password variables
    """

    def __init__(self, parent, controller):
        """
        will need to add in macro processing sometime 23-11-2017
        :param parent:
        :param controller:
        """
        tk.Frame.__init__(self, parent, bg="light blue")
        self.parent = parent
        self.controller = controller
        self.title_font = tkfont.Font(size=18, weight="bold")
        self.controller.password.trace("w", self.password_changed)
        self.controller.email.trace("w", self.email_changed)
        self.enter_butt = None
        self.create_widgets()

    def create_widgets(self):
        """
        creates buttons, labels, and text boxes
        :return:
        """
        login_lbl = tk.Label(self, text="Please login with your email and Password", bg="light blue",
                             font=self.title_font)
        login_lbl.grid(row=1, column=1, pady=10, padx=20, columnspan=2)
        email_lbl = tk.Label(self, text="Email:", bg="light blue")
        email_lbl.grid(row=2, column=1, sticky=tk.E, pady=10, padx=5)
        email_txt = tk.Entry(self, textvariable=self.controller.email)
        email_txt.grid(row=2, column=2, sticky=tk.W, pady=10, padx=5)
        pass_lbl = tk.Label(self, text="Password:", bg="light blue")
        pass_lbl.grid(row=3, column=1, sticky=tk.E, pady=10, padx=5)
        pass_txt = tk.Entry(self, show="*", textvariable=self.controller.password)
        pass_txt.grid(row=3, column=2, sticky=tk.W, pady=10, padx=5)
        cncl_butt = tk.Button(self, text="Cancel", command=self.cancel_button_pushed)
        cncl_butt.grid(row=4, column=2, sticky=tk.W, pady=20, padx=55)
        self.enter_butt = tk.Button(self, text="Enter", command=self.enter_button_pushed)
        self.enter_butt.grid(row=4, column=1, sticky=tk.E, pady=20, padx=20)
        # cant press enter unless there is something in the entry boxes
        if self.controller.email.get() and self.controller.password.get():
            self.enter_butt.config(state="normal")
        else:
            self.enter_butt.config(state="disabled")

        self.parent.columnconfigure(1, weight=2)

    def cancel_button_pushed(self):
        # closes program
        self.controller.destroy()

    def enter_button_pushed(self):
        # shows the main screen
        self.controller.show_frame("MainScreen")

    def email_changed(self, _, __, ___):
        # makes sure enter cant be pushed until something is typed
        if self.controller.email.get() and self.controller.password.get() and re.match("[^@]+@[^@]+\.[^@]+",
                                                                                       self.controller.email.get()):
            self.enter_butt.config(state="normal")
        else:
            self.enter_butt.config(state="disabled")

    def password_changed(self, _, __, ___):
        # makes sure enter cant be pushed until something is typed
        if self.controller.email.get() and self.controller.password.get() and re.match("[^@]+@[^@]+\.[^@]+",
                                                                                       self.controller.email.get()):
            self.enter_butt.config(state="normal")
        else:
            self.enter_butt.config(state="disabled")


class MainScreen(tk.Frame):
    def __init__(self, parent, controller):
        """
        this window will be the main screen with four buttons, it will be top level so that it can pop up a window for
        each button
        """
        tk.Frame.__init__(self, parent, bg="red")
        self.parent = parent
        self.controller = controller
        self.title_font = tkfont.Font(size=18, weight="bold")
        self.box_font = tkfont.Font(size=14)
        self.create_widgets()

    def create_widgets(self):
        welcome_lbl = tk.Label(self, text="Please Choose an Option:", font=self.title_font, bg="red")
        welcome_lbl.grid(row=1, column=1, sticky=tk.W, pady=20, padx=140, columnspan=4)
        edit_butt = tk.Button(self, text="View/\nEdit", font=self.box_font, command=self.view_edit_window)
        edit_butt.grid(row=2, column=1, pady=20)
        sign_butt = tk.Button(self, text="Sign Out", font=self.box_font, command=self.sign_out)
        sign_butt.grid(row=2, column=2)
        in_butt = tk.Button(self, text="Sign In/\nForm 1", font=self.box_font, command=self.sign_in)
        in_butt.grid(row=2, column=3)
        exit_butt = tk.Button(self, text="Exit", font=self.box_font, command=self.exit_button_pushed)
        exit_butt.grid(row=2, column=4)

    def view_edit_window(self):
        # pops up the view/edit window
        new_window = tk.Toplevel(self.parent)
        app = ViewEdit(new_window, self.controller)

    def sign_out(self):
        # pops up sign out window
        new_window = tk.Toplevel(self.parent)
        app = SignOut(new_window, self.controller)

    def sign_in(self):
        # pops up sign in window
        new_window = tk.Toplevel(self.parent)
        app = SignIn(new_window, self.controller)

    def exit_button_pushed(self):
        self.controller.destroy()


class ViewEdit:
    def __init__(self, master, controller):
        """
        this will be the view/edit macros window, opened by main screen
        create widgets calls show macro, which will take a macro dictionary as parameter and display it with entry
        fields
        """
        self.master = master
        self.controller = controller
        self.title_font = tkfont.Font(size=12, weight="bold")
        self.curr_row = 0
        self.mac_counter = 1
        self.location = tk.StringVar()
        self.state = tk.StringVar()
        self.ptype = tk.StringVar()
        self.city = tk.StringVar()
        self.create_widgets()

    def create_widgets(self):
        self.curr_row = 1
        lbl_page = tk.Label(self.master, text="View/Edit Macros", font=self.title_font)
        lbl_page.grid(row=self.curr_row, column=1, columnspan=4, pady=10)

        self.curr_row += 1
        # show each label with this persons username
        for m in self.controller.macros:
            curr_user = m.user
            if curr_user == self.make_username(self.controller.email):
                self.show_macro(m)
        lbl_new = tk.Label(self.master, text="New Macro:")
        lbl_new.grid(row=self.curr_row, column=1, padx=10, pady=5)

        self.curr_row += 1
        lbl_loc = tk.Label(self.master, text="Location:")
        lbl_loc.grid(row=self.curr_row, column=1, pady=10)
        entry_loc = tk.Entry(self.master, textvariable=self.location)
        entry_loc.grid(row=self.curr_row, column=2, pady=10)

        lbl_city = tk.Label(self.master, text="City:")
        lbl_city.grid(row=self.curr_row, column=3, pady=5)
        entry_loc = tk.Entry(self.master, textvariable=self.city)
        entry_loc.grid(row=self.curr_row, column=4, pady=5)

        # blank = tk.Label(self.master)
        # blank.grid(row=self.curr_row, column=5)

        self.curr_row += 1
        self.ptype.set('Weekend')
        passes = {"SCA", "AOC", "SAP", "Sponsor", "Weekend", "VCS"}
        lbl_pass = tk.Label(self.master, text="Pass Type:")
        lbl_pass.grid(row=self.curr_row, column=1, pady=5)
        box_pass = tk.OptionMenu(self.master, self.ptype, *passes)
        box_pass.grid(row=self.curr_row, column=2, pady=5)

        lbl_state = tk.Label(self.master, text="State:")
        lbl_state.grid(row=self.curr_row, column=3)
        entry_state = tk.Entry(self.master, textvariable=self.state)
        entry_state.grid(row=self.curr_row, column=4, padx=15)

        self.curr_row += 1
        btn_save = tk.Button(self.master, text="Save", command=self.save_macros)
        btn_save.grid(row=self.curr_row, column=2, pady=10)

        btn_cancel = tk.Button(self.master, text="Cancel", command=self.cancel_button_pushed)
        btn_cancel.grid(row=self.curr_row, column=4, pady=10)

    def show_macro(self, macro):
        # displays each macro, depending on how many there are from create widgets
        lbl_count = tk.Label(self.master, text="Macro #{}:".format(self.mac_counter))
        lbl_count.grid(row=self.curr_row, column=1)
        self.curr_row += 1
        self.mac_counter += 1
        lbl_loc = tk.Label(self.master, text="Location:")
        lbl_loc.grid(row=self.curr_row, column=1)
        lbl_loc2 = tk.Label(self.master, text=macro.loc)
        lbl_loc2.grid(row=self.curr_row, column=2)
        lbl_city = tk.Label(self.master, text="City:")
        lbl_city.grid(row=self.curr_row, column=3, sticky=tk.E)
        lbl_city2 = tk.Label(self.master, text=macro.city)
        lbl_city2.grid(row=self.curr_row, column=4)
        blank = tk.Label(self.master)
        blank.grid(row=self.curr_row, column=5)
        self.curr_row += 1
        lbl_ptype = tk.Label(self.master, text="Pass Type:")
        lbl_ptype.grid(row=self.curr_row, column=1)
        entry_ptype = tk.Label(self.master, text=macro.ptype)
        entry_ptype.grid(row=self.curr_row, column=2)
        lbl_state = tk.Label(self.master, text="State:")
        lbl_state.grid(row=self.curr_row, column=3, sticky=tk.E)
        entry_state = tk.Label(self.master, text=macro.state)
        entry_state.grid(row=self.curr_row, column=4)
        self.curr_row += 1

    def make_username(self, email):
        """
        returns first inital and lastname given email
        :param email: email to extract username from
        :return: str that is first initial and last name
        """
        email = email.get().lower()
        first_initial = email[3]
        last_name = email[email.index(".") + 1:email.index("@")]
        return first_initial.lower() + last_name.lower()

    def save_macros(self):
        # will save the macro typed in only if all fields are filled, will overwrite a macro of same location
        new_macro = {"user": self.make_username(self.controller.email), "loc": self.location.get(),
                     "city": self.city.get(), "state": self.state.get(), "ptype": self.ptype.get()}
        mac_obj = Macro(new_macro)
        temp = []
        for x in self.controller.macros:
            if x.user == mac_obj.user:
                temp.append(x)
        for x in temp:
            if x.loc == mac_obj.loc:
                if mac_obj.city:
                    self.controller.macros.remove(x)
                    self.controller.macros.append(mac_obj)
                    break
                else:
                    self.controller.macros.remove(x)
                    break
            elif x.loc != mac_obj.loc and mac_obj.city:
                self.controller.macros.append(mac_obj)
                break
        self.master.destroy()

    def cancel_button_pushed(self):
        self.master.destroy()


class SignOut:
    def __init__(self, master, controller):
        """
        this will be the sign out page, with fillable text boxes and dropdown menus, that is opened by MainScreen
        """
        self.master = master
        self.controller = controller
        self.macros = self.controller.macros
        self.loc = tk.StringVar()
        self.loc.trace("w", self.loc_changed)
        self.city = tk.StringVar()
        self.state = tk.StringVar()
        self.user = self.make_username(self.controller.email)
        self.ptype = tk.StringVar()
        self.return_date = tk.StringVar()
        self.return_time = tk.StringVar()
        self.return_time.set("Ex. 2300")
        self.return_date.set("YYYY-MM-DD")
        self.comments = tk.StringVar()
        self.sca = tk.StringVar()
        self.title_font = tkfont.Font(size=12, weight="bold")
        self.create_widgets()

    def create_widgets(self):
        lbl_page = tk.Label(self.master, text="Fill Information", font=self.title_font)
        lbl_page.grid(row=1, column=1, pady=5, columnspan=2)

        lbl_location = tk.Label(self.master, text="Location:")
        lbl_location.grid(row=2, column=1, pady=5)
        entry_location = tk.Entry(self.master, textvariable=self.loc)
        entry_location.grid(row=2, column=2, pady=5)

        lbl_city = tk.Label(self.master, text="City:")
        lbl_city.grid(row=3, column=1, pady=5)
        entry_city = tk.Entry(self.master, textvariable=self.city)
        entry_city.grid(row=3, column=2, pady=5)

        lbl_state = tk.Label(self.master, text="State:")
        lbl_state.grid(row=4, column=1, pady=5)
        entry_state = tk.Entry(self.master, textvariable=self.state)
        entry_state.grid(row=4, column=2, pady=5)

        lbl_date = tk.Label(self.master, text="Return Date:")
        lbl_date.grid(row=5, column=1, pady=5, padx=10)
        entry_date = tk.Entry(self.master, textvariable=self.return_date)
        entry_date.grid(row=5, column=2, pady=5, padx=10)

        lbl_time = tk.Label(self.master, text="Return Time:")
        lbl_time.grid(row=6, column=1, pady=5)
        entry_time = tk.Entry(self.master, textvariable=self.return_time)
        entry_time.grid(row=6, column=2, pady=5)

        lbl_comments = tk.Label(self.master, text="Comments:")
        lbl_comments.grid(row=7, column=1, pady=5)
        entry_comments = tk.Entry(self.master, textvariable=self.comments)
        entry_comments.grid(row=7, column=2, pady=5)

        self.ptype.set('Weekend')
        passes = {"SCA", "AOC", "SAP", "Sponsor", "Weekend", "VCS"}
        lbl_pass = tk.Label(self.master, text="Pass Type:")
        lbl_pass.grid(row=8, column=1, pady=5)
        box_pass = tk.OptionMenu(self.master, self.ptype, *passes)
        box_pass.grid(row=8, column=2, pady=5)

        lbl_number = tk.Label(self.master, text="SCA #:")
        lbl_number.grid(row=9, column=1, pady=5)
        entry_number = tk.Entry(self.master, textvariable=self.sca)
        entry_number.grid(row=9, column=2, pady=5)

        out_button = tk.Button(self.master, text="Sign Out", command=self.signout_pushed)
        out_button.grid(row=10, column=1, pady=10)
        clear_button = tk.Button(self.master, text="Clear", command=self.clear_button_pushed)
        clear_button.grid(row=10, column=2, pady=10, sticky=tk.W, padx=25)
        cancel_button = tk.Button(self.master, text="Cancel", command=self.cancel_button_pushed)
        cancel_button.grid(row=10, column=2, sticky=tk.E, pady=10, padx=20)

    def loc_changed(self, _, __, ___):
        # autofill macro
        for x in self.macros:
            if self.loc.get() == x.loc and self.user == x.user:
                self.city.set(x.city)
                self.state.set(x.state)
                self.ptype.set(x.ptype)

    def clear_button_pushed(self):
        # resets default values
        self.state.set("")
        self.city.set("")
        self.loc.set("")
        self.ptype.set("Weekend")
        self.return_time.set("Ex. 2300")
        self.return_date.set("YYYY-MM-DD")
        self.comments.set("")
        self.sca.set("")

    def signout_pushed(self):
        # signs out with G4DI framework, works sometimes
        new_signout = None
        if self.ptype.get() == "Weekend":
            new_signout = G4DI(self.controller.email.get(), self.controller.password.get(), loc=self.loc.get(),
                               city=self.city.get(), state=self.state.get(), ptype=self.ptype.get(),
                               return_date=self.return_date.get(),
                               return_time=self.return_time.get())
        elif self.ptype.get() == "SAP" or self.ptype.get() == "Sponsor" or self.ptype.get() == "VCS" or \
                        self.ptype.get() == "AOC":
            new_signout = G4DI(self.controller.email.get(), self.controller.password.get(), loc=self.loc.get(),
                               city=self.city.get(), state=self.state.get(), disc_type=self.ptype.get(),
                               return_date=self.return_date.get(),
                               return_time=self.return_time.get(), comments=self.comments.get(), ptype="Disc")
        elif self.ptype.get() == "SCA":
            new_signout = G4DI(self.controller.email.get(), self.controller.password.get(), loc=self.loc.get(),
                               city=self.city.get(), state=self.state.get(), ptype=self.ptype.get(),
                               return_date=self.return_date.get(),
                               return_time=self.return_time.get(), sca=self.sca.get())
        sign_out = new_signout.signout()
        new_window = tk.Toplevel(self.master)
        Notification(new_window, self.controller, sign_out, "in")

    def make_username(self, email):
        """
        returns first inital and lastname given email
        :param email: email to extract username from
        :return: str that is first initial and last name
        """
        email = email.get().lower()
        first_initial = email[3]
        last_name = email[email.index(".") + 1:email.index("@")]
        return first_initial.lower() + last_name.lower()

    def cancel_button_pushed(self):
        self.master.destroy()


class SignIn:
    def __init__(self, master, controller):
        """
        this will be the sign in page, opened by MainScreen, with 3 buttons
        """
        self.controller = controller
        self.master = master
        self.g4di = G4DI(self.controller.email.get(), self.controller.password.get())
        self.create_widgets()

    def create_widgets(self):
        in_butt = tk.Button(self.master, text="Sign in", command=self.sign_in)
        in_butt.grid(row=1, column=1, padx=30, pady=20)
        form_butt = tk.Button(self.master, text="Sign Form 1", command=self.form_1)
        form_butt.grid(row=2, column=1, padx=30, pady=20)
        cncl_butt = tk.Button(self.master, text="Cancel", command=self.master.destroy)
        cncl_butt.grid(row=3, column=1, padx=30, pady=20)

    def sign_in(self):
        sign_in = self.g4di.signin()
        new_window = tk.Toplevel(self.master)
        Notification(new_window, self.controller, not sign_in, "in")

    def form_1(self):
        form_1 = self.g4di.sign_form_1()
        new_window = tk.Toplevel(self.master)
        Notification(new_window, self.controller, not form_1, "1")


class Notification:
    def __init__(self, master, controller, sign_out, in_or_1):
        self.signed_out = sign_out  # Type:boolean
        self.master = master
        self.in_or_1 = in_or_1
        self.create_widgets()

    def create_widgets(self):
        # notifies if signin/signout/form 1 is successful
        if self.in_or_1 == "in":
            if self.signed_out:
                signed_out_text = "You are signed out"
            else:
                signed_out_text = "You are not signed out"
        elif self.in_or_1 == "1":
            if self.signed_out:
                signed_out_text = "The form 1 is not signed"
            else:
                signed_out_text = "The form 1 is signed"
        lbl_signed_out = tk.Label(self.master, text=signed_out_text)
        lbl_signed_out.grid(row=1, column=1)
        ok_butt = tk.Button(self.master, text="WOOHOO!", command=self.master.destroy)
        ok_butt.grid(row=2, column=1)
