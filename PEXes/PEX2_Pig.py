#!/usr/bin/env python3
"""
Pex #2 - Pig game
CS 210, Introduction to Programming
"""

import easygui
import random
import turtle

__author__ = "C3C Ludvig Oliver"  # Your name. Ex: John Doe
__section__ = "M3/M4"  # Your section. Ex: M1
__instructor__ = "Lt Col Harder"  # Your instructor. Ex: Lt Col Doe
__date__ = "14 Sep 2017"  # Today's date. Ex: 25 Dec 2017
__documentation__ = """ None """  # Multiple lines OK with triple quotes

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960  # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 / 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = 32  # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = 16  # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = True  # Set to True for fast, stealthy turtles.


def main():
    # Create the turtle screen and two turtles (leave this as the first line of main).

    screen, artist, writer = turtle_setup()

    # TODO: Implement your main program here.
    # Screen Set Up
    screen.bgcolor("green")

    player_one = easygui.enterbox("Insert Name", "Player 1")
    player_two = easygui.enterbox("Insert Name", "Player 2")
    score_one = 0
    score_two = 0
    game_over = False
    writer.hideturtle()

    while game_over != True:

        writer.write("{} {} {} {}".format(player_one, score_one, player_two, score_two), align="center",
                     font=("Courier", FONT_SIZE, "bold"))

        new_score_one = take_one_turn(artist, player_one)
        easygui.msgbox("Your Total Score This round is " + str(new_score_one), "Round Summary")
        artist.clear()
        score_one = score_one + new_score_one
        writer.clear()
        writer.write("{} {} {} {}".format(player_one, score_one, player_two, score_two), align="center",
                     font=("Courier", FONT_SIZE, "bold"))

        new_score_two = take_one_turn(artist, player_two)
        easygui.msgbox("Your Total Score This round is " + str(new_score_two), "Round Summary")
        artist.clear()
        score_two = score_two + new_score_two
        writer.clear()
        writer.write("{} {} {} {}".format(player_one, score_one, player_two, score_two), align="center",
                     font=("Courier", FONT_SIZE, "bold"))

        if score_one >= 100 or score_two >= 100:
            game_over = True

    if score_one >= 100 and score_one > score_two:
        winner = player_one
    else:
        winner = player_two

    writer.clear()
    writer.hideturtle()
    writer.write("{} {} {} {}".format(player_one, score_one, player_two, score_two), align="center",
                 font=("Courier", FONT_SIZE, "bold"))
    writer.up()
    writer.goto(0,0)
    writer.down()
    writer.write("{} Wins! ".format(winner), align="center", font=("Courier", FONT_SIZE, "bold"))
    # Wait for the user to click before closing the window (leave this as the last line of main).
    screen.exitonclick()



def take_one_turn(artist, player):
    """
    Works corresponds to the players turn in the game, Allows for multiple rolls and follows the rules
    as set by the guidelines

    :param artist: The turtle used to draw the dice and pip accordingly
    :param player: Informs who's turn it is during the game
    :return: Returns the Score During The Round
    """
    # TODO: Implement your take_one_turn function here.
    roll = 0
    turn_score = 0
    turn_round = False

    while turn_round != True:
        selection = easygui.buttonbox("May The Odds Ever Be In Your Favor", player + "'s turn", ("Roll", "Hold"))
        if selection == "Roll":
            number = random.randint(1, 6)
            draw_die(artist, artist, number, roll)
        else:
            number = 0
            turn_round = True

        if number == 1:
            turn_score = 0
            number = 0
            turn_round = True

        turn_score = turn_score + number
        roll = roll + 1

    return turn_score


def draw_die(die, pip, number, roll):
    """

    Draws Dice and Pips for each player
    Calls on draw_square and draw_pip functions to create dice

    :param die: Turtle for Dice
    :param pip: Turtle drawn for the Dice Pip
    :param number: The randomized number chosen by each player
    :param roll: Informs function of what roll the player is on to place the dice accordingly
    :return: n/a
    """
    # TODO: Implement your draw_die function here.

    initial_x = 0
    initial_y = 0
    die_length = (WIDTH - (MARGIN * 9)) / 8
    shifter = die_length + MARGIN

    # Positions Dice accordingly to how many rolls the player has done during their round
    if roll < 8:
        initial_x = (-WIDTH / 2) + MARGIN + (shifter * roll)
        initial_y = (HEIGHT / 2) - (MARGIN * 5)
    if 8 <= roll < 16:
        roll = roll - 8
        initial_x = (-WIDTH / 2) + MARGIN + (shifter * roll)
        initial_y = (HEIGHT / 2) - (MARGIN * 9)
    if 16 <= roll < 24:
        roll = roll - 16
        initial_x = (-WIDTH / 2) + MARGIN + (shifter * roll)
        initial_y = (HEIGHT / 2) - (MARGIN * 13)
    if 24 <= roll < 32:
        roll = roll - 24
        initial_x = (-WIDTH / 2) + MARGIN + (shifter * roll)
        initial_y = (HEIGHT / 2) - (MARGIN * 17)
    if 32 <= roll < 40:
        roll = roll - 32
        initial_x = (-WIDTH / 2) + MARGIN + (shifter * roll)
        initial_y = (HEIGHT / 2) - (MARGIN * 20)

    draw_square(die, die_length, initial_x, initial_y)
    draw_pip(pip, number, initial_x, initial_y)


def draw_square(artist, length, x, y):
    """
    Draws the square for the Dice

    :param artist: The Turtle for the Dice outline
    :param length: length of each side of the Dice (84.0)
    :param x: The X-position for the starting point of the dice
    :param y: The Y-position for the starting point of the dice
    :return: n/a
    """

    # Turtle Set Up for Dice Outline
    artist.hideturtle()
    artist.up()
    artist.goto(x, y)
    artist.down()
    artist.fillcolor("red")
    artist.begin_fill()
    for _ in range(4):  # Draw Square for Dice
        artist.forward(length)
        artist.left(90)

    artist.end_fill()


def draw_pip(pip, number, x, y):
    """
    Draw pip for dice

    :param pip: The Turtle for the Pip
    :param number: Depending on the rolled dice, the pip will be drawn accordingly
    :param x: References the X coordinate for the dice
    :param y: References the Y coordinate for the dice
    :return: n/a
    """

    # Pip Set Up
    die_length = (WIDTH - (MARGIN * 9)) / 8

    pip.fillcolor("Black")
    pip.shapesize(0.75)
    pip.shape("circle")
    pip.hideturtle()

    # Establishes the Pip Drawing depending on the rolled number
    if number == 1:
        pip.up()
        pip.goto(x + ((die_length + die_length/14)/2), y + (die_length/2))
        pip.down()
        pip.stamp()

    elif number == 2:
        pip.up()
        pip.goto(x + (die_length/5), y + (4*die_length/5))
        pip.down()
        pip.stamp()
        pip.up()
        pip.goto(x + (4*die_length/5), y + (die_length/5))
        pip.down()
        pip.stamp()

    elif number == 3:
        pip.up()
        pip.goto(x + (die_length/5), y + (4*die_length/5))
        pip.down()
        pip.stamp()
        pip.up()
        pip.goto(x + (die_length/2), y + (die_length/2))
        pip.down()
        pip.stamp()
        pip.up()
        pip.goto(x + (4*die_length/5), y + (die_length/5))
        pip.down()
        pip.stamp()

    elif number == 4:
        pip.up()
        pip.goto(x + (die_length/5), y + (4*die_length/5))
        pip.down()
        pip.stamp()
        pip.up()
        pip.goto(x + (4*die_length/5), y + (4*die_length/5))
        pip.down()
        pip.stamp()
        pip.up()
        pip.goto(x + (die_length/5), y + (die_length/5))
        pip.down()
        pip.stamp()
        pip.up()
        pip.goto(x + (4*die_length/5), y + (die_length/5))
        pip.down()
        pip.stamp()

    elif number == 5:
        pip.up()
        pip.goto(x + (die_length/5), y + (4*die_length/5))
        pip.down()
        pip.stamp()
        pip.up()
        pip.goto(x + (4*die_length/5), y + (4*die_length/5))
        pip.down()
        pip.stamp()
        pip.up()
        pip.goto(x + (die_length/2), y + (die_length/2))
        pip.down()
        pip.stamp()
        pip.up()
        pip.goto(x + (die_length/5), y + (die_length/5))
        pip.down()
        pip.stamp()
        pip.up()
        pip.goto(x + (4*die_length/5), y + (die_length/5))
        pip.down()
        pip.stamp()

    else:
        pip.up()
        pip.goto(x + (die_length/5), y + (4*die_length/5))
        pip.down()
        pip.stamp()
        pip.up()
        pip.goto(x + (die_length/5), y + (die_length/2))
        pip.down()
        pip.stamp()
        pip.up()
        pip.goto(x + (die_length/5), y + (die_length/5))
        pip.down()
        pip.stamp()
        pip.up()
        pip.goto(x + (4*die_length/5), y + (4*die_length/5))
        pip.down()
        pip.stamp()
        pip.up()
        pip.goto(x + (4*die_length/5), y + (die_length/2))
        pip.down()
        pip.stamp()
        pip.up()
        pip.goto(x + (4*die_length/5), y + (die_length/5))
        pip.down()
        pip.stamp()

    pass


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def turtle_setup():
    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    #  ___   ___     _  _  ___ _____    __  __  ___  ___ ___ _____   __
    # |   \ / _ \   | \| |/ _ \_   _|  |  \/  |/ _ \|   \_ _| __\ \ / /
    # | |) | (_) |  | .` | (_) || |    | |\/| | (_) | |) | || _| \ V /
    # |___/ \___/   |_|\_|\___/ |_|    |_|  |_|\___/|___/___|_|   |_|
    #  _____ _  _ ___ ___    ___ _   _ _  _  ___ _____ ___ ___  _  _
    # |_   _| || |_ _/ __|  | __| | | | \| |/ __|_   _|_ _/ _ \| \| |
    #   | | | __ || |\__ \  | _|| |_| | .` | (__  | |  | | (_) | .` |
    #   |_| |_||_|___|___/  |_|  \___/|_|\_|\___| |_| |___\___/|_|\_|
    #
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup(WIDTH, HEIGHT, MARGIN, MARGIN)
    screen.bgcolor("SkyBlue")

    # Create two turtles, one for drawing and one for writing.
    turtle.TurtleScreen._RUNNING = True  # Get around bug in v3.5.2 http://bugs.python.org/issue26571
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape("turtle")

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay(0)
        artist.hideturtle()
        artist.speed("fastest")
        writer.hideturtle()
        writer.speed("fastest")

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading(90)  # Straight up, which makes it look sort of like a cursor.
    writer.penup()  # A turtle's pen does not have to be down to write text.
    writer.setposition(0, HEIGHT // 2 - FONT_SIZE * 2)  # Centered at top of the screen.

    return screen, artist, writer


if __name__ == "__main__":
    print(__doc__.strip())
    print("Author:", (__author__ if __author__ else "\033[91mBLANK (You must fill in the __author__ metadata)\033[0m"))
    print("Section:",
          (__section__ if __section__ else "\033[91mBLANK (You must fill in the __section__ metadata)\033[0m"))
    print("Instructor:",
          (__instructor__ if __instructor__ else "\033[91mBLANK (You must fill in the __instructor__ metadata)\033[0m"))
    print("Date:", (__date__ if __date__ else "\033[91mBLANK (You must fill in the __date__ metadata)\033[0m"))
    print("Documentation:", (
        __documentation__ if __documentation__.strip() else
        "\033[91mBLANK (You must fill in the __documentation__ metadata)\033[0m"))
    b = b'CmltcG9ydCBnZXRwYXNzLCBoYXNobGliLCBjb2RlY3MsIHN0cmluZyBhcyBfX1MKdSA9IGdldHBhc3MuZ2V0dXNlcigpCmggPSBoYXN' + \
        b'obGliLnNoYTI1Nih1LmVuY29kZSgpKS5oZXhkaWdlc3QoKQpyID0gY29kZWNzLmVuY29kZSh1Lmxvd2VyKCkudHJhbnNsYXRlKHtvcm' + \
        b'Qoayk6IE5vbmUgZm9yIGsgaW4gX19TLmRpZ2l0c30pLnJlcGxhY2UoJy4nLCcnKSwgJ3JvdF8xMycpCndpdGggb3BlbihfX2ZpbGVfX' + \
        b'ywgInIiKSBhcyBmOgogICAgaWYgaCBub3QgaW4gZi5yZWFkKCk6CiAgICAgICAgd2l0aCBvcGVuKF9fZmlsZV9fLCAiYSIpIGFzIGY6' + \
        b'CiAgICAgICAgICAgIHByaW50KCIjIiwgaCwgciwgZmlsZT1mKQo='
    try:
        import base64

        eval(compile(base64.b64decode(b), '<string>', "exec"))
    except:
        pass
    finally:
        main()
# 72de204a7312018d00e34442a7df16633351eefd989d08b6170f0796da863df7 pyhqivtbyvire
