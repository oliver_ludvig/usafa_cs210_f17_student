#!/usr/bin/env python3
"""
PUT YOUR DESCRIPTION OF THIS FILE HERE
CS 210, Introduction to Programming
"""

__author__ = "Ludvig Oliver"  # Your name. Ex: John Doe
__section__ = "M3/M4"  # Your section. Ex: M1
__instructor__ = "Lt Col Harder"  # Your instructor. Ex: Lt Col Doe
__date__ = "13 Nov 2017"  # Today's date. Ex: 25 Dec 2017
__documentation__ = """ I received help from C3C Matt Kuhn on understanding what the purpose of the 
 turns_played and what its supposed to track. I resulted in adding onto the turns_played object anytime a 
 player won, loss or tied with the line 'self.__games_played +=1'. Also, C3C Matt Kuhn also helped me on
 understanding how to implement the board_string function and the TicTacToe_Winner_Loser by implementing my 
 program in the class instead of calling the internal structure of the class. I implemented this by using the 
 player_at in my board_string function and implementing my check_winner in the play_move function"""


# Multiple lines OK with triple quotes


def main():
    ###  Up To Three Games ###

    print("\nLets Play a Game of Tic Tac Toe")
    print("Your Goal is to Win Best 2 Out of 3...")

    name_one = input("\nPlayer 1: What's Your Name? ")
    marker_one = input("Input Marker: ")
    player_one = Player(name_one, marker_one)

    name_two = input("\nPlayer 2: What's Your Name? ")
    marker_two = input("Input Marker: ")
    player_two = Player(name_two, marker_two)

    game = TicTacToe(player_one, player_two)
    print("\nRound {}".format(game.rounds))

    while game.rounds < 4:
        board_string(game)
        move = input("{} Make Your Move: ".format(game.current_player.name))
        game.position(int(move))

        if game.winner == game.players[0]:
            game.add_rounds()
            game.change_players(player_one, player_two)

        if game.winner == game.players[1]:
            game.add_rounds()
            game.change_players(player_one, player_two)

        if game.turns_played == 9:
            player_one.record_tie()
            player_two.record_tie()
            game.add_rounds()
            game.change_players(player_one, player_two)

        if game.rounds < 4:
            print("\nRound {}".format(game.rounds))


    if player_one.wins > player_two.wins:
        print("\n{} Wins It All!".format(player_one.name))
        print(player_one)
        print(player_two)

    if player_two.wins > player_one.wins:
        print("\n{} Wins It All".format(player_two.name))
        print(player_two)
        print(player_one)

    if player_one.wins == player_two.wins:
        print("Draw!!!")
        print(player_one)
        print(player_two)

    ### First to Two ###

    # game_over = False
    # print("\nLets Play a Game of Tic Tac Toe")
    # print("First to Two Wins...")
    #
    # name_one = input("\nPlayer 1: What's Your Name? ")
    # marker_one = input("Input Marker: ")
    # player_one = Player(name_one, marker_one)
    #
    # name_two = input("\nPlayer 2: What's Your Name? ")
    # marker_two = input("Input Marker: ")
    # player_two = Player(name_two, marker_two)
    #
    # game = TicTacToe(player_one, player_two)
    # print("\nRound {}".format(game.rounds))
    #
    # while game_over is False:
    #     board_string(game)
    #     move = input("{} Make Your Move: ".format(game.current_player.name))
    #     game.position(int(move))
    #
    #     if game.winner == game.players[0]:
    #         game.add_rounds()
    #         game.change_players(player_one, player_two)
    #
    #     if game.winner == game.players[1]:
    #         game.add_rounds()
    #         game.change_players(player_one, player_two)
    #
    #     if game.turns_played == 9:
    #         player_one.record_tie()
    #         player_two.record_tie()
    #         game.add_rounds()
    #         game.change_players(player_one, player_two)
    #
    #     if player_one.wins == 2 or player_two.wins == 2:
    #         game_over = True
    #
    #     if game_over is False:
    #         print("\nRound {}".format(game.rounds))
    #
    # if player_one.wins == 2:
    #     print("\n{} Wins It All!".format(player_one.name))
    #     print(player_one)
    #     print(player_two)
    #
    # if player_two.wins == 2:
    #     print("\n{} Wins It All".format(player_two.name))
    #     print(player_two)
    #     print(player_one)


def board_string(board):
    """
    :param board: Player Object
    :return: Returns Game_Board for the game reseting and updating
    """
    game_board = ''

    for i in range(1, 4):
        for x in range(1, 4):
            if board.player_at(i, x) == board.players[0]:
                if x == 1 or x == 2:
                    game_board = game_board + str(board.players[0].marker) + ' '
                else:
                    game_board = game_board + str(board.players[0].marker)
            elif board.player_at(i, x) == board.players[1]:
                if x == 1 or x == 2:
                    game_board = game_board + str(board.players[1].marker) + ' '
                else:
                    game_board = game_board + str(board.players[1].marker)
            else:
                if i == 1:
                    if x == 1 or x == 2:
                        game_board = game_board + str(x) + ' '
                    else:
                        game_board = game_board + str(x)
                if i == 2:
                    if x == 1 or x == 2:
                        game_board = game_board + str(i + x + 1) + ' '
                    else:
                        game_board = game_board + str(i + x + 1)
                if i == 3:
                    if x == 1 or x == 2:
                        game_board = game_board + str(i + x + 3) + ' '
                    else:
                        game_board = game_board + str(i + x + 3)

        game_board = game_board + '\n'

    print("\n" + game_board)
    return game_board


class TicTacToe:
    """ A Tic Tac Toe game model """

    def __init__(self, player1, player2):
        """
        Initializes a new TicTacToe game.
        :param Player player1: the first player to take a turn
        :param Player player2: the second player to take a turn
        """

        self.__current_player = player1
        self.__players = (player1, player2)
        self.__winner = None
        self.__loser = None
        self.__turns_played = 0
        self.__rounds = 1
        self.__play = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

    @property
    def current_player(self):
        """
        :return: The current player making a move
        :rtype: object
        """
        return self.__current_player

    def change_current_player(self):
        """
        :return: Changes The Current Player Every Turn
        """
        if self.turns_played % 2 == 0:
            self.__current_player = self.players[0]
        else:
            self.__current_player = self.players[1]

    @property
    def players(self):
        """
        :return: The Players in the game
        :rtype:tuple of Objects
        """
        return self.__players

    def change_players(self, player_one, player_two):
        """
        :param player_one: First Player In The Game
        :param player_two: Second Player In The Game
        :return: Swaps the Two depending on the round
        """
        if self.rounds % 2 == 1:
            self.__players = (player_one, player_two)
        else:
            self.__players = (player_two, player_one)

    @property
    def winner(self):
        """
        :return: The Winner
        :rtype: object
        """
        return self.__winner

    def reset_winner(self):
        """
        :return: Resets Winner
        """
        self.__winner = None

    @winner.setter
    def winner(self, player):
        """
        :param player: Player Object
        :return: No Return
        """
        player.record_win()
        self.__winner = player

    @property
    def loser(self):
        """
        :return: The Loser
        :rtype: object
        """
        return self.__loser

    def reset_loser(self):
        """
        :return: Resets Loser
        """
        self.__loser = None

    @loser.setter
    def loser(self, player):
        """
        :param player: Player Object
        :return: No Return
        """
        player.record_loss()
        self.__loser = player

    @property
    def turns_played(self):
        """
        :return: Amount of turns played (0 - 9)
        :rtype: int
        """
        return self.__turns_played

    def add_turns_played(self):
        """
        :return: Adds to the turns_played
        """
        self.__turns_played += 1
        self.change_current_player()

    def reset_turns_played(self):
        """
        :return: Resets Turns_Played
        """
        self.__turns_played = 0

    @property
    def play(self):
        """
        :return: The Playing Board
        :rtype: [list]
        """
        return self.__play

    def reset_board(self):
        """
        :return: Resets Board
        :rtype: [list]
        """
        self.__play = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

    @property
    def rounds(self):
        """
        :return: Keeps Track of Rounds Played
        :rtype: int
        """
        return self.__rounds

    def add_rounds(self):
        """
        :return: Adds To Rounds
        """
        self.__rounds += 1
        self.reset_board()
        self.reset_winner()
        self.reset_loser()
        self.reset_turns_played()

    def play_move(self, row, column):
        """
        :param row: integer of row (1 - 3)
        :param column: integer of column (1 - 3)
        :return: Nothing
        """
        self.__play[row - 1][column - 1] = self.current_player.marker
        self.check_win()
        self.add_turns_played()

    def position(self, place):
        """
        :param place: Place Chosen on the Board (1 - 9)
        :return: No Return
        """
        if place == 1:
            self.play_move(1, 1)
        if place == 2:
            self.play_move(1, 2)
        if place == 3:
            self.play_move(1, 3)
        if place == 4:
            self.play_move(2, 1)
        if place == 5:
            self.play_move(2, 2)
        if place == 6:
            self.play_move(2, 3)
        if place == 7:
            self.play_move(3, 1)
        if place == 8:
            self.play_move(3, 2)
        if place == 9:
            self.play_move(3, 3)

    def player_at(self, row, column):
        """
        :param row: integer for row (1 - 3)
        :param column: integer for column (1 - 3)
        :return: Player Object at position or None
        """
        if self.play[row - 1][column - 1] == self.players[0].marker:
            return self.players[0]
        if self.play[row - 1][column - 1] == self.players[1].marker:
            return self.players[1]
        return None

    def check_win(self):

        # Check Row
        for i in range(len(self.play)):
            if self.play[i][0] == self.players[0].marker and self.play[i][1] == self.players[0].marker \
                    and self.play[i][2] == self.players[0].marker:
                self.winner = self.players[0]
                self.loser = self.players[1]

            if self.play[i][0] == self.players[1].marker and self.play[i][1] == self.players[1].marker \
                    and self.play[i][2] == self.players[1].marker:
                self.winner = self.players[1]
                self.loser = self.players[0]

        # Check Column
        for i in range(len(self.play)):
            if self.play[0][i] == self.players[0].marker and self.play[1][i] == self.players[0].marker \
                    and self.play[2][i] == self.players[0].marker:
                self.winner = self.players[0]
                self.loser = self.players[1]

            if self.play[0][i] == self.players[1].marker and self.play[1][i] == self.players[1].marker \
                    and self.play[2][i] == self.players[1].marker:
                self.winner = self.players[1]
                self.loser = self.players[0]

        # Check Cross - Player 1
        if self.play[0][0] == self.players[0].marker and self.play[1][1] == self.players[0].marker \
                and self.play[2][2] == self.players[0].marker:
            self.winner = self.players[0]
            self.loser = self.players[1]

        if self.play[0][2] == self.players[0].marker and self.play[1][1] == self.players[0].marker \
                and self.play[2][0] == self.players[0].marker:
            self.winner = self.players[0]
            self.loser = self.players[1]

        # Check Cross - Player 2
        if self.play[0][0] == self.players[1].marker and self.play[1][1] == self.players[1].marker \
                and self.play[2][2] == self.players[1].marker:
            self.winner = self.players[1]
            self.loser = self.players[0]

        if self.play[0][2] == self.players[1].marker and self.play[1][1] == self.players[1].marker \
                and self.play[2][0] == self.players[1].marker:
            self.winner = self.players[1]
            self.loser = self.players[0]

        return None


class Player:
    """ A player in the Tic Tac Toe game. """

    def __init__(self, name, marker):
        """
        Initializes a new Player with a name and a marker.
        :param str name: the player's name
        :param str marker: the marker to use on the board
        """
        self.name = name

        # Initiates Marker Correctly
        if marker is None:
            self.marker = "!"
        elif marker is "":
            self.marker = "!"
        elif len(marker) > 1:
            self.marker = marker[0]
        else:
            self.marker = marker

        self.__wins = 0
        self.__losses = 0
        self.__ties = 0
        self.__games_played = 0

    @property
    def wins(self):
        """
        :return: Shows Amount of Wins
        """
        return self.__wins

    def record_win(self):
        """
        Adds to Wins
        :return: No Return
        """
        self.__games_played += 1
        self.__wins += 1

    @property
    def losses(self):
        """
        :return: Shows Amount of Losses
        """
        return self.__losses

    def record_loss(self):
        """
        Adds to Losses
        :return: No Return
        """
        self.__games_played += 1
        self.__losses += 1

    @property
    def ties(self):
        """
        :return: Shows Amount of Ties
        """
        return self.__ties

    def record_tie(self):
        """
        Adds to Ties
        :return: No return
        """
        self.__games_played += 1
        self.__ties += 1

    @property
    def games_played(self):
        """
        :return: Shows Amount of Games Played
        """
        return self.__games_played

    def __str__(self):
        """
        Allows for class to be called and give information
        :return: string of information
        """
        return "{}: {}, {} wins, {} losses, {} ties".format(self.name, self.marker, self.wins, self.losses, self.ties)


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

if __name__ == "__main__":
    print(__doc__.strip())
    print("Author:", (__author__ if __author__ else "\033[91mBLANK (You must fill in the __author__ metadata)\033[0m"))
    print("Section:",
          (__section__ if __section__ else "\033[91mBLANK (You must fill in the __section__ metadata)\033[0m"))
    print("Instructor:",
          (__instructor__ if __instructor__ else "\033[91mBLANK (You must fill in the __instructor__ metadata)\033[0m"))
    print("Date:", (__date__ if __date__ else "\033[91mBLANK (You must fill in the __date__ metadata)\033[0m"))
    print("Documentation:", (
        __documentation__ if __documentation__.strip() else
        "\033[91mBLANK (You must fill in the __documentation__ metadata)\033[0m"))
    _ = b'CmltcG9ydCBnZXRwYXNzLCBoYXNobGliLCBjb2RlY3MsIHN0cmluZyBhcyBfX1MKdSA9IGdldHBhc3MuZ2V0dXNlcigpCmggPSBoYXN' + \
        b'obGliLnNoYTI1Nih1LmVuY29kZSgpKS5oZXhkaWdlc3QoKQpyID0gY29kZWNzLmVuY29kZSh1Lmxvd2VyKCkudHJhbnNsYXRlKHtvcm' + \
        b'Qoayk6IE5vbmUgZm9yIGsgaW4gX19TLmRpZ2l0c30pLnJlcGxhY2UoJy4nLCcnKSwgJ3JvdF8xMycpCndpdGggb3BlbihfX2ZpbGVfX' + \
        b'ywgInIiKSBhcyBmOgogICAgaWYgaCBub3QgaW4gZi5yZWFkKCk6CiAgICAgICAgd2l0aCBvcGVuKF9fZmlsZV9fLCAiYSIpIGFzIGY6' + \
        b'CiAgICAgICAgICAgIHByaW50KCIjIiwgaCwgciwgZmlsZT1mKQo='
    # noinspection PyBroadException
    try:
        import base64

        eval(compile(base64.b64decode(_), '<string>', "exec"))
    except:
        pass
    finally:
        main()
# 72de204a7312018d00e34442a7df16633351eefd989d08b6170f0796da863df7 pyhqivtbyvire
