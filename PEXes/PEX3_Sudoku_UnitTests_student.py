#!/usr/bin/env python3
"""
Unit tests for use with Programming Exercise 3, Sudoku.

CS 210, Introduction to Programming, Fall 2017, Dr. Bower.

Documentation: https://docs.python.org/3/library/unittest.html

To run this file, right click on it in PyCharm and
choose "Run Unittests in PEX3_Sudoku_UnitTests_student.py"
"""

import io
import sys
import unittest

# Change the first part of this import statement for each student.
from PEXes.PEX3_Sudoku import str_sudoku, print_sudoku, \
    create_sudoku, open_sudoku, is_solved, is_valid, solve_sudoku

# Data strings from Sudoku00.txt, Sudoku01.txt, and Sudoku02.txt
# Note: The formatting below looks a little odd, but the data formatting
# is exactly what is being tested so this precise formatting is necessary.
DATA = [
    """1 2 3 4 5 6 7 8 9
4 5 6 7 8 9 1 2 3
7 8 9 1 2 3 4 5 6
2 3 4 5 6 7 8 9 1
5 6 7 8 9 1 2 3 4
8 9 1 2 3 4 5 6 7
3 4 5 6 7 8 9 1 2
6 7 8 9 1 2 3 4 5
9 1 2 3 4 5 6 7 8""",
    """5 3 4 6 7 8 9 1 2 6 7 2 1 9 5 3 4 8 1 9 8 3 4 2 5 6 7
8 5 9 7 6 1 4 2 3 4 2 6 8 5 3 7 9 1 7 1 3 9 2 4 8 5 6
9 6 1 5 3 7 2 8 4 2 8 7 4 1 9 6 3 5 3 4 5 2 8 6 1 7 9""",
    """3 9 6 4 1 5 2 7 8 1 2 5 7 3 8 4 6 9 4 7 8 2 6 9 3 1 5 7 5 9 6 4 2 8 3 1 8 4 3 5 """ +
    """9 1 7 2 6 2 6 1 3 8 7 5 9 4 5 3 4 9 2 6 1 8 7 6 8 7 1 5 3 9 4 2 9 1 2 8 7 4 6 5 3"""]

# List of file names, Sudoku00.txt through Sudoku12.txt.
FILES = ["../Data/Sudoku/Sudoku{:0>2d}.txt".format(n) for n in range(0, 13)]

# Puzzles from Sudoku00.txt through Sudoku12.txt.
PUZZLES = [
    [[1, 2, 3, 4, 5, 6, 7, 8, 9], [4, 5, 6, 7, 8, 9, 1, 2, 3], [7, 8, 9, 1, 2, 3, 4, 5, 6],
     [2, 3, 4, 5, 6, 7, 8, 9, 1], [5, 6, 7, 8, 9, 1, 2, 3, 4], [8, 9, 1, 2, 3, 4, 5, 6, 7],
     [3, 4, 5, 6, 7, 8, 9, 1, 2], [6, 7, 8, 9, 1, 2, 3, 4, 5], [9, 1, 2, 3, 4, 5, 6, 7, 8]],
    [[5, 3, 4, 6, 7, 8, 9, 1, 2], [6, 7, 2, 1, 9, 5, 3, 4, 8], [1, 9, 8, 3, 4, 2, 5, 6, 7],
     [8, 5, 9, 7, 6, 1, 4, 2, 3], [4, 2, 6, 8, 5, 3, 7, 9, 1], [7, 1, 3, 9, 2, 4, 8, 5, 6],
     [9, 6, 1, 5, 3, 7, 2, 8, 4], [2, 8, 7, 4, 1, 9, 6, 3, 5], [3, 4, 5, 2, 8, 6, 1, 7, 9]],
    [[3, 9, 6, 4, 1, 5, 2, 7, 8], [1, 2, 5, 7, 3, 8, 4, 6, 9], [4, 7, 8, 2, 6, 9, 3, 1, 5],
     [7, 5, 9, 6, 4, 2, 8, 3, 1], [8, 4, 3, 5, 9, 1, 7, 2, 6], [2, 6, 1, 3, 8, 7, 5, 9, 4],
     [5, 3, 4, 9, 2, 6, 1, 8, 7], [6, 8, 7, 1, 5, 3, 9, 4, 2], [9, 1, 2, 8, 7, 4, 6, 5, 3]],
    [[1, 8, 4, 6, 3, 9, 2, 5, 7], [5, 9, 7, 1, 2, 4, 6, 8, 3], [6, 3, 2, 5, 8, 7, 4, 1, 9],
     [7, 6, 9, 8, 4, 5, 3, 2, 1], [4, 5, 3, 2, 7, 1, 9, 6, 8], [2, 1, 8, 9, 6, 3, 7, 4, 5],
     [3, 7, 1, 4, 5, 2, 8, 9, 6], [8, 2, 5, 7, 9, 6, 1, 3, 4], [9, 4, 6, 3, 1, 8, 5, 7, 2]],
    [[1, 2, 3, 4, 5, 6, 7, 8, 9], [2, 3, 4, 5, 6, 7, 8, 9, 1], [3, 4, 5, 6, 7, 8, 9, 1, 2],
     [4, 5, 6, 7, 8, 9, 1, 2, 3], [5, 6, 7, 8, 9, 1, 2, 3, 4], [6, 7, 8, 9, 1, 2, 3, 4, 5],
     [7, 8, 9, 1, 2, 3, 4, 5, 6], [8, 9, 1, 2, 3, 4, 5, 6, 7], [9, 1, 2, 3, 4, 5, 6, 7, 8]],
    [[5, 3, 4, 6, 7, 8, 9, 1, 2], [6, 7, 2, 1, 9, 5, 3, 4, 8], [1, 9, 8, 3, 4, 2, 5, 6, 7],
     [8, 5, 9, 7, 6, 1, 4, 2, 3], [4, 2, 6, 8, 5, 3, 7, 9, 1], [7, 1, 3, 9, 2, 2, 8, 5, 6],
     [9, 6, 1, 5, 3, 7, 4, 8, 4], [2, 8, 7, 4, 1, 9, 6, 3, 5], [3, 4, 5, 2, 8, 6, 1, 7, 9]],
    [[5, 3, 4, 0, 7, 8, 9, 1, 2], [6, 7, 0, 1, 9, 5, 3, 4, 8], [1, 9, 8, 3, 4, 2, 5, 0, 7],
     [8, 5, 9, 7, 6, 1, 4, 2, 3], [0, 2, 6, 8, 2, 3, 7, 9, 1], [7, 1, 3, 9, 5, 4, 8, 0, 6],
     [9, 6, 1, 5, 3, 7, 2, 8, 0], [2, 8, 7, 4, 1, 0, 6, 3, 5], [0, 4, 5, 2, 8, 6, 1, 7, 9]],
    [[5, 0, 4, 6, 7, 8, 9, 1, 2], [6, 7, 2, 1, 9, 5, 3, 0, 8], [1, 9, 8, 3, 0, 2, 5, 6, 7],
     [0, 5, 9, 7, 8, 1, 4, 2, 3], [4, 2, 6, 0, 5, 3, 7, 9, 1], [7, 1, 3, 9, 2, 4, 8, 5, 6],
     [9, 6, 0, 5, 3, 7, 2, 8, 4], [2, 8, 7, 4, 1, 9, 0, 3, 5], [3, 4, 5, 2, 8, 0, 1, 7, 9]],
    [[5, 3, 4, 6, 7, 8, 9, 1, 2], [6, 7, 2, 0, 9, 5, 3, 4, 8], [1, 9, 8, 3, 4, 2, 5, 6, 7],
     [8, 5, 9, 7, 6, 1, 4, 2, 3], [4, 2, 6, 8, 5, 3, 7, 9, 1], [7, 0, 3, 1, 2, 4, 8, 5, 6],
     [9, 6, 1, 5, 3, 7, 2, 8, 4], [2, 8, 7, 4, 1, 9, 6, 3, 5], [3, 4, 5, 2, 8, 6, 1, 7, 9]],
    [[5, 0, 4, 6, 7, 8, 9, 1, 2], [6, 7, 2, 1, 9, 0, 3, 4, 8], [0, 9, 8, 3, 4, 2, 0, 6, 7],
     [8, 5, 0, 7, 6, 1, 4, 2, 3], [4, 2, 6, 0, 5, 3, 7, 9, 1], [7, 1, 3, 9, 2, 4, 8, 0, 6],
     [9, 6, 1, 5, 0, 7, 2, 8, 4], [2, 8, 7, 4, 1, 9, 0, 3, 5], [3, 4, 0, 2, 8, 6, 5, 7, 0]],
    [[0, 2, 3, 4, 5, 6, 7, 8, 9], [4, 0, 6, 7, 8, 9, 1, 2, 3], [7, 8, 0, 1, 2, 3, 4, 5, 6],
     [2, 3, 4, 0, 6, 7, 8, 9, 1], [5, 6, 7, 8, 0, 1, 2, 3, 4], [8, 9, 1, 2, 3, 0, 5, 6, 7],
     [3, 4, 5, 6, 7, 8, 0, 1, 2], [6, 7, 8, 9, 1, 2, 3, 0, 5], [9, 1, 2, 3, 4, 5, 6, 7, 0]],
    [[0, 0, 5, 8, 0, 9, 3, 0, 0], [0, 0, 0, 0, 4, 0, 0, 6, 0], [8, 3, 0, 0, 2, 0, 0, 1, 0],
     [0, 0, 9, 1, 0, 0, 2, 0, 5], [1, 0, 0, 0, 0, 0, 0, 0, 3], [6, 0, 2, 0, 0, 4, 7, 0, 0],
     [0, 2, 0, 0, 1, 0, 0, 7, 9], [0, 6, 0, 0, 9, 0, 0, 0, 0], [0, 0, 4, 5, 0, 2, 1, 0, 0]],
    [[5, 3, 0, 0, 7, 0, 0, 0, 0], [6, 0, 0, 1, 9, 5, 0, 0, 0], [0, 9, 8, 0, 0, 0, 0, 6, 0],
     [8, 0, 0, 0, 6, 0, 0, 0, 3], [4, 0, 0, 8, 0, 3, 0, 0, 1], [7, 0, 0, 0, 2, 0, 0, 0, 6],
     [0, 6, 0, 0, 0, 0, 2, 8, 0], [0, 0, 0, 4, 1, 9, 0, 0, 5], [0, 0, 0, 0, 8, 0, 0, 7, 9]]
]

# Solved and valid answers for the above puzzles (yes, hard-coded and ugly ... sorry).
SOLVED = [True, True, True, True, False, False, False, False, False, False, False, False, False]
VALID = [True, True, True, True, False, False, False, False, False, False, True, True, True]

# A couple printed puzzles for testing.
PRINT_00 = """+===+===+===+===+===+===+===+===+===+
# 1 | 2 | 3 # 4 | 5 | 6 # 7 | 8 | 9 #
+---+---+---+---+---+---+---+---+---+
# 4 | 5 | 6 # 7 | 8 | 9 # 1 | 2 | 3 #
+---+---+---+---+---+---+---+---+---+
# 7 | 8 | 9 # 1 | 2 | 3 # 4 | 5 | 6 #
+===+===+===+===+===+===+===+===+===+
# 2 | 3 | 4 # 5 | 6 | 7 # 8 | 9 | 1 #
+---+---+---+---+---+---+---+---+---+
# 5 | 6 | 7 # 8 | 9 | 1 # 2 | 3 | 4 #
+---+---+---+---+---+---+---+---+---+
# 8 | 9 | 1 # 2 | 3 | 4 # 5 | 6 | 7 #
+===+===+===+===+===+===+===+===+===+
# 3 | 4 | 5 # 6 | 7 | 8 # 9 | 1 | 2 #
+---+---+---+---+---+---+---+---+---+
# 6 | 7 | 8 # 9 | 1 | 2 # 3 | 4 | 5 #
+---+---+---+---+---+---+---+---+---+
# 9 | 1 | 2 # 3 | 4 | 5 # 6 | 7 | 8 #
+===+===+===+===+===+===+===+===+===+"""

PRINT_10 = """+===+===+===+===+===+===+===+===+===+
#   | 2 | 3 # 4 | 5 | 6 # 7 | 8 | 9 #
+---+---+---+---+---+---+---+---+---+
# 4 |   | 6 # 7 | 8 | 9 # 1 | 2 | 3 #
+---+---+---+---+---+---+---+---+---+
# 7 | 8 |   # 1 | 2 | 3 # 4 | 5 | 6 #
+===+===+===+===+===+===+===+===+===+
# 2 | 3 | 4 #   | 6 | 7 # 8 | 9 | 1 #
+---+---+---+---+---+---+---+---+---+
# 5 | 6 | 7 # 8 |   | 1 # 2 | 3 | 4 #
+---+---+---+---+---+---+---+---+---+
# 8 | 9 | 1 # 2 | 3 |   # 5 | 6 | 7 #
+===+===+===+===+===+===+===+===+===+
# 3 | 4 | 5 # 6 | 7 | 8 #   | 1 | 2 #
+---+---+---+---+---+---+---+---+---+
# 6 | 7 | 8 # 9 | 1 | 2 # 3 |   | 5 #
+---+---+---+---+---+---+---+---+---+
# 9 | 1 | 2 # 3 | 4 | 5 # 6 | 7 |   #
+===+===+===+===+===+===+===+===+===+"""


class UnitTests(unittest.TestCase):
    """Tests the Sudoku functions."""

    def test_str_sudoku(self):
        """Test the str_sudoku function."""
        # Print the first three puzzles for a visual check in the console window.
        for i in range(3):
            print("PUZZLES[ {} ]:\n{}\n".format(i, str_sudoku(PUZZLES[i])))

        # Tests the first three puzzles by getting the string, comparing the values
        # without whitespace, and then checking that there sufficient whitespace for
        # a character between each value; not exact, but should be pretty close.
        for index in range(3):
            with self.subTest("index = {}".format(index)):
                s = str_sudoku(PUZZLES[index])
                self.assertEqual("".join(DATA[index].split()), "".join(s.split()))
                self.assertTrue(len(s) > 160)  # A whitespace character between each value.

    def test_print_sudoku(self):
        """Test the print_sudoku function."""
        # Print puzzles 0 and 10 for a visual check in the console window.
        print("\nPUZZLES[ 0 ]:")
        print_sudoku(PUZZLES[0])
        print("\nPUZZLES[ 10 ]:")
        print_sudoku(PUZZLES[10])
        print()

        # Save standard out so it can be put back later.
        old_stdout = sys.stdout

        # Capture standard out in a result string and print puzzle zero.
        with self.subTest("print_sudoku( PUZZLES[ 0 ] )"):
            sys.stdout = result = io.StringIO()
            print_sudoku(PUZZLES[0])
            self.assertEqual(PRINT_00, result.getvalue().strip().replace(" \n", "\n"))

        # Capture standard out in a result string and print puzzle ten.
        with self.subTest("print_sudoku( PUZZLES[ 10 ] )"):
            sys.stdout = result = io.StringIO()
            print_sudoku(PUZZLES[10])
            self.assertEqual(PRINT_10, result.getvalue().strip().replace(" \n", "\n"))

        # Put standard out back to normal.
        sys.stdout = old_stdout

    def test_create_sudoku(self):
        """Test the create_sudoku function."""
        # Test the DATA strings with the first three puzzles.
        # print( create_sudoku( DATA[ 0 ] ), PUZZLES[ 0 ], sep="\n", flush=True )
        for index in range(3):
            with self.subTest("index = {}".format(index)):
                self.assertEqual(PUZZLES[index], create_sudoku(DATA[index]))

    def test_create_and_str(self):
        """Test the create_sudoku and str_sudoku functions."""
        for index in range(13):
            with self.subTest("index = {}".format(index)):
                self.assertEqual(PUZZLES[index], create_sudoku(str_sudoku(PUZZLES[index])))

    def test_open_sudoku(self):
        """Test the open_sudoku function."""
        for index in range(13):
            with self.subTest("index = {}".format(index)):
                self.assertEqual(PUZZLES[index], open_sudoku(FILES[index]))

    def test_is_solved(self):
        """Test the is_solved function."""
        for index in range(13):
            with self.subTest("index = {}".format(index)):
                self.assertEqual(SOLVED[index], is_solved(PUZZLES[index]))

        with self.subTest("Puzzle of all 5s"):
            # Catches the error of only checking the sum of each row/column/grid equal to 45.
            self.assertFalse(is_solved([[5] * 9] * 9))

    def test_is_valid(self):
        """Test the is_valid function."""
        for index in range(13):
            with self.subTest("index = {}".format(index)):
                self.assertEqual(VALID[index], is_valid(PUZZLES[index]))

    # def test_solve_sudoku(self):
    #     """Test the solve_sudoku function."""
    #     # Create a fresh puzzles for these tests since they will be changed.
    #
    #     with self.subTest("Puzzle 0 with just one value removed from the first cell."):
    #         puzzle = [[0, 2, 3, 4, 5, 6, 7, 8, 9], [4, 5, 6, 7, 8, 9, 1, 2, 3], [7, 8, 9, 1, 2, 3, 4, 5, 6],
    #                   [2, 3, 4, 5, 6, 7, 8, 9, 1], [5, 6, 7, 8, 9, 1, 2, 3, 4], [8, 9, 1, 2, 3, 4, 5, 6, 7],
    #                   [3, 4, 5, 6, 7, 8, 9, 1, 2], [6, 7, 8, 9, 1, 2, 3, 4, 5], [9, 1, 2, 3, 4, 5, 6, 7, 8]]
    #         solve_sudoku(puzzle)
    #         self.assertTrue(is_solved(puzzle))
    #
    #     with self.subTest("Puzzle 0 with just one value removed from the center cell."):
    #         puzzle = [[1, 2, 3, 4, 5, 6, 7, 8, 9], [4, 5, 6, 7, 8, 9, 1, 2, 3], [7, 8, 9, 1, 2, 3, 4, 5, 6],
    #                   [2, 3, 4, 5, 6, 7, 8, 9, 1], [5, 6, 7, 8, 0, 1, 2, 3, 4], [8, 9, 1, 2, 3, 4, 5, 6, 7],
    #                   [3, 4, 5, 6, 7, 8, 9, 1, 2], [6, 7, 8, 9, 1, 2, 3, 4, 5], [9, 1, 2, 3, 4, 5, 6, 7, 8]]
    #         solve_sudoku(puzzle)
    #         self.assertTrue(is_solved(puzzle))
    #
    #     with self.subTest("Puzzle 0 with just one value removed from the last cell."):
    #         puzzle = [[1, 2, 3, 4, 5, 6, 7, 8, 9], [4, 5, 6, 7, 8, 9, 1, 2, 3], [7, 8, 9, 1, 2, 3, 4, 5, 6],
    #                   [2, 3, 4, 5, 6, 7, 8, 9, 1], [5, 6, 7, 8, 9, 1, 2, 3, 4], [8, 9, 1, 2, 3, 4, 5, 6, 7],
    #                   [3, 4, 5, 6, 7, 8, 9, 1, 2], [6, 7, 8, 9, 1, 2, 3, 4, 5], [9, 1, 2, 3, 4, 5, 6, 7, 0]]
    #         solve_sudoku(puzzle)
    #         self.assertTrue(is_solved(puzzle))
    #
    #     with self.subTest("Puzzle 10."):
    #         puzzle = [[0, 2, 3, 4, 5, 6, 7, 8, 9], [4, 0, 6, 7, 8, 9, 1, 2, 3], [7, 8, 0, 1, 2, 3, 4, 5, 6],
    #                   [2, 3, 4, 0, 6, 7, 8, 9, 1], [5, 6, 7, 8, 0, 1, 2, 3, 4], [8, 9, 1, 2, 3, 0, 5, 6, 7],
    #                   [3, 4, 5, 6, 7, 8, 0, 1, 2], [6, 7, 8, 9, 1, 2, 3, 0, 5], [9, 1, 2, 3, 4, 5, 6, 7, 0]]
    #         solve_sudoku(puzzle)
    #         self.assertTrue(is_solved(puzzle))


if __name__ == '__main__':
    sys.argv.append("-v")  # Add the verbose command line flag.
    unittest.main()
