#!/usr/bin/env python3
"""
PEX#1 - Create tank gauges that illustrates and calculates the percentage given by the user
CS 210, Introduction to Programming
"""

import turtle
import math

__author__ = "Ludvig Oliver"  # Your name. Ex: John Doe
__section__ = "M3"  # Your section. Ex: M1
__instructor__ = "Lt Col Harder"  # Your instructor. Ex: Lt Col Doe
__date__ = "26 Aug 17"  # Today's date. Ex: 25 Dec 2017
__documentation__ = """I recieved help from C3C Matthew Kuhn.  He gave me information on how to make my code better and 
               solve the problem better by manipulating the given values.  More specifically on line 36 with my shape 
               size.  Also, he helped me by instructing me how to use the format codes to create my commas and decimals.
               More specifically on lines 76 on proper formating of the numbers.  I also referenced older labs on 
               making the draw_square and draw_triangle functions."""  # Multiple lines OK with triple quotes

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960  # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 / 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = WIDTH/30  # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = 16  # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = True  # Set to True for fast, stealthy turtles.


def main():
    # Create the turtle screen and two turtles (leave this as the first line of main).
    screen, artist, writer = turtle_setup()

    # Request user input for fill percentage

    fill = screen.numinput("Input", "Enter Tank Percentage (range: 50 - 95):", 50, 50, 95)
    fill = fill/100


    # Drawing Shapes
    shape_size = (WIDTH - (4*MARGIN)) / 3  # Length of base and diameter

    draw_triangle(artist, MARGIN-WIDTH/2, 4*MARGIN-HEIGHT/2)
    draw_square(artist, 2*MARGIN - WIDTH / 2 + shape_size, 4 * MARGIN - HEIGHT / 2, shape_size)
    draw_circle(artist, WIDTH/2 - (MARGIN + (shape_size/2)), 4 * MARGIN - HEIGHT/2, shape_size/2)

    # Drawing Fill Lines

    # Triangle
    draw_line(artist, MARGIN - WIDTH / 2, 4 * MARGIN - HEIGHT / 2 + (shape_size * fill), shape_size - (shape_size*fill))
    # Square
    draw_line(artist, 2 * MARGIN - WIDTH / 2 + shape_size, 4 * MARGIN - HEIGHT / 2 + (shape_size * fill), shape_size)

    # Circle
    a = ((4 * MARGIN) - (HEIGHT / 2) + (shape_size * fill) - (4 * MARGIN - HEIGHT / 2 + (shape_size/2)))
    c = shape_size/2
    b = math.sqrt(c*c - a*a)  # 1/2 of Chord Length

    # This was frustrating me so I improvised...
    draw_line(artist, WIDTH/2 - (MARGIN + (shape_size/2)), 4 * MARGIN - HEIGHT / 2 + (shape_size * fill), -b)
    draw_line(artist, WIDTH / 2 - (MARGIN + (shape_size/2)), 4 * MARGIN - HEIGHT / 2 + (shape_size * fill), b)

    # Writing Text

    # Fill Percentage
    writer.write("Fill Percentage: {:.1%}" .format(fill), align="center", font=("Courier", FONT_SIZE, "bold"))

    # Area of the Triangle
    writer.penup()
    writer.setposition(5*MARGIN-WIDTH/2, 2 * MARGIN - HEIGHT / 2)
    writer.pendown()

    t_height = shape_size*fill
    t_width = shape_size - (shape_size*fill)
    ts = t_height*t_width
    t_tri = shape_size - t_width
    pt_area = (.5*(t_tri*t_tri) + ts)

    writer.write("{:,.2f}".format(pt_area), align="center", font=("Courier", 12, "bold"))

    # Area of the Square
    writer.penup()
    writer.setposition(6.5*MARGIN - WIDTH / 2 + shape_size, 2 * MARGIN - HEIGHT / 2)
    writer.pendown()

    s_area = (shape_size * shape_size)
    ps_area = s_area * fill

    writer.write("{:,.2f}".format(ps_area), align="center", font=("Courier", 12, "bold"))

    # Area of the Circle
    writer.penup()
    writer.setposition(WIDTH/2 - (MARGIN + (shape_size/2)), 2 * MARGIN - HEIGHT / 2)
    writer.pendown()

    c_area = math.pi * ((shape_size/2) ** 2)
    radius = shape_size/2
    c_length = 2*radius*math.sqrt(1-(a/radius)**2)
    angle = 2*math.asin(c_length/(2*radius))
    tc_area = ((radius**2)/2)*(angle - math.sin(angle))
    pc_area = c_area - tc_area

    writer.write("{:,.2f}".format(pc_area), align="center", font=("Courier", 12, "bold"))

    # Wait for the user to click before closing the window (leave this as the last line of main).
    screen.exitonclick()


def draw_triangle(artist, x, y):
    """
    Use the given turtle to draw a triangle with one corner at coordinate (x,y).

    Note: The orientation of the triangle is dependent on the heading of the turtle.
    If the turtle's heading is zero, the (x,y) coordinate is the lower-left corner.

    :param turtle.Turtle artist: The turtle to do the drawing.
    :param int x: The x-coordinate of one corner of the triangle.
    :param int y: The y-coordinate of one corner of the triangle.
    """
    # Lift the pen and move to the indicated position.
    artist.penup()
    artist.setposition(x, y)
    artist.pendown()

    shape_size = (WIDTH - (4 * MARGIN)) / 3
    hyp = math.sqrt(shape_size*shape_size + shape_size*shape_size)

    # Draw the triangle.
    artist.forward(shape_size)
    artist.left(135)
    artist.forward(hyp)
    artist.left(135)
    artist.forward(shape_size)
    artist.left(90)


def draw_square(artist: object, x, y, size):
    """
    Use the given turtle to draw a square with one corner at coordinate (x,y).

    Note: The orientation of the square is dependent on the heading of the turtle.
    If the turtle's heading is zero, the (x,y) coordinate is the lower-left corner.

    :param turtle.Turtle artist: The turtle to do the drawing.
    :param int x: The x-coordinate of one corner of the square.
    :param int y: The y-coordinate of one corner of the square.
    :param int size: The length of one side of the square.
    """
    # Lift the pen and move to the indicated position.
    artist.penup()
    artist.setposition(x, y)
    artist.pendown()

    # Draw the square.
    for _ in range(4):
        artist.forward(size)
        artist.left(90)


def draw_circle(artist, x, y, size):

    artist.penup()
    artist.setposition(x, y)
    artist.pendown()

    artist.circle(size)


def draw_line(artist, x, y, size):

    artist.penup()
    artist.setposition(x, y)
    artist.pendown()

    artist.color("red")
    artist.forward(size)

# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================


def turtle_setup():

    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup(WIDTH, HEIGHT, MARGIN, MARGIN)
    screen.bgcolor("SkyBlue")

    # Create two turtles, one for drawing and one for writing.
    turtle.TurtleScreen._RUNNING = True  # Get around bug in v3.5.2 http://bugs.python.org/issue26571
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape("turtle")

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay(0)
        artist.hideturtle()
        artist.speed("fastest")
        writer.hideturtle()
        writer.speed("fastest")

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading(90)  # Straight up, which makes it look sort of like a cursor.
    writer.penup()  # A turtle's pen does not have to be down to write text.
    writer.setposition(0, HEIGHT // 2 - FONT_SIZE * 4)  # Centered at top of the screen.

    return screen, artist, writer


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    print(__doc__.strip())
    print("Author:", (__author__ if __author__ else "\033[91mBLANK (You must fill in the __author__ metadata)\033[0m"))
    print("Section:",
          (__section__ if __section__ else "\033[91mBLANK (You must fill in the __section__ metadata)\033[0m"))
    print("Instructor:",
          (__instructor__ if __instructor__ else "\033[91mBLANK (You must fill in the __instructor__ metadata)\033[0m"))
    print("Date:", (__date__ if __date__ else "\033[91mBLANK (You must fill in the __date__ metadata)\033[0m"))
    print("Documentation:", (
        __documentation__ if __documentation__.strip() else
        "\033[91mBLANK (You must fill in the __documentation__ metadata)\033[0m"))
    b = b'CmltcG9ydCBnZXRwYXNzLCBoYXNobGliLCBjb2RlY3MsIHN0cmluZyBhcyBfX1MKdSA9IGdldHBhc3MuZ2V0dXNlcigpCmggPSBoYXN' + \
        b'obGliLnNoYTI1Nih1LmVuY29kZSgpKS5oZXhkaWdlc3QoKQpyID0gY29kZWNzLmVuY29kZSh1Lmxvd2VyKCkudHJhbnNsYXRlKHtvcm' + \
        b'Qoayk6IE5vbmUgZm9yIGsgaW4gX19TLmRpZ2l0c30pLnJlcGxhY2UoJy4nLCcnKSwgJ3JvdF8xMycpCndpdGggb3BlbihfX2ZpbGVfX' + \
        b'ywgInIiKSBhcyBmOgogICAgaWYgaCBub3QgaW4gZi5yZWFkKCk6CiAgICAgICAgd2l0aCBvcGVuKF9fZmlsZV9fLCAiYSIpIGFzIGY6' + \
        b'CiAgICAgICAgICAgIHByaW50KCIjIiwgaCwgciwgZmlsZT1mKQo='
    try:
        import base64

        eval(compile(base64.b64decode(b), '<string>', "exec"))
    except:
        pass
    finally:
        main()
# 72de204a7312018d00e34442a7df16633351eefd989d08b6170f0796da863df7 pyhqivtbyvire
