#!/usr/bin/env python3
"""
PUT YOUR DESCRIPTION OF THIS FILE HERE
CS 210, Introduction to Programming
"""

__author__ = "C3C Ludvig Oliver"  # Your name. Ex: John Doe
__section__ = "M3/M4"  # Your section. Ex: M1
__instructor__ = "Lt Col Harder"  # Your instructor. Ex: Lt Col Doe
__date__ = "14 Nov 2017"  # Today's date. Ex: 25 Dec 2017
__documentation__ = """ I received help from C3C Matt Kuhn on understanding what the purpose of the 
 turns_played and what its supposed to track. I resulted in adding onto the turns_played object anytime a 
 player won, loss or tied with the line 'self.__games_played +=1'. """  # Multiple lines OK with triple quotes


def main():
    # Initialize Game
    game_over = False

    print("\nLets Play a Game of Tic Tac Toe")
    print("Your Goal is to Win Best 2 Out of 3...")

    name_one = input("\nPlayer 1: What's Your Name? ")
    marker_one = input("Input Marker: ")
    player_one = Player(name_one, marker_one)

    name_two = input("\nPlayer 2: What's Your Name? ")
    marker_two = input("Input Marker: ")
    player_two = Player(name_two, marker_two)

    game = TicTacToe(player_one, player_two)
    print("\nRound {}".format(game.rounds))

    while game_over is False:

        game.change_players(player_one, player_two)

        board_string(game.play)

        if game.turns_played % 2 == 0:
            game.current_player = game.players[0]
        else:
            game.current_player = game.players[1]

        print(game.current_player)
        print(game.players)

        move = input("{} Make Your Move (1-9): ".format(game.current_player.name))
        placement = position(int(move))
        game.play_move(placement[0], placement[1])
        game.add_turns_played()
        checking = game.check_win()

        if checking == game.players[0]:
            print("\n{} Wins! {} Losses...".format(game.players[0].name, game.players[1].name))
            player_one.record_win()
            player_two.record_loss()
            game.add_rounds()
            game.reset_turns_played()
            if game.players[0].wins != 2:
                print("\nRound {}".format(game.rounds))
        if checking == game.players[1]:
            print("\n{} Wins! {} Losses...".format(game.players[1].name, game.players[0].name))
            player_two.record_win()
            player_one.record_loss()
            game.add_rounds()
            game.reset_turns_played()
            if game.players[1].wins != 2:
                print("\nRound {}".format(game.rounds))
        if checking == "None" and game.turns_played > 8:
            print("\nTry Again...")
            player_one.record_tie()
            player_two.record_tie()
            game.add_rounds()
            game.reset_turns_played()
            print("\nRound {}".format(game.rounds))

        if player_one.wins == 2 or player_two.wins == 2:
            game_over = True

    if player_one.wins == 2:
        print("\n{} Wins It All!".format(player_one.name))
        print(player_one)
        print(player_two)

    if player_two.wins == 2:
        print("\n{} Wins It All".format(player_two.name))
        print(player_two)
        print(player_one)


def board_string(board):
    game_board = ''

    for i in range(len(board)):
        for x in range(len(board[i])):
            if x == 0 or x == 1:
                game_board = game_board + str(board[i][x]) + ' '  # Creates the lists of lists into a string
            else:
                game_board = game_board + str(board[i][x])

        game_board = game_board + '\n'

    print("\n" + game_board)
    return game_board


def position(place):
    if place == 1:
        row = 1
        column = 1
        return row, column
    if place == 2:
        row = 1
        column = 2
        return row, column
    if place == 3:
        row = 1
        column = 3
        return row, column
    if place == 4:
        row = 2
        column = 1
        return row, column
    if place == 5:
        row = 2
        column = 2
        return row, column
    if place == 6:
        row = 2
        column = 3
        return row, column
    if place == 7:
        row = 3
        column = 1
        return row, column
    if place == 8:
        row = 3
        column = 2
        return row, column
    if place == 9:
        row = 3
        column = 3
        return row, column


class TicTacToe:
    """ A Tic Tac Toe game model """

    def __init__(self, player1, player2):
        """
        Initializes a new TicTacToe game.
        :param Player player1: the first player to take a turn
        :param Player player2: the second player to take a turn
        """

        self.__current_player = player1
        self.__players = (player1, player2)
        self.__winner = None
        self.__loser = None
        self.__turns_played = 0
        self.__rounds = 1
        self.__play = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

    @property
    def current_player(self):
        """
        :return: Which ever player is currently playing
        :rtype: Player
        """
        return self.__current_player

    @current_player.setter
    def current_player(self, players):
        # if self.turns_played % 2 == 0:
        #     self.__current_player = self.players[0]
        # else:
        #     self.__current_player = self.players[1]

        self.__current_player = players

    @property
    def players(self):
        """
        :return: Shows the players in the game
        """
        return self.__players[0], self.__players[1]

    def change_players(self, player_one, player_two):
        if self.rounds % 2 == 1:
            self.__players = (player_one, player_two)
        else:
            self.__players = (player_two, player_one)

    @property
    def winner(self):
        """
        :return: The winner of the round
        """
        return self.__winner

    @winner.setter
    def winner(self, player):

        self.__winner = player

    @property
    def loser(self):
        """
        :return: The loser of the round
        """
        return self.__loser

    @loser.setter
    def loser(self, player):

        self.__loser = player

    @property
    def turns_played(self):
        """
        :return: Shows how many turns have been made
        """

        return self.__turns_played

    def reset_turns_played(self):
        self.__turns_played = 1

    def add_turns_played(self):
        if self.turns_played > 8:
            self.__turns_played = 0
            self.add_rounds()
            self.__play = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

        self.__turns_played += 1

    @property
    def rounds(self):
        return self.__rounds

    def add_rounds(self):
        self.__rounds += 1

    @property
    def play(self):
        return self.__play

    def play_move(self, row, column):

        if self.play[row - 1][column - 1] == self.players[0].marker \
                or self.play[row - 1][column - 1] == self.players[1].marker:
            return

        self.__play[row - 1][column - 1] = self.current_player.marker

    def player_at(self, row, column):
        if self.play[row - 1][column - 1] == self.players[0].marker:
            return self.players[0]

        if self.play[row - 1][column - 1] == self.players[1].marker:
            return self.players[1]

        return None

    def check_win(self):

        # Check Row
        for i in range(len(self.play)):
            if self.play[i][0] == self.players[0].marker and self.play[i][1] == self.players[0].marker \
                    and self.play[i][2] == self.players[0].marker:
                self.__play = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
                return self.players[0]

            if self.play[i][0] == self.players[1].marker and self.play[i][1] == self.players[1].marker \
                    and self.play[i][2] == self.players[1].marker:
                self.__play = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
                return self.players[1]

        # Check Column
        for i in range(len(self.play)):
            if self.play[0][i] == self.players[0].marker and self.play[1][i] == self.players[0].marker \
                    and self.play[2][i] == self.players[0].marker:
                self.__play = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
                return self.players[0]

            if self.play[0][i] == self.players[1].marker and self.play[1][i] == self.players[1].marker \
                    and self.play[2][i] == self.players[1].marker:
                self.__play = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
                return self.players[1]

        # Check Cross - Player 1
        if self.play[0][0] == self.players[0].marker and self.play[1][1] == self.players[0].marker \
                and self.play[2][2] == self.players[0].marker:
            self.__play = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
            return self.players[0]
        if self.play[0][2] == self.players[0].marker and self.play[1][1] == self.players[0].marker \
                and self.play[2][0] == self.players[0].marker:
            self.__play = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
            return self.players[0]

        # Check Cross - Player 2
        if self.play[0][0] == self.players[1].marker and self.play[1][1] == self.players[1].marker \
                and self.play[2][2] == self.players[1].marker:
            self.__play = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
            return self.players[1]
        if self.play[0][2] == self.players[1].marker and self.play[1][1] == self.players[1].marker \
                and self.play[2][0] == self.players[1].marker:
            self.__play = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
            return self.players[1]

        return None


class Player:
    """ A player in the Tic Tac Toe game. """

    def __init__(self, name, marker="!"):
        """
        Initializes a new Player with a name and a marker.
        :param str name: the player's name
        :param str marker: the marker to use on the board
        """
        self.name = name

        if marker is None:
            self.marker = "!"
        elif len(marker) > 1:
            self.marker = marker[0]
        elif marker == "":
            self.marker = "!"
        else:
            self.marker = marker

        self.__wins = 0
        self.__losses = 0
        self.__ties = 0
        self.__games_played = 0

    def __str__(self):
        """
        :return: Allows for information to be displayed
        """
        # return "{}: {}, {} wins, {} losses, {} ties".format(self.name, self.marker, self.wins, self.losses, self.ties)

    @property
    def wins(self):
        """
        :return: Amount of Wins
        :rtype: int
        """
        return self.__wins

    def record_win(self):
        """
        Adds to Win Count
        :rtype: int
        """
        self.__games_played += 1
        self.__wins += 1

    @property
    def losses(self):
        """
        :return: Amount of Losses
        :rtype: int
        """
        return self.__losses

    def record_loss(self):
        """
        Adds to Loss Count
        :rtype: int
        """
        self.__games_played += 1
        self.__losses += 1

    @property
    def ties(self):
        """
        :return: Amount of Ties
        :rtype: int
        """
        return self.__ties

    def record_tie(self):
        """
        Adds to Tie Count
        :rtype: int
        """
        self.__games_played += 1
        self.__ties += 1

    @property
    def games_played(self):
        """
        :return: Amount of rounds played
        :rtype: int
        """
        return self.__games_played


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

if __name__ == "__main__":
    print(__doc__.strip())
    print("Author:", (__author__ if __author__ else "\033[91mBLANK (You must fill in the __author__ metadata)\033[0m"))
    print("Section:",
          (__section__ if __section__ else "\033[91mBLANK (You must fill in the __section__ metadata)\033[0m"))
    print("Instructor:",
          (__instructor__ if __instructor__ else "\033[91mBLANK (You must fill in the __instructor__ metadata)\033[0m"))
    print("Date:", (__date__ if __date__ else "\033[91mBLANK (You must fill in the __date__ metadata)\033[0m"))
    print("Documentation:", (
        __documentation__ if __documentation__.strip() else
        "\033[91mBLANK (You must fill in the __documentation__ metadata)\033[0m"))
    _ = b'CmltcG9ydCBnZXRwYXNzLCBoYXNobGliLCBjb2RlY3MsIHN0cmluZyBhcyBfX1MKdSA9IGdldHBhc3MuZ2V0dXNlcigpCmggPSBoYXN' + \
        b'obGliLnNoYTI1Nih1LmVuY29kZSgpKS5oZXhkaWdlc3QoKQpyID0gY29kZWNzLmVuY29kZSh1Lmxvd2VyKCkudHJhbnNsYXRlKHtvcm' + \
        b'Qoayk6IE5vbmUgZm9yIGsgaW4gX19TLmRpZ2l0c30pLnJlcGxhY2UoJy4nLCcnKSwgJ3JvdF8xMycpCndpdGggb3BlbihfX2ZpbGVfX' + \
        b'ywgInIiKSBhcyBmOgogICAgaWYgaCBub3QgaW4gZi5yZWFkKCk6CiAgICAgICAgd2l0aCBvcGVuKF9fZmlsZV9fLCAiYSIpIGFzIGY6' + \
        b'CiAgICAgICAgICAgIHByaW50KCIjIiwgaCwgciwgZmlsZT1mKQo='
    # noinspection PyBroadException
    try:
        import base64

        eval(compile(base64.b64decode(_), '<string>', "exec"))
    except:
        pass
    finally:
        main()
# 72de204a7312018d00e34442a7df16633351eefd989d08b6170f0796da863df7 pyhqivtbyvire
