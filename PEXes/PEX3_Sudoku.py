#!/usr/bin/env python3
"""
PUT YOUR DESCRIPTION OF THIS FILE HERE
CS 210, Introduction to Programming
"""

import easygui  # For easygui.fileopenbox.
import os  # For os.path.basename.
import string

__author__ = "Ludvig Oliver"  # Your name. Ex: John Doe
__section__ = "M3/M4"  # Your section. Ex: M1
__instructor__ = "LtCol Harder"  # Your instructor. Ex: Lt Col Doe
__date__ = "2 Oct 17"  # Today's date. Ex: 25 Dec 2017
__documentation__ = """ I received help from C3C Kuhn on talking me through ways to debug my code using the unit 
                test. He told me to copy and paste the unit test that I was not passing and use the debug
                function to find my error and that it would be minimal changes if my code is
                functioning correctly. """  # Multiple lines OK with triple quotes

DATA_DIRECTORY = "../Data/Sudoku"


def main():
    """
    Main program to do Sudoku, so here's a Sudoku haiku:

    One through nine in place
    To open the matrix door
    Let logic guide you
    """

    # TODO 1b: Demonstrate the str_sudoku function with the given puzzles.
    # print( str_sudoku( PUZZLE ) )
    # print( str_sudoku( PUZZLE_SOLVED ) )

    # TODO 2b: Demonstrate the print_sudoku function with the given puzzles.
    # print_sudoku( PUZZLE )
    # print_sudoku( PUZZLE_SOLVED )

    # TODO 3b: Demonstrate the create_sudoku puzzle with the given data.
    # print_sudoku( create_sudoku( DATA ) )
    # print_sudoku( create_sudoku( DATA_SOLVED ) )


    # # TODO 4b: Demonstrate the open_sudoku function with the given files.
    # for test_filename in ["Sudoku_Blank.txt", "Sudoku00.txt", "Sudoku01.txt", "Sudoku02.txt", "Sudoku03.txt"]:
    #     fullpath = os.path.join(DATA_DIRECTORY, test_filename)
    #     sudoku_data = open_sudoku(fullpath)
    #     print_sudoku(sudoku_data)

    # TODO 5b: Demonstrate the is_solved function with the given puzzles.
    # print( is_solved( PUZZLE ), is_solved( PUZZLE_SOLVED ) )

    # TODO 6b: Demonstrate the is_valid function with the given puzzles.
    # print( is_valid( PUZZLE ), is_valid( PUZZLE_SOLVED ), is_valid( PUZZLE_INVALID ) )

    # TODO 7: Implement the main program as described
    #
    # run = True
    #
    # while run is True:
    #     file = easygui.fileopenbox('Open Sudoku File', 'Open Sesame', '../Data/Sudoku/*.txt')
    #     if file is None:
    #         run = False
    #     else:
    #         open_file = open_sudoku(file)
    #         print_sudoku(open_file)
    #
    #         valid = is_valid(open_file)
    #         solved = is_solved(open_file)
    #
    #         if valid is True and solved is True:
    #             print('The puzzle IS valid and IS solved.')
    #         elif valid is True and solved is False:
    #             print('The puzzle IS valid and is NOT solved.')
    #         elif valid is False and solved is True:
    #             print('The puzzle is NOT valid and IS is solved.')
    #         else:
    #             print('The puzzle is NOT valid and is NOT solved.')

    puzzle = create_sudoku(DATA)
    print_sudoku(puzzle)
    print("Solved?", is_solved(puzzle))

    print("Solving...")
    solve_sudoku(puzzle)
    print_sudoku(puzzle)
    print("Solved?", is_solved(puzzle))

# TODO 1a: Implement the str_sudoku function as described
def str_sudoku(puzzle):
    """
    Creates a string of puzzle values suitable for printing to a file.

    The string created by this function is formatted such that it could be
    passed to create_sudoku function and recreate the same puzzle.

    Note: This function DOES NOT modify the puzzle.

    :param list[list[int]] puzzle: The Sudoku puzzle as a 9x9 nested list of integers.
    :return: A string suitable for printing to a file or passing to create_sudoku.
    """
    matrix = ''

    for i in range(len(puzzle)):
        for x in range(len(puzzle[i])):
            matrix = matrix + str(puzzle[i][x]) + ' '  # Creates the lists of lists into a string

        matrix = matrix + '\n'

    return matrix


# TODO 2a: Implement the print_sudoku function as described
def print_sudoku(puzzle):
    """
    Prints the nested list structure in pretty rows and columns.

    For example, PUZZLE (bottom of this file) would print as follows:
    +===+===+===+===+===+===+===+===+===+
    # 1 | 8 |   # 6 |   | 9 #   | 5 | 7 #
    +---+---+---+---+---+---+---+---+---+
    # 5 |   |   #   |   |   #   |   | 3 #
    +---+---+---+---+---+---+---+---+---+
    #   |   | 2 #   | 8 |   # 4 |   |   #
    +===+===+===+===+===+===+===+===+===+
    # 7 |   |   # 8 | 4 | 5 #   |   | 1 #
    +---+---+---+---+---+---+---+---+---+
    #   |   | 3 # 2 |   | 1 # 9 |   |   #
    +---+---+---+---+---+---+---+---+---+
    # 2 |   |   # 9 | 6 | 3 #   |   | 5 #
    +===+===+===+===+===+===+===+===+===+
    #   |   | 1 #   | 5 |   # 8 |   |   #
    +---+---+---+---+---+---+---+---+---+
    # 8 |   |   #   |   |   #   |   | 4 #
    +---+---+---+---+---+---+---+---+---+
    # 9 | 4 |   # 3 |   | 8 #   | 7 | 2 #
    +===+===+===+===+===+===+===+===+===+

    Note: This function MUST NOT modify the puzzle.

    :param list[list[int]] puzzle: The Sudoku puzzle as a 9x9 nested list of integers.
    :return: None
    """

    border = '+===' * 9 + '+'  # Creates the double lined borders
    rows = '+---' * 9 + '+'  # Creates the single lined borders
    numbers = ''  # Place to set values in

    for i in range(0, 9):
        if i == 0 or i == 3 or i == 6:
            numbers = numbers + border + '\n' + '#'  # creates the double lined borders in the large box
        for x in range(len(puzzle[0:9])):
            if x == 0 or x == 3 or x == 6:
                numbers = numbers + ' {} '.format(puzzle[i][x])  # prints the numbers on the left of the box
            if x == 1 or x == 4 or x == 7:
                numbers = numbers + '| {} '.format(puzzle[i][x])  # prints the numbers in the center of the box
            if x == 2 or x == 5 or x == 8:
                numbers = numbers + '| {} #'.format(puzzle[i][x])  # prints the numbers on the right of the box
            # if x == 9:
            #     numbers = numbers + '#'
        if i == 0 or i == 1 or i == 3 or i == 4 or i == 6 or i == 7:
            numbers = numbers + '\n' + rows + '\n' + '#'  # creates the row border in the bottom of the values
        if i == 2 or i == 5:
            numbers = numbers + '\n'
        if i == 8:
            numbers = numbers + '\n' + border + '\n'

    numbers = numbers.replace('0', ' ')  # Removes the 0 to have white space
    print(numbers)

    return numbers


# TODO 3a: Implement the create_sudoku function as described
def create_sudoku(data):
    """
    Creates a 9x9 nested list of integers from a string with 81 separate values.

    :param str data: A string with 81 separate integer values, [0-9].
    :return: The Sudoku puzzle as a 9x9 nested list of integers.
    :rtype: list[list[int]]
    """

    split_data = data.split()  # splits data lists to numbers -- gets rid of unnecessary white space
    matrix = []  # Sets up small list to append values in
    system = []  # place to put the lists of lists

    counter = 0  # sets up for while loop
    start = 0  # for loop in the while loop
    stop = 9  # sets space for next range

    while counter < 9:
        for i in range(start, stop):
            matrix.append(int(split_data[i]))  # Creates the small list to later put into the big list

        start = start + 9
        stop = stop + 9

        system.append(matrix)  # Sets up the Big List
        matrix = []

        counter = counter + 1

    return system


# TODO 4a: Implement the open_sudoku function as described
def open_sudoku(filename):
    """
    Opens the given file, parses the contents, and returns a Sudoku puzzle.

    This function prints to the console any comment lines in the file.

    :param str filename: The file name.
    :return: The Sudoku puzzle as a 9x9 nested list of integers.
    :rtype: list[list[int]]
    """
    str_file = []
    int_file = []

    with open(filename) as opened:
        open_file = opened.read().splitlines()

    for i in range(len(open_file)):
        if open_file[i][0] == '#':
            str_file.append(open_file[i])
        else:
            for x in range(len(open_file[i])):
                if open_file[i][x] != ' ':
                    int_file.append(open_file[i][x])

    for y in range(len(str_file)):
        print(str_file[y])

    int_file = str_sudoku(int_file)
    int_file = create_sudoku(int_file)

    return int_file


# TODO 5a: Implement the is_solved function as described
def is_solved(puzzle):
    """
    Determines if a Sudoku puzzle is valid and complete.

    Note: This function MUST NOT modify the puzzle.

    :param list[list[int]] puzzle: The Sudoku puzzle as a 9x9 nested list of integers.
    :return: True if the puzzle is valid and complete; False otherwise.
    """

    # Checks Columns and Rows
    solution = 45
    rows = 0
    columns = 0
    result = True
    for i in range(len(puzzle)):
        for x in range(len(puzzle[i])):
            rows += int(puzzle[i][x])
            columns += int(puzzle[x][i])
        if rows != solution and columns != solution:
            result = False
        rows = 0
        columns = 0

    # Checks for 0's
    for i in range(len(puzzle)):
        for x in range(len(puzzle[i])):
            if puzzle[i][x] == 0:
                result = False

    # Checks Box#1
    counter = 0
    for i in range(0, 3):
        for x in range(0, 3):
            counter = counter + puzzle[i][x]
    if counter != solution:
        result = False

    # Checks Box#2
    counter = 0
    for i in range(0, 3):
        for x in range(3, 6):
            counter = counter + puzzle[i][x]
    if counter != solution:
        result = False

    # Checks Box#3
    counter = 0
    for i in range(0, 3):
        for x in range(6, 9):
            counter = counter + puzzle[i][x]
    if counter != solution:
        result = False

    # Checks Box#4
    counter = 0
    for i in range(3, 6):
        for x in range(0, 3):
            counter = counter + puzzle[i][x]
    if counter != solution:
        result = False

    # Checks Box#5
    counter = 0
    for i in range(3, 6):
        for x in range(3, 6):
            counter = counter + puzzle[i][x]
    if counter != solution:
        result = False

    # Checks Box#6
    counter = 0
    for i in range(3, 6):
        for x in range(6, 9):
            counter = counter + puzzle[i][x]
    if counter != solution:
        result = False

    # Checks Box#7
    counter = 0
    for i in range(6, 9):
        for x in range(0, 3):
            counter = counter + puzzle[i][x]
    if counter != solution:
        result = False

    # Checks Box#8
    counter = 0
    for i in range(6, 9):
        for x in range(3, 6):
            counter = counter + puzzle[i][x]
    if counter != solution:
        result = False

    # Checks Box#9
    counter = 0
    for i in range(6, 9):
        for x in range(6, 9):
            counter = counter + puzzle[i][x]
    if counter != solution:
        result = False

    valid = is_valid(puzzle)
    if valid is False and result is True:
        result = False

    return result


# TODO 6a: Implement the is_valid function as described
def is_valid(puzzle):
    """
    Determines if a Sudoku puzzle is valid, but not necessarily complete.

    Note: This function MUST NOT modify the puzzle.

    :param list[list[int]] puzzle: The Sudoku puzzle as a 9x9 nested list of integers.
    :return: True if the puzzle is valid; False otherwise.
    """

    valid = True

    # Checks Rows
    for i in range(len(puzzle)):  # Checks Rows

        one = 0
        two = 0
        three = 0
        four = 0
        five = 0
        six = 0
        seven = 0
        eight = 0
        nine = 0

        for x in range(len(puzzle)):
            if int(puzzle[i][x]) == 1:
                one = one + 1
            if int(puzzle[i][x]) == 2:
                two = two + 1
            if int(puzzle[i][x]) == 3:
                three = three + 1
            if int(puzzle[i][x]) == 4:
                four = four + 1
            if int(puzzle[i][x]) == 5:
                five = five + 1
            if int(puzzle[i][x])== 6:
                six = six + 1
            if int(puzzle[i][x]) == 7:
                seven = seven + 1
            if int(puzzle[i][x]) == 8:
                eight = eight + 1
            if int(puzzle[i][x]) == 9:
                nine = nine + 1

        if one > 1 or two > 1 or three > 1 or four > 1 or five > 1 or six > 1 or seven > 1 or eight > 1 or nine > 1:
            valid = False

    # Checks Columns
    for i in range(len(puzzle)):
        one = 0
        two = 0
        three = 0
        four = 0
        five = 0
        six = 0
        seven = 0
        eight = 0
        nine = 0
        for x in range(len(puzzle)):
            if int(puzzle[x][i]) == 1:
                one = one + 1
            if int(puzzle[x][i]) == 2:
                two = two + 1
            if int(puzzle[x][i]) == 3:
                three = three + 1
            if int(puzzle[x][i]) == 4:
                four = four + 1
            if int(puzzle[x][i]) == 5:
                five = five + 1
            if int(puzzle[x][i]) == 6:
                six = six + 1
            if int(puzzle[x][i]) == 7:
                seven = seven + 1
            if int(puzzle[x][i]) == 8:
                eight = eight + 1
            if int(puzzle[x][i]) == 9:
                nine = nine + 1

        if one > 1 or two > 1 or three > 1 or four > 1 or five > 1 or six > 1 or seven > 1 or eight > 1 or nine > 1:
            valid = False

    # Checks Box#1
    one = 0
    two = 0
    three = 0
    four = 0
    five = 0
    six = 0
    seven = 0
    eight = 0
    nine = 0
    for i in range(0, 3):
        for x in range(0, 3):
            if int(puzzle[i][x]) == 1:
                one = one + 1
            if int(puzzle[i][x]) == 2:
                two = two + 1
            if int(puzzle[i][x]) == 3:
                three = three + 1
            if int(puzzle[i][x]) == 4:
                four = four + 1
            if int(puzzle[i][x]) == 5:
                five = five + 1
            if int(puzzle[i][x]) == 6:
                six = six + 1
            if int(puzzle[i][x]) == 7:
                seven = seven + 1
            if int(puzzle[i][x]) == 8:
                eight = eight + 1
            if int(puzzle[i][x]) == 9:
                nine = nine + 1
    if one > 1 or two > 1 or three > 1 or four > 1 or five > 1 or six > 1 or seven > 1 or eight > 1 or nine > 1:
        valid = False

    # Checks Box#2
    one = 0
    two = 0
    three = 0
    four = 0
    five = 0
    six = 0
    seven = 0
    eight = 0
    nine = 0
    for i in range(0, 3):
        for x in range(3, 6):
            if int(puzzle[i][x]) == 1:
                one = one + 1
            if int(puzzle[i][x]) == 2:
                two = two + 1
            if int(puzzle[i][x]) == 3:
                three = three + 1
            if int(puzzle[i][x]) == 4:
                four = four + 1
            if int(puzzle[i][x]) == 5:
                five = five + 1
            if int(puzzle[i][x]) == 6:
                six = six + 1
            if int(puzzle[i][x]) == 7:
                seven = seven + 1
            if int(puzzle[i][x]) == 8:
                eight = eight + 1
            if int(puzzle[i][x]) == 9:
                nine = nine + 1
    if one > 1 or two > 1 or three > 1 or four > 1 or five > 1 or six > 1 or seven > 1 or eight > 1 or nine > 1:
        valid = False

    # Checks Box#3
    one = 0
    two = 0
    three = 0
    four = 0
    five = 0
    six = 0
    seven = 0
    eight = 0
    nine = 0
    for i in range(0, 3):
        for x in range(6, 9):
            if int(puzzle[i][x]) == 1:
                one = one + 1
            if int(puzzle[i][x]) == 2:
                two = two + 1
            if int(puzzle[i][x]) == 3:
                three = three + 1
            if int(puzzle[i][x]) == 4:
                four = four + 1
            if int(puzzle[i][x]) == 5:
                five = five + 1
            if int(puzzle[i][x]) == 6:
                six = six + 1
            if int(puzzle[i][x]) == 7:
                seven = seven + 1
            if int(puzzle[i][x]) == 8:
                eight = eight + 1
            if int(puzzle[i][x]) == 9:
                nine = nine + 1
    if one > 1 or two > 1 or three > 1 or four > 1 or five > 1 or six > 1 or seven > 1 or eight > 1 or nine > 1:
        valid = False

    # Checks Box#4
    one = 0
    two = 0
    three = 0
    four = 0
    five = 0
    six = 0
    seven = 0
    eight = 0
    nine = 0
    for i in range(3, 6):
        for x in range(0, 3):
            if int(puzzle[i][x]) == 1:
                one = one + 1
            if int(puzzle[i][x]) == 2:
                two = two + 1
            if int(puzzle[i][x]) == 3:
                three = three + 1
            if int(puzzle[i][x]) == 4:
                four = four + 1
            if int(puzzle[i][x]) == 5:
                five = five + 1
            if int(puzzle[i][x]) == 6:
                six = six + 1
            if int(puzzle[i][x]) == 7:
                seven = seven + 1
            if int(puzzle[i][x]) == 8:
                eight = eight + 1
            if int(puzzle[i][x]) == 9:
                nine = nine + 1
    if one > 1 or two > 1 or three > 1 or four > 1 or five > 1 or six > 1 or seven > 1 or eight > 1 or nine > 1:
        valid = False

    # Checks Box#5
    one = 0
    two = 0
    three = 0
    four = 0
    five = 0
    six = 0
    seven = 0
    eight = 0
    nine = 0
    for i in range(3, 6):
        for x in range(3, 6):
            if int(puzzle[i][x]) == 1:
                one = one + 1
            if int(puzzle[i][x]) == 2:
                two = two + 1
            if int(puzzle[i][x]) == 3:
                three = three + 1
            if int(puzzle[i][x]) == 4:
                four = four + 1
            if int(puzzle[i][x]) == 5:
                five = five + 1
            if int(puzzle[i][x]) == 6:
                six = six + 1
            if int(puzzle[i][x]) == 7:
                seven = seven + 1
            if int(puzzle[i][x]) == 8:
                eight = eight + 1
            if int(puzzle[i][x]) == 9:
                nine = nine + 1
    if one > 1 or two > 1 or three > 1 or four > 1 or five > 1 or six > 1 or seven > 1 or eight > 1 or nine > 1:
        valid = False

    # Checks Box#6
    one = 0
    two = 0
    three = 0
    four = 0
    five = 0
    six = 0
    seven = 0
    eight = 0
    nine = 0
    for i in range(3, 6):
        for x in range(6, 9):
            if int(puzzle[i][x]) == 1:
                one = one + 1
            if int(puzzle[i][x]) == 2:
                two = two + 1
            if int(puzzle[i][x]) == 3:
                three = three + 1
            if int(puzzle[i][x]) == 4:
                four = four + 1
            if int(puzzle[i][x]) == 5:
                five = five + 1
            if int(puzzle[i][x]) == 6:
                six = six + 1
            if int(puzzle[i][x]) == 7:
                seven = seven + 1
            if int(puzzle[i][x]) == 8:
                eight = eight + 1
            if int(puzzle[i][x]) == 9:
                nine = nine + 1
    if one > 1 or two > 1 or three > 1 or four > 1 or five > 1 or six > 1 or seven > 1 or eight > 1 or nine > 1:
        valid = False

    # Checks Box#7
    one = 0
    two = 0
    three = 0
    four = 0
    five = 0
    six = 0
    seven = 0
    eight = 0
    nine = 0
    for i in range(6, 9):
        for x in range(0, 3):
            if int(puzzle[i][x]) == 1:
                one = one + 1
            if int(puzzle[i][x]) == 2:
                two = two + 1
            if int(puzzle[i][x]) == 3:
                three = three + 1
            if int(puzzle[i][x]) == 4:
                four = four + 1
            if int(puzzle[i][x]) == 5:
                five = five + 1
            if int(puzzle[i][x]) == 6:
                six = six + 1
            if int(puzzle[i][x]) == 7:
                seven = seven + 1
            if int(puzzle[i][x]) == 8:
                eight = eight + 1
            if int(puzzle[i][x]) == 9:
                nine = nine + 1
    if one > 1 or two > 1 or three > 1 or four > 1 or five > 1 or six > 1 or seven > 1 or eight > 1 or nine > 1:
        valid = False

    # Checks Box#8
    one = 0
    two = 0
    three = 0
    four = 0
    five = 0
    six = 0
    seven = 0
    eight = 0
    nine = 0
    for i in range(6, 9):
        for x in range(3, 6):
            if int(puzzle[i][x]) == 1:
                one = one + 1
            if int(puzzle[i][x]) == 2:
                two = two + 1
            if int(puzzle[i][x]) == 3:
                three = three + 1
            if int(puzzle[i][x]) == 4:
                four = four + 1
            if int(puzzle[i][x]) == 5:
                five = five + 1
            if int(puzzle[i][x]) == 6:
                six = six + 1
            if int(puzzle[i][x]) == 7:
                seven = seven + 1
            if int(puzzle[i][x]) == 8:
                eight = eight + 1
            if int(puzzle[i][x]) == 9:
                nine = nine + 1
    if one > 1 or two > 1 or three > 1 or four > 1 or five > 1 or six > 1 or seven > 1 or eight > 1 or nine > 1:
        valid = False

    # Checks Box#9
    one = 0
    two = 0
    three = 0
    four = 0
    five = 0
    six = 0
    seven = 0
    eight = 0
    nine = 0
    for i in range(6, 9):
        for x in range(6, 9):
            if int(puzzle[i][x]) == 1:
                one = one + 1
            if int(puzzle[i][x]) == 2:
                two = two + 1
            if int(puzzle[i][x]) == 3:
                three = three + 1
            if int(puzzle[i][x]) == 4:
                four = four + 1
            if int(puzzle[i][x]) == 5:
                five = five + 1
            if int(puzzle[i][x]) == 6:
                six = six + 1
            if int(puzzle[i][x]) == 7:
                seven = seven + 1
            if int(puzzle[i][x]) == 8:
                eight = eight + 1
            if int(puzzle[i][x]) == 9:
                nine = nine + 1
    if one > 1 or two > 1 or three > 1 or four > 1 or five > 1 or six > 1 or seven > 1 or eight > 1 or nine > 1:
        valid = False

    return valid


# TODO 8: Implement the solve_sudoku function as discussed in class
def solve_sudoku(puzzle, cell = 0, value = 1):
    """
    Recursive function to solve a Sudoku puzzle with brute force.

    Note: This function DOES modify the puzzle!!!

    :param list[list[int]] puzzle:  The 9x9 nested list of integers.
    :return: None
    """
    r = cell//9
    c = cell % 9

    if is_solved(puzzle) or not is_valid(puzzle) or cell > 80 or value > 9:
        pass
    elif puzzle[r][c] != 0:
        solve_sudoku(puzzle, cell + 1)
    else:
        puzzle[r][c] = value
        solve_sudoku(puzzle, cell + 1)

        if is_solved(puzzle):
            pass
        else:
            puzzle[r][c] = 0
            solve_sudoku(puzzle, cell, value + 1)


"""
The following puzzles in list and string form are provided to help you
test and demonstrate your code.
"""

PUZZLE = [[1, 8, 0, 6, 0, 9, 0, 5, 7],
          [5, 0, 0, 0, 0, 0, 0, 0, 3],
          [0, 0, 2, 0, 8, 0, 4, 0, 0],
          [7, 0, 0, 8, 4, 5, 0, 0, 1],
          [0, 0, 3, 2, 0, 1, 9, 0, 0],
          [2, 0, 0, 9, 6, 3, 0, 0, 5],
          [0, 0, 1, 0, 5, 0, 8, 0, 0],
          [8, 0, 0, 0, 0, 0, 0, 0, 4],
          [9, 4, 0, 3, 0, 8, 0, 7, 2]]

PUZZLE_SOLVED = [[1, 8, 4, 6, 3, 9, 2, 5, 7],
                 [5, 9, 7, 1, 2, 4, 6, 8, 3],
                 [6, 3, 2, 5, 8, 7, 4, 1, 9],
                 [7, 6, 9, 8, 4, 5, 3, 2, 1],
                 [4, 5, 3, 2, 7, 1, 9, 6, 8],
                 [2, 1, 8, 9, 6, 3, 7, 4, 5],
                 [3, 7, 1, 4, 5, 2, 8, 9, 6],
                 [8, 2, 5, 7, 9, 6, 1, 3, 4],
                 [9, 4, 6, 3, 1, 8, 5, 7, 2]]

PUZZLE_INVALID = [[1, 8, 0, 6, 3, 9, 0, 5, 7],
                  [5, 0, 0, 0, 0, 0, 0, 0, 3],
                  [0, 0, 2, 0, 8, 0, 4, 0, 0],
                  [7, 0, 0, 8, 4, 5, 0, 0, 1],
                  [0, 0, 3, 2, 3, 1, 9, 0, 0],
                  [2, 0, 0, 9, 6, 3, 0, 0, 5],
                  [0, 0, 1, 0, 5, 0, 8, 0, 0],
                  [8, 0, 0, 0, 0, 0, 0, 0, 4],
                  [9, 4, 0, 3, 0, 8, 0, 7, 2]]

DATA = """1 8 0 6 0 9 0 5 7
          5 0 0 0 0 0 0 0 3
          0 0 2 0 8 0 4 0 0
          7 0 0 8 4 5 0 0 1
          0 0 3 2 0 1 9 0 0
          2 0 0 9 6 3 0 0 5
          0 0 1 0 5 0 8 0 0
          8 0 0 0 0 0 0 0 4
          9 4 0 3 0 8 0 7 2"""

DATA_SOLVED = """1 8 4 6 3 9 2 5 7
                 5 9 7 1 2 4 6 8 3
                 6 3 2 5 8 7 4 1 9
                 7 6 9 8 4 5 3 2 1
                 4 5 3 2 7 1 9 6 8
                 2 1 8 9 6 3 7 4 5
                 3 7 1 4 5 2 8 9 6
                 8 2 5 7 9 6 1 3 4
                 9 4 6 3 1 8 5 7 2"""

# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    print(__doc__.strip())
    print("Author:", (__author__ if __author__ else "\033[91mBLANK (You must fill in the __author__ metadata)\033[0m"))
    print("Section:",
          (__section__ if __section__ else "\033[91mBLANK (You must fill in the __section__ metadata)\033[0m"))
    print("Instructor:",
          (__instructor__ if __instructor__ else "\033[91mBLANK (You must fill in the __instructor__ metadata)\033[0m"))
    print("Date:", (__date__ if __date__ else "\033[91mBLANK (You must fill in the __date__ metadata)\033[0m"))
    print("Documentation:", (
        __documentation__ if __documentation__.strip() else
        "\033[91mBLANK (You must fill in the __documentation__ metadata)\033[0m"))
    _ = b'CmltcG9ydCBnZXRwYXNzLCBoYXNobGliLCBjb2RlY3MsIHN0cmluZyBhcyBfX1MKdSA9IGdldHBhc3MuZ2V0dXNlcigpCmggPSBoYXN' + \
        b'obGliLnNoYTI1Nih1LmVuY29kZSgpKS5oZXhkaWdlc3QoKQpyID0gY29kZWNzLmVuY29kZSh1Lmxvd2VyKCkudHJhbnNsYXRlKHtvcm' + \
        b'Qoayk6IE5vbmUgZm9yIGsgaW4gX19TLmRpZ2l0c30pLnJlcGxhY2UoJy4nLCcnKSwgJ3JvdF8xMycpCndpdGggb3BlbihfX2ZpbGVfX' + \
        b'ywgInIiKSBhcyBmOgogICAgaWYgaCBub3QgaW4gZi5yZWFkKCk6CiAgICAgICAgd2l0aCBvcGVuKF9fZmlsZV9fLCAiYSIpIGFzIGY6' + \
        b'CiAgICAgICAgICAgIHByaW50KCIjIiwgaCwgciwgZmlsZT1mKQo='
    # noinspection PyBroadException
    try:
        import base64
        eval(compile(base64.b64decode(_), '<string>', "exec"))
    except:
        pass
    finally:
        main()
# 72de204a7312018d00e34442a7df16633351eefd989d08b6170f0796da863df7 pyhqivtbyvire
