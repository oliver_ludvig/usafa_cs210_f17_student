#!/usr/bin/env python3
"""CS 210, Introduction to Programming, Fall 2017.

This module demonstrates inheritance and the super() function.
See https://docs.python.org/3/library/functions.html#super

Documentation: None.
"""

from Labs.Lab36_Fraction import Fraction

__author__ = "Dr. Bower"


def main():
    """Main program to demonstrate the ProperFraction class."""

    # A Fraction object with numerator larger than denominator is unnatural to read when printed.
    f = Fraction(7, 2)
    print("f = {}".format(f))

    # A ProperFraction object prints in a much nicer format.
    f = ProperFraction(7, 2)
    print("f = {}".format(f))

    # The above example is not surprising since both Fraction and ProperFraction implement __str__.
    # However, the beauty of inheritance is that ProperFraction can do anything a Fraction can do!
    # For example, the __iadd__ method is inherited from Fraction (as are all other Fraction methods.)
    f += ProperFraction(7, 3)
    print("f = {}".format(f))


# A ProperFraction extends or inherits from Fraction.
class ProperFraction(Fraction):
    """Class for representing a proper fraction in the form '3 1/2'."""

    # The only difference between a ProperFraction and a Fraction is how
    # they are converted to strings, so this is the only method that must
    # be implemented; the rest are inherited for free!
    def __str__(self):
        """Build and return a string representation of the object.

        :return: A string representation of this Fraction in the format "w n/d".
        :rtype: str
        """
        # Notice the references to self.n and self.d; a ProperFraction object
        # has the attributes/properties the same way a Fraction object does.
        if self.n < self.d:
            # Use the super() function to call the regular Fraction __str__ method.
            return super().__str__()
        else:
            return "{} {}/{}".format(self.n // self.d, self.n % self.d, self.d)


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
