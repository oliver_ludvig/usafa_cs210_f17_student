#!/usr/bin/env python3
"""
Lab#6 - Proper Formatting of Strings and Inputs
CS 210, Introduction to Programming
"""

import math

__author__ = "Ludvig Oliver"
__instructor__ = "Lt Col Harder"
__date__ = "24 Aug 2017"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
-	Read Lesson 6: Functions from our online textbook
-	Pay particular attention to the Code Lens examples in this reading!
-   Read about string formatting at https://mkaz.tech/code/python-string-format-cookbook/

Lesson Objectives
-	Introduce functions with return values
-	Reinforce functions and parameters
-	Reinforce the range function
-   Learn string formatting of numbers

Note: Unless specifically requested, functions in lab exercises do not require docstrings.
"""


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    # exercise0()
    # exercise1()
    # exercise2()
    exercise3()
    exercise4()
    exercise5()


def exercise0():
    """Example code."""
    print_exercise_name()

    # Call a function that returns a value
    answer = square(3)  # First save the number 9 in answer
    answer = square(answer)  # Then save the number 81 in answer
    print("Answer should be 81.  It is:", answer)

    # Some ways to format strings
    # More at https://mkaz.tech/code/python-string-format-cookbook/
    pi = 3.14159
    print("Two digits: {:.2f}".format(pi))
    print("Two digits, fixed width, right aligned: [{:>6.2f}]".format(pi))
    print("Two digits, fixed width, left aligned : [{:<6.2f}]".format(pi))
    print("Centered number: [{:^20}]".format(pi))
    print("Centered string: [{:^20}]".format("hello world"))


# TODO 0: Read and understand the square and bad_square functions below, discussing with a classmate.
def square(n):
    """Calculate and return the square of the given number.

    :param int n: The number to be squared.
    :return: The square of the given number.
    :rtype: int
    """
    return n ** 2


def bad_square(n):
    """Calculate and print the square of the given number.

    :param int n: The number to be squared.
    """
    print(n ** 2)


# TODO 1: Run exercise1() and observe the results, discussing with a classmate.
def exercise1():
    """
    Demonstrate the very common error of a function printing
    rather than returning a value.

    a.	Carefully read the code in the exercise1 function,
        including the comments.

    b.	Discuss with a classmate and predict the results before
        running the program.

    c.	Run the program with the exercise1 function un-commented
        and observe the results.

    d.	Discuss the results with a classmate and be sure you
        understand the output before moving on.

    Ask questions of other classmates and your instructor as necessary.
    """
    print_exercise_name()

    print("Calling the square function with the actual parameter 7:")
    result = square(7)  # Calculates the square of 7 and puts the value in the variable result.
    print("result =", result)  # Prints the value obtained from the square function.
    print(flush=True)  # Prints a blank line and forces all buffered output to the console.

    print("Calling the bad_square function with the actual parameter 7:")
    result = bad_square(7)  # Calculates the square of 7, but only prints it (does not return it).
    print("result =", result)  # Prints the value obtained from the bad_square function, which is None.
    print(flush=True)  # Prints a blank line and forces all buffered output to the console.


# TODO 2: Run exercise2() and observe the results, discussing with a classmate.
def exercise2():
    """
    Demonstrate the very common error of a function printing rather
    than returning a value.

    a.	Carefully read the code in the exercise2 function, including the comments.

    b.	Discuss with a classmate and predict the results before running the program.

    c.	Run the program with the exercise2 function un-commented and
        observe the results.

    d.	Discuss the results with a classmate and be sure you understand
        the output before moving on.

    Ask questions of other classmates and your instructor as necessary.
    """
    print_exercise_name()

    print("Using the square function to calculate a hypotenuse:")
    result = math.sqrt(square(3) + square(4))  # This works fine, passing 9 + 16 to math.sqrt.
    print("result = [{:10.3f}]".format(result))  # Prints the expected result of 5.
    print(flush=True)

    print("Using the bad_square function to calculate a hypotenuse:")
    result = math.sqrt(bad_square(3) + bad_square(4))  # This passes None + None to math.sqrt ... Error!
    print("result = [{:10.3f}]".format(result))  # Prints the expected result of 5.
    print(flush=True)


# TODO 3a: Run exercise3() and observe the results, discussing with a classmate.
def exercise3():
    """
    Use the get_celsius function to create a table of temperature conversions.

    a.	Run the program with the exercise3 function un-commented and observe the results.

    b.	In the space TODO 3a, complete the get_celsius function so it properly calculates
        the Celsius temperature.

    c.	In the space TODO 3b, improve the formatting so that it matches the results shown.

         F     C
        ===  =====
          0  -17.8
         10  -12.2
         20   -6.7
         30   -1.1
         40    4.4
         50   10.0
         60   15.6
         70   21.1
         80   26.7
         90   32.2
        100   37.8

    """
    print_exercise_name()

    # Print column headers.
    print("  F      C  ")
    print(" ===   =====")
    # Print the temperature conversions.
    for f in range(0, 101, 10):
        # Use the string format() method to make the output pretty.
        # More formatting examples here:
        # https://mkaz.tech/code/python-string-format-cookbook/
        # TODO 3b: Improve the formatting
        print(" {:>3.0f}   {:>5.1f}".format(f, get_celsius(f)))

    print()  # A blank line after the table.


# TODO 3a: Complete the get_celsius function so it calculates and returns the celsius temperature.
def get_celsius(fahrenheit):
    """
    Calculate and return the celsius temperature equivalent to the
    given fahrenheit temperature.

    :param float fahrenheit: The fahrenheit temperature to convert to celsius.
    :return: The celsius temperature equivalent to the given fahrenheit temperature.
    :rtype: float
    """
    return (fahrenheit - 32)/1.8


def exercise4():
    print_exercise_name()
    print("Exercise 4 deleted.")


def exercise5():
    """
    Use the packing_material function to create a table of packing
    material for globe shipping.

    a.	In the space "TODO 5a", write a function named packing_material that
        receives as a parameter the radius of a globe and returns the amount
        of packing material required to safely ship the globe in a box with
        one inch of extra space on the sides.

        i.	In other words, how much of those little Styrofoam peanuts do
            you need to fill up the space around the sphere?

        ii.	Be sure to include a complete and correct docstring comment!

    b.	In the space marked "TODO 5b" complete the exercise5 function so it
        displays a table of required packing material for globes with radii 6, 8, 10, and 12.

        Use a for loop to generate the numbers 6, 8, 10, 12.

    c.	Use appropriate string formatting to properly align the numbers as shown.

        Globe  Packing
        =====  =======
          6    1839.22
          8    3687.34
         10    6459.21
         12   10337.77

    """
    print_exercise_name()

    print("Globe  Packing")
    print("=====  =======")

    for r in range(6,13,2):
        print(" {:>2.0f}   {:>8.2f}".format(r, packing_material(r)))


    # TODO 5b: Use the packing_material function to display a table of packing material requirements.
    # Use the string format() method to make the output pretty.
    # More formatting examples here:
    # https://mkaz.tech/code/python-string-format-cookbook/


# TODO 5a: In the space below, write the packing_material function.

def packing_material(radius):
    pi = 3.1415
    Vsquare = ((radius*2)+2)**3   # Finds the Volume of the box with a one inch margin
    Vcircle = (4/3)*pi*(radius**3)   # Finds the Volume of the Circle
    material = (Vsquare-Vcircle)   # Finds the excess space in the box
    return material   # Returns Calculations



# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def print_exercise_name():
    """Clever code to print the name of the current function."""
    import sys
    print('\n\033[94m', sys._getframe(1).f_code.co_name, '\033[0m\n', flush=True)


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
