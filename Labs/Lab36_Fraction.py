#!/usr/bin/env python3
"""CS 210, Introduction to Programming, Fall 2017.

This module demonstrates the @property decorator with a Fraction class.
See https://docs.python.org/3/library/functions.html#property

Documentation: None.
"""

__author__ = "Dr. Bower"


def main():
    """Main program to demonstrate the Fraction class."""
    a = Fraction(1, 4)
    b = Fraction(1, 3)
    c = a.add(b)
    print("{} + {} = {}".format(a, b, c))

    # This automatically calls the __add__ method.
    d = a + b
    print("{} + {} = {}".format(a, b, d))

    # This automatically calls the __iadd__ method.
    a += b
    print(a)


class Fraction:
    """Class for representing a fraction with integer values for numerator and denominator."""

    def __init__(self, n=0, d=1):
        """Create a new Fraction with the given values.

        :param int n: the numerator.
        :param int d: the denominator.
        """
        # Assign temporary default values to the private attributes so PyCharm
        # will not warn us about creating attributes outside the constructor.
        self.__n = 0
        self.__d = 1

        # Now assign the given parameters using the property setters,
        # which causes any error checking code to be executed.
        self.n = n
        self.d = d

    @property
    def n(self):
        """Returns the current value of the numerator.

        :return: the numerator.
        :rtype: int
        """
        return self.__n

    @n.setter
    def n(self, n):
        """Sets the numerator to the given value.

        :param int n: the numerator.
        :return: None
        """
        # Ensure the value stored is an integer.
        self.__n = int(n)

    @property
    def d(self):
        """Returns the current value of the denominator.

        :return: the denominator.
        :rtype: int
        """
        return self.__d

    @d.setter
    def d(self, d):
        """Sets the denominator to the given value.

        :param int d: the denominator.
        :return: None
        """
        # Ensure the value stored is a non-zero integer.
        d = int(d)
        if d == 0:
            raise ZeroDivisionError("Fraction denominator cannot be zero.")

        if d > 0:
            self.__d = d
        else:
            self.__n *= -1  # To ensure the negative sign is on the numerator,
            self.__d = -d  # invert both the numerator and denominator.

    # NOTE: Throughout the rest of the class, the property values are used
    #       rather than directly accessing the private attributes self.__n
    #       and self.__d to ensure any assignment to these attributes is
    #       fully error checked.

    # The __str__ method, if present in a class, is automatically called
    # when a string representation if an instance of the class is needed.
    def __str__(self):
        """Build and return a string representation of the object.

        :return: A string representation of this Fraction in the format "n/d".
        :rtype: str
        """
        return "{}/{}".format(self.n, self.d)

    def add(self, other):
        """Adds two Fraction objects, building and returning a new Fraction object.

        Note: This method does NOT modify the self or other Fraction objects.

        Note: This method ensures the result Fraction object is in lowest terms.

        :param Fraction other: the other Fraction object to be added to this Fraction object.
        :return: a new Fraction object equal to the sum of the self and other Fraction objects.
        :rtype: Fraction
        """
        n1 = self.n * other.d
        n2 = self.d * other.n
        d = self.d * other.d
        result = Fraction(n1 + n2, d)
        result.simplify()
        return result

    # The __add__ method, if present in a class, is automatically called
    # when two instances are added with the + operator.
    def __add__(self, other):
        """Adds two Fraction objects, building and returning a new Fraction object.

        Note: This method does NOT modify the self or other Fraction objects.

        Note: This method ensures the result Fraction object is in lowest terms.

        :param Fraction other: the other Fraction object to be added to this Fraction object.
        :return: a new Fraction object equal to the sum of the self and other Fraction objects.
        :rtype: Fraction
        """
        n1 = self.n * other.d
        n2 = self.d * other.n
        d = self.d * other.d
        result = Fraction(n1 + n2, d)
        result.simplify()
        return result

    # The __iadd__ method, if present in a class, is automatically called
    # when two instances are added with the += operator (in-place add).
    def __iadd__(self, other):
        """Adds two Fraction objects, modifying the self Fraction object.

        Note: This method DOES modify the self Fraction object,
              but does NOT modify the other Fraction object.

        Note: This method ensures the Fraction object is in lowest terms.

        :param Fraction other: the other Fraction object to be added to this Fraction object.
        :return: a new Fraction object equal to the sum of the self and other Fraction objects.
        :rtype: Fraction
        """
        self.n *= other.d
        self.n += self.d * other.n
        self.d *= other.d
        self.simplify()
        return self

    # Note: Many other operations may be defined for a class.
    #       See https://docs.python.org/3/library/operator.html

    def simplify(self):
        """Simplifies this Fraction object such that it is in lowest terms."""
        # Find the greatest common divisor of this Fraction object's numerator and denominator.
        divisor = gcd(self.n, self.d)
        # Simplify this Fraction object's numerator and denominator.
        self.n //= divisor
        self.d //= divisor


def gcd(a, b):
    """Uses division-based version of Euclid's algorithm to calculate
    the greatest common divisor of two integers.

    https://en.wikipedia.org/wiki/Euclidean_algorithm#Implementations

    :param int a: An integer.
    :param int b: An integer.
    :return: The greatest common divisor of a and b.
    :rtype: int
    """
    while b != 0:
        temp = b
        b = a % b
        a = temp
    return a


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
