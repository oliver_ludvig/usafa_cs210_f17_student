# !/usr/bin/env python3

# String Manipulation
# File I/O
# Lists
# Nested Lists
# Recursion
# Dictionaries

import json
import string


def load_word_groups(filename):  # filename: '../Data/address.txt'
    with open(filename, "r") as f:
        words = f.read().split()

    d = {}  # type: dict[str,list[str]]
    for word in words:
        if len(word) > 0:
            first_letter = word[0].lower()
            if first_letter in string.ascii_letters():
                if first_letter not in d:
                    d[first_letter] = []

                # Strip out punctuations
                word = word.lower()
                new_word = ""
                for c in word:
                    if c in string.ascii_letters:
                        new_word = new_word + c
                word = new_word

                d[first_letter].append(word)
    return d


d = load_word_groups("../Data/Address.txt")
print(json.dumps(d, indent=2))
print("Number of first letters:", len(d))


def summarize_data(filename, data):
    with open(filename,"w") as f:
        # print(d, file=f)
        for letter in string.ascii_lowercase:
            print(letter.upper(), file=f)

            if letter in data:
                for word in data[letter]:
                    print("\t", word, file=f)



summarize_data("../Data/Address_summary.txt", d)
