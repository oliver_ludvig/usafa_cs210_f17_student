#!/usr/bin/env python3
"""
PUT YOUR DESCRIPTION OF THIS FILE HERE
CS 210, Introduction to Programming
"""

__author__ = "Ludvig Oliver"
__instructor__ = "LtCol Harder"
__date__ = "21 Sep 2017"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
-	Read Lesson 15: Strings, Lists, and Files from our online textbook
Lesson Objectives
-	Reinforce functions, parameters, return values, and selection statements
-	Reinforce definite iteration with the for loop and indefinite iteration with the while loop
-	Introduce Python strings and lists
-	Demonstrate Python's basic file reading construct


"""

import easygui


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    # exercise0()
    # exercise1()
    # exercise2()
    # exercise3()
    exercise4()


def exercise0():
    """
    Data Files – Manipulating data files can be a complicated task; however, Python
    makes it very simple to read the entire contents of a data file into a single variable.

    Read through the following code, and run it to see various ways to read a file.
    """
    print_exercise_name()

    # In the folder "next to" the Labs folder is a Data folder with several text files
    filename = "../Data/Test.txt"

    # Example 1: Read the entire contents of a file into a single string.
    # The result of the following few lines of code is the variable
    # data_string containing a string with the entire contents of the file Test.txt
    # The "with open..." construct AUTOMATICALLY CLOSES the file afterward, so you
    # do not need to call .close() yourself.  The "with open..." is SAFER CODE.
    with open(filename) as data_file:
        data_string = data_file.read()
    # Un-indent after reading the file so the "with" construct will close the file.
    print("Example 1: Dump the file contents", data_string, sep="\n")

    # Example 2: Read the entire contents of a file into a list of strings, one per word.
    # Further, a slight modification to the above results in the contents of the
    # data file being split into a list of strings, one per word:
    with open(filename) as data_file:
        data_words = data_file.read().split()
    print("Example 2: Break up the contents with .split()", data_words, sep="\n")
    print()

    # Example 3: Read the entire contents of a file into a list of strings, one per line.
    # Further still, the contents of the data file can be split into a list
    # of strings, one per line:
    with open(filename) as data_file:
        data_lines = data_file.read().splitlines()
    print("Example 3: Break up the contents with .splitlines()", data_lines, sep="\n")
    print()

    # Example 4: Uses file open dialog to select a txt file from the data folder.
    filename = easygui.fileopenbox(default="../Data/*.txt")
    with open(filename) as data_file:
        data_string = data_file.read()
    #
    # # The data_string can be split into words and lines _without_re-reading_ the file:
    data_words = data_string.split()
    data_lines = data_string.splitlines()
    print("Example 4:", data_string, data_words, data_lines, sep="\n")


def exercise1():
    """
    Reads an entire file into a string, then estimates the number of words per sentence.

    Sentence Length – The average words per sentence is a crude measure of readability.
    For example, sentences of 12 words or less might be considered easy to read while
    sentences of 24 words or more might be difficult to read. In this exercise, you will
    estimate the number of words per sentence in several text files.

    a.	In the space "TODO 1a", write a function named count_char that counts the number
        of times an individual character occurs in a longer string. The function accepts
        two strings as parameters (the individual character and the longer string) and
        returns an integer (the count of occurrences of the character within the string).

    b.  In the space "TODO 1b", add three more tests to those provided that show
        that your function works as expected.

    c.	In the space "TODO 1c", write code that reads a file into a string, uses the
        count_char function to estimate the number of sentences in the string (i.e.,
        count periods, exclamation points, and question marks) and then calculates
        the average number of words per sentence in the string.  Display the result
        in an easygui message box as shown.

        Hint: the number of words in a string can be estimated with

            words = len( data_string.split() )

        Hint: Start with the ../Data/Test.txt file, which is easy to verify, and then
        move on to a larger file.

            +-----------------------------------------+
            |  Test.txt                   [_] [ ] [X] |
            +-----------------------------------------+
            |  21/3 = 7.00 words per sentence         |
            |             +------+                    |
            |             |  OK  |                    |
            |             +------+                    |
            +-----------------------------------------+
    """
    print_exercise_name()

    # Test your function
    if count_char("a", "alphabet") != 2:
        print("Incorrect: a, alphabet")
    if count_char(".", "Hello. Good-bye.") != 2:
        print("Incorrect: ., Hello. Good-bye.")
    # TODO 1b: Add three more examples to the two above that test your function
    if count_char("a", "Panthers") != 1:
        print("Incorrect: a, Panthers")
    if count_char("T", "Too Fine Too Nine") != 2:
        print("Incorrect: T, Too Fine Too Nine")
    if count_char("L", "Libero") != 1:
        print("Incorrect: L, Libero")

    # TODO 1c: Write code to use the count_char function
    filename = "../Data/Test.txt"
    with open(filename) as data_text:
        data_list = data_text.read().split()

    iteration = 0
    length = len(data_list)
    lines = 0
    while iteration != length:
        periods = count_char(".", data_list[iteration])
        questions = count_char("?", data_list[iteration])
        exclamation = count_char("!", data_list[iteration])
        if periods == 1 or questions == 1 or exclamation == 1:
            lines = lines + 1
        iteration = iteration + 1

    average = len(data_list)/lines
    easygui.msgbox("{}/{} = {:.2f} words per sentences".format(len(data_list),lines,average), "Average Word Count")



# TODO 1a: In the space below, write the count_char function as described in the lab document.
def count_char(test, lists):
    check = 0
    count = 0
    while check != len(lists):
        if test == lists[check]:
            count = count + 1
        check = check + 1

    return count


def exercise2():
    """
    Reads a data file of Python keywords, then counts how many appear in a Python source file.

    Keywords – In this exercise you will count the number of Python keywords in a
    Python source file.

    a.	In the space "TODO 2a", write a function named count_words that counts the number
        of times words from one list appear in a second list. The function accepts two lists
        of strings as parameters; the first is the list of words to be counted and the second
        is the list of words in which words from the first list are to be counted. The function
        returns an integer which is the number of times words from the first list appear in
        the second list.

    b.  In the space "TODO 2b", add three more tests to those provided that show
        that your function works as expected.

    c.	In the space "TODO 2c", write code that reads the file Keywords.txt into a list
        of words, reads a Python source file into a list of words, and then uses the
        count_words function to determine the number of Python keywords in the source file.
        Use an easygui.fileopenbox to select a python file to inspect for keywords, and
        display the results in an easygui message box.
    """
    print_exercise_name()

    # Test your function
    if count_words(["a", "b"], ["a", "b"]) != 2:
        print("Incorrect: a, b")
    if count_words(["cat", "dog"], ["zebra", "cat", "dog"]) != 2:
        print("Incorrect: zebra, cat, dog")
    # TODO 2b: Add three more examples to the two above that test your function

    # TODO 2c: Write code to use the count_words function

    filename = "../Data/Keywords.txt"
    with open(filename) as data_text:
        data_list = data_text.read().split()

    file = easygui.fileopenbox(default="../Data/*.txt")
    with open(file) as openfile:
        test_text = openfile.read().split()

    keywords = count_words(data_list, test_text)
    easygui.msgbox("There are {} Python Keywords found".format(keywords))



# TODO 2a: In the space below, write the count_words function as described in the lab document.
def count_words(test_list, list):
    count = 0
    for i in range(len(test_list)):
        for x in range(len(list)):
            if test_list[i] == list[x]:
                count = count + 1

    return count

def exercise3():
    """
    Display file information until the user clicks Cancel.

    Keywords – In this exercise you will count the number of Python keywords in a
    Python source file.

    a.	In the space "TODO 3a", write a function named file_info that receives a
        file name (a string) as a parameter and returns a string with information
        about the file such as (for the Test.txt file in your data folder):

	        Lines: 4, Words: 21, Characters: 104

        Note: the file should only be opened and read once; see the latter part of
        the code in Exercise 0, Example 4.

    b.	In the space "TODO 3b", write code that repeatedly obtains a file name
        using the easygui.fileopenbox and then displays an easygui.msgbox with
        the file information received from the file_info function. This should
        continue until the user clicks Cancel on the fileopenbox.
    """
    print_exercise_name()

    # TODO 3b: Write code to use the file_info function
    window = True

    while window == True:
        file = easygui.fileopenbox(default="../Data/*.txt")

        if file == None:
            window = False
        else:
            with open(file) as openfile:
                document = openfile.read().split()
                words = len(document)
            with open(file) as openfile:
                lines = len(openfile.read().splitlines())
                characters = file_info(document)

            easygui.msgbox("Lines: {}, Words: {}, Characters: {}".format(lines, words, characters))



# TODO 3a: In the space below, write the file_info function as described in the lab document.
def file_info(file_name):
    characters = 0
    for i in range(len(file_name)):
        for x in range(len(file_name[i])):
            characters = characters + x

    return characters

def exercise4():
    """
    Display files with line numbers until the user clicks Cancel.

    a.	In the space "TODO 4a", write a function named print_file that receives
        a file name (a string) as a parameter and prints the file to the console
        window with line numbers. This function does not return a value. The Test.txt
        file in your data folder would be displayed as follows:

           1: This is a test.
           2: This is only a test.
           3: Had this been an actual emergency,
           4: You would have heard BIG VOICE.

    Note: The line numbers are right-aligned in a three-character wide column.

    b.	In the space "TODO 4b", write code that repeatedly obtains a file name using
        the easygui.fileopenbox and then prints the file with line numbers by calling
        the print_file function. This should continue until the user clicks Cancel on
        the fileopenbox.
    """
    print_exercise_name()

    # TODO 4b: Write code to use the print_file function as described in the lab document.
    window = True

    while window == True:
        file = easygui.fileopenbox(default="../Data/*.txt")

        if file == None:
            window = False
        else:
            with open(file) as openfile:
                lines = openfile.read().splitlines()
            print_file(lines)


# TODO 4a: In the space below, write the print_file function as described in the lab document.
def print_file(document):
    for i in range(1,len(document) + 1, 1):
        print("{:>3}: {:>3}".format(i, document[i - 1]))

    print("")



"""
Challenge Exercises:

1.	Complete unfinished exercises from any previous lab.
"""


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
