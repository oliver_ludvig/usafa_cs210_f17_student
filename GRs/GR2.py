#!/usr/bin/env python3
"""
GR2, CS210, Fall 2017
"""
import os
import random
import string
import easygui

# Fill in the metadata
__author__ = "C3C Ludvig Oliver"  # Your name. Ex: John Doe
__section__ = "M3/M4"  # Your section. Ex: M1
__version__ = "A"  # Test version. Ex: A
__date__ = "26 Oct 2017"  # Today's date. Ex: 25 Dec 2017

""" The following code segments are provided for your reference.
    You are free to copy/paste them into your answers below.

    # Use an easygui.fileopenbox to get a filename from the user.
    filename = easygui.fileopenbox(default="../Data/*.txt")

    # Read the entire contents of a file into a single string.
    with open(filename, "r") as data_file:
        data_string = data_file.read()

    # Read the entire contents of a file into a list of strings, one per word.
    with open(filename, "r") as data_file:
        data_words = data_file.read().split()

    # Read the entire contents of a file into a list of strings, one per line.
    with open(filename, "r") as data_file:
        data_lines = data_file.read().splitlines()

    # Split a filename into its base and extension; build a new filename.
    base, ext = os.path.splitext(filename)
    output_filename = "{}_Output{}".format(base, ext)

    # Write a string to a file.
    with open(output_filename, "w") as output_file:
        output_file.write( "This is the output.\n" )
"""


# ======================================================================
# Main
# ======================================================================
def main():
    """ Comment and uncomment these lines as needed to work on each problem. """
    # problem1()
    # problem2()
    # problem3()
    # problem4()
    problem5()
    # problem6()


# ======================================================================
# Problem 1                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem1():
    """Problem 1 from the GR."""
    print_problem_name()

    # TODO 1b
    attempt = 1

    # Ticket
    ticket = generate_lotto(24)

    # Winner
    gambler = generate_lotto(24)
    while gambler != ticket:
        gambler = generate_lotto(24)
        attempt = attempt + 1

    print("Ticket = " + str(ticket))
    print("Winner = " + str(gambler))
    print("It took you " + str(attempt) + " attempts")


# TODO 1a
def generate_lotto(upper_bound):
    """

    :param upper_bound: int
    :return: list[int]
    """

    lotto = []

    for i in range(6):
        lucky_number = random.randint(1, upper_bound)

        if lucky_number in lotto:
            lucky_number = random.randint(1, upper_bound)

        lotto.append(lucky_number)

    sort_list = sorted(lotto)

    return sort_list


# ======================================================================
# Problem 2                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem2():
    """Problem 2 from the GR."""
    print_problem_name()

    board1 = [list("aa.bocc"),
              list("aa.bbcc"),
              list("qq.abb."),
              list("qqcaabb"),
              list("occaabb"),
              list("ccaacoo")]

    board2 = [list("q.c.oo.qb."),
              list("baa.boccbb"),
              list("baa.bbccba"),
              list("aqq.abb.ab"),
              list("aqqcaabbcq"),
              list("c.ccaabbcc"),
              list("cccaacooqc")]

    # TODO 2b
    print("Board 1, a:", count_pattern(board1, "a"))
    print("Board 1, b:", count_pattern(board1, "b"))
    print("Board 1, c:", count_pattern(board1, "c"))

    print("Board 2, a:", count_pattern(board2, "a"))
    print("Board 2, b:", count_pattern(board2, "b"))
    print("Board 2, c:", count_pattern(board2, "c"))


# TODO 2a
def count_pattern(board, marker):
    """

    :param board: str
    :param marker: str
    :return: integer
    """
    counter = 0

    for i in range(len(board) - 1):
        for x in range(len(board[i]) - 1):
            if board[i][x] == marker:
                if board[i + 1][x] == marker and board[i + 1][x + 1] == marker:
                    counter = counter + 1

    return (counter)


# ======================================================================
# Problem 3                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem3():
    """Problem 3 from the GR."""
    print_problem_name()

    # TODO 3b
    with open(easygui.fileopenbox(default="..Data/*.txt"), "w") as open_file:
        print(open_file)


# TODO 3a
def convert_to_madlib(filename, newfile, list):
    with open(filename) as open_file:
        read_file = open_file.read()


# ======================================================================
# Problem 4                                                 ____ / 5 pts
# Grades: All or nothing
# ======================================================================
def problem4():
    """Problem 4 from the GR."""
    print_problem_name()

    # TODO 4
    answer = "G E A B F C D"  # Put your answer here, eg, "L M N O P"
    print("Sequence:", answer)


# ======================================================================
# Problem 5                                                ____ / 20 pts
# Grades: A: 20, B: 16, C: 14, D: 12, F: 10
# ======================================================================
def problem5():
    """Problem 5 from the GR."""
    print_problem_name()

    # TODO 5b

    # if " " == recursive_extract_vowels(" "):
    #     print("pass")

    if "ooe" == recursive_extract_vowels("booklet") is True:
        print("pass")
    else:
        print("fail")


# TODO 5a
def recursive_extract_vowels(word):
    if len(word) <= 1:
        return
    else:
        if word[0] == "a" or word[0] == "e" or word[0] == "i" or word[0] == "o" or word[0] == "u":
            return " " + word[0] and recursive_extract_vowels(word[1:])


# ======================================================================
# Problem 6                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem6():
    """Problem 6 from the GR."""
    print_problem_name()

    # TODO 6c
    updates = load_updates("../Data/update_inventory.txt")
    inventory = {"apples": 1000, "grapes": 700}
    print("Before:", inventory)
    update_inventory(inventory, updates)
    print("After", inventory)


# TODO 6b
def update_inventory(dictionary, list):
    for items in list:
        if items[0] in dictionary:
            dictionary[items[0]] = dictionary[items[0]] + items[1]
        else:
            dictionary[items[0]] = items[1]


# TODO 6a
def load_updates(filename):
    file = []

    with open(filename) as open_file:
        read_file = open_file.read().splitlines()
        for lists in read_file:
            split_list = lists.split()
            items = (split_list[0], int(split_list[1]))
            file.append(items)

    return file


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def print_problem_name():
    """Print the name and docstring of the calling function (i.e., the current problem.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        doc_color = "\033[91m" if doc is None else "\033[92m"
        print('\n\033[94m{}\n{}\n{}{}\033[99m'.format(name, "=" * len(name), doc_color, doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


if __name__ == "__main__":
    print(__doc__.strip())
    print("Author:",
          (__author__ if __author__ else "\033[91mBLANK (You must fill in the __author__ metadata)\033[0m"))
    print("Section:",
          (__section__ if __section__ else "\033[91mBLANK (You must fill in the __section__ metadata)\033[0m"))
    print("Test Version:",
          (__version__ if __version__ else "\033[91mBLANK (You must fill in the __version__ metadata)\033[0m"))
    print("Date:", (__date__ if __date__ else "\033[91mBLANK (You must fill in the __date__ metadata)\033[0m"))
    _ = b'CmltcG9ydCBnZXRwYXNzLCBoYXNobGliLCBjb2RlY3MsIHN0cmluZyBhcyBfX1MKdSA9IGdldHBhc3MuZ2V0dXNlcigpCmggPSBoYXN' + \
        b'obGliLnNoYTI1Nih1LmVuY29kZSgpKS5oZXhkaWdlc3QoKQpyID0gY29kZWNzLmVuY29kZSh1Lmxvd2VyKCkudHJhbnNsYXRlKHtvcm' + \
        b'Qoayk6IE5vbmUgZm9yIGsgaW4gX19TLmRpZ2l0c30pLnJlcGxhY2UoJy4nLCcnKSwgJ3JvdF8xMycpCndpdGggb3BlbihfX2ZpbGVfX' + \
        b'ywgInIiKSBhcyBmOgogICAgaWYgaCBub3QgaW4gZi5yZWFkKCk6CiAgICAgICAgd2l0aCBvcGVuKF9fZmlsZV9fLCAiYSIpIGFzIGY6' + \
        b'CiAgICAgICAgICAgIHByaW50KCIjIiwgaCwgciwgZmlsZT1mKQo='
    # noinspection PyBroadException
    try:
        import base64

        eval(compile(base64.b64decode(_), '<string>', "exec"))
    except:
        pass
    finally:
        main()

# 72de204a7312018d00e34442a7df16633351eefd989d08b6170f0796da863df7 pyhqivtbyvire
