#!/usr/bin/env python3
"""
GR1, CS210, Fall 2017
"""
import math
import random
import easygui
import turtle

# Fill in the metadata
__author__ = "C3C Ludvig Oliver"  # Your name. Ex: John Doe
__section__ = "M3/M4"  # Your section. Ex: M1
__version__ = "A"  # Test version. Ex: A
__date__ = "19 Sep 2017 "  # Today's date. Ex: 25 Dec 2017

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960  # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 // 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = WIDTH // 30  # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = MARGIN // 2  # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = True  # Set to True for fast, non-animated turtle movement.

""" The following code segments are provided for your reference.
    You are free to copy/paste them into your answers below.

    # Use the writing turtle to write a message, centered at its location, in a bold font.
    writer.write( "Hello, World!", align="center", font=( "Courier", FONT_SIZE, "bold" ) )

    # An easygui message box to display a formatted string.
    easygui.msgbox( "pi = {:.2f}".format( 22 / 7 ), "Result" )

    # An easygui enter box for entering a string.
    s = easygui.enterbox( "Enter a string:", "Input" )

    # An easygui integer box for entering a positive integer.
    n = easygui.integerbox( "Enter a positive integer:", "Input", 42, 1, 2 ** 31 )

    # Read the entire contents of a file into a single string.
    with open( easygui.fileopenbox( default="../Data/*.txt" ) ) as data_file:
        data_string = data_file.read()

    # Read the entire contents of a file into a list of strings, one per word.
    with open( easygui.fileopenbox( default="../Data/*.txt" ) ) as data_file:
        data_words = data_file.read().split()

    # Read the entire contents of a file into a list of strings, one per line.
    with open( easygui.fileopenbox( default="../Data/*.txt" ) ) as data_file:
        data_lines = data_file.read().splitlines()
"""


# ======================================================================
# Main
# ======================================================================
def main():
    """ Comment and uncomment these lines as needed to work on each problem. """
    # problem1()
    # problem2()
    # problem3()
    # problem4()
    problem5()


# ======================================================================
# Problem 1                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem1():
    """Problem 1 from the GR."""
    print_problem_name()

    # TODO 1b
    print("Hands" + "\t" + "Feet")
    print("=====" + "\t" + "=====")
    for hands in range(10, 20+1):
        foot = hands_to_feet(hands)
        print(str(hands) + "\t" + "\t{:.2f}".format(foot))


# TODO 1a
def hands_to_feet(hands):
    feet = hands/12 * 4

    return feet

# ======================================================================
# Problem 2                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem2():
    """Problem 2 from the GR."""
    print_problem_name()

    # TODO 2b
    desired_rolls = easygui.integerbox("Input Desired Amount of Rolls (1 - 1000)", "Roll Box", 29, 1, 1000)
    target_number = easygui.integerbox("Input Target Value (2 - 12)", "Target Box", 7, 2, 12)
    results = count_dice(desired_rolls,target_number)

    easygui.msgbox("In {} rolls, your target value of {} was hit {} times.".format(desired_rolls, target_number, results)
                   , "Game Over Box")

# TODO 2a
def count_dice(rolls, target):
    turns = 0
    counter = 0
    while turns != rolls:
        dice_1 = random.randint(1, 6)
        dice_2 = random.randint(1, 6)
        total = dice_1 + dice_2

        if total == target:
            counter = counter + 1

        turns = turns + 1

    return counter

# ======================================================================
# Problem 3                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# Including 5 pts for good docstring
# ======================================================================
def problem3():
    """Problem 3 from the GR."""
    print_problem_name()

    # TODO 3b
    value_one = easygui.integerbox("Input First Value", "One", 42, 1, 99999)
    value_two = easygui.integerbox("Input Second Value", "Two", 28, 1, 99999)
    value_three = easygui.integerbox("Input Third Value", "Three", 63, 1, 99999)
    value_four = easygui.integerbox("Input Fourth Value", "Four", 84, 1, 99999)

    divisor_one = gcd(value_one, value_two)
    divisor_two = gcd(value_three, value_four)

    overall_divisor = gcd(divisor_one, divisor_two)

    easygui.msgbox("The Greatest Common Divisor for values {}, {}, {}, and {} is {}"
                   .format(value_one, value_two, value_three, value_four, overall_divisor), "Math is Hard")

# TODO 3a
def gcd(a, b):
    """
    This function calculates the greatest common divisor between two values through the use of Euclid's original
    algorithm that compares the two values and subtracts each value accordingly

    :param a: The first value given to the function
    :param b: The second value given to the function
    :return: The common divisor between the two values - "denominator"
    """

    while a != b:
        if a > b:  # If the first value is larger than the second value then we subtract a - b to be the first value
            a = a - b
        elif b > a:  # If the second value is larger than the first value then we subtract b - a to be the second value
            b = b - a

    denominator = a

    return denominator
# ======================================================================
# Problem 4                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem4():
    """Problem 4 from the GR."""
    print_problem_name()

    with open(easygui.fileopenbox(default="../Data/*.txt")) as data_file:
        data_words = data_file.read().split()
        # Reading in a file followed by split() creates a list of words
        # such as you have used in labs. For example:
        # data_words = ["It", "was", "the", "worst", "of", "times"]

    # TODO 4b
    for_he = count_word("he", data_words)
    for_him = count_word("him", data_words)
    for_she = count_word("she", data_words)
    for_her = count_word("her", data_words)

    male = for_he + for_him
    female = for_she + for_her

    if male > female:
        easygui.msgbox("There are more male than female pronouns.", "Read")
    elif female > male:
        easygui.msgbox("There are more female than male pronouns.", "Read")
    elif male == female:
        easygui.msgbox("There are equal female and male pronouns.", "Read")

# TODO 4a
def count_word(word, lists):
    count = 0
    for i in range(len(lists)):
        if word == lists[i]:
            count = count + 1

    return count


# ======================================================================
# Problem 5                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem5():
    """Problem 5 from the GR."""
    print_problem_name()

    # Create the turtle screen and two turtles
    screen, artist, writer = turtle_setup()

    # TODO 5
    radius_big = HEIGHT/2 - MARGIN
    radius_small = radius_big/2

    artist.up()
    artist.goto(0, -HEIGHT/2 + MARGIN)
    artist.down()
    artist.circle(radius_big)

    artist.up()
    artist.goto(0, -4*MARGIN + 10)
    artist.down()
    artist.circle(radius_small)

    shots = 0
    miss = 0
    score = 0

    artist.shape("circle")
    artist.pensize(MARGIN)
    while shots != 10 or miss == 7:
        x_pos = random.randint(-WIDTH/2 + MARGIN/2 , WIDTH/2 - MARGIN/2)
        y_pos = random.randint(-HEIGHT/2 + MARGIN/2 , HEIGHT/2 - MARGIN/2)

        dot_color = dot(x_pos, y_pos)
        if dot_color == "red":
            miss = miss + 1
        elif dot_color == "yellow":
            score = score + 1
        elif dot_color == "green":
            score = score + 2

        artist.showturtle()
        artist.up()
        artist.goto(x_pos, y_pos)
        artist.down()
        artist.color(dot_color)
        artist.stamp()

        shots = shots + 1

    writer.up()
    writer.goto(0, 0)
    writer.down()
    writer.write("Shots Taken: {}".format(shots) + "\n" + "Final Score = {}".format(score),align="center",
                 font=("Courier", FONT_SIZE, "bold"))


    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


def dot(x,y):

    radius_big = HEIGHT/2 - MARGIN
    radius_small = radius_big/2

    if -radius_small < x < radius_small and -radius_small < y < radius_small:
        color = "green"
    elif -radius_big < x < radius_big and -radius_big < y < radius_big:
        color = "yellow"
    else:
        color = "red"

    return color





# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def turtle_setup():
    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    #  ___   ___     _  _  ___ _____    __  __  ___  ___ ___ _____   __
    # |   \ / _ \   | \| |/ _ \_   _|  |  \/  |/ _ \|   \_ _| __\ \ / /
    # | |) | (_) |  | .` | (_) || |    | |\/| | (_) | |) | || _| \ V /
    # |___/ \___/   |_|\_|\___/ |_|    |_|  |_|\___/|___/___|_|   |_|
    #  _____ _  _ ___ ___    ___ _   _ _  _  ___ _____ ___ ___  _  _
    # |_   _| || |_ _/ __|  | __| | | | \| |/ __|_   _|_ _/ _ \| \| |
    #   | | | __ || |\__ \  | _|| |_| | .` | (__  | |  | | (_) | .` |
    #   |_| |_||_|___|___/  |_|  \___/|_|\_|\___| |_| |___\___/|_|\_|
    #
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup(WIDTH, HEIGHT, MARGIN, MARGIN)
    screen.bgcolor("SkyBlue")

    # Create two turtles, one for drawing and one for writing.
    turtle.TurtleScreen._RUNNING = True  # Get around bug in v3.5.2 http://bugs.python.org/issue26571
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape("turtle")

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay(0)
        artist.hideturtle()
        artist.speed("fastest")
        writer.hideturtle()
        writer.speed("fastest")

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading(90)  # Straight up, which makes it look sort of like a cursor.
    writer.penup()  # A turtle's pen does not have to be down to write text.
    writer.setposition(0, HEIGHT // 2 - FONT_SIZE * 2)  # Centered at top of the screen.

    return screen, artist, writer


def print_problem_name():
    """Print the name and docstring of the calling function (i.e., the current problem.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        doc_color = "\033[91m" if doc is None else "\033[92m"
        print('\n\033[94m{}\n{}\n{}{}\033[99m'.format(name, "=" * len(name), doc_color, doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


if __name__ == "__main__":
    print(__doc__.strip())
    print("Author:",
          (__author__ if __author__ else "\033[91mBLANK (You must fill in the __author__ metadata)\033[0m"))
    print("Section:",
          (__section__ if __section__ else "\033[91mBLANK (You must fill in the __section__ metadata)\033[0m"))
    print("Test Version:",
          (__version__ if __version__ else "\033[91mBLANK (You must fill in the __version__ metadata)\033[0m"))
    print("Date:", (__date__ if __date__ else "\033[91mBLANK (You must fill in the __date__ metadata)\033[0m"))
    _ = b'CmltcG9ydCBnZXRwYXNzLCBoYXNobGliLCBjb2RlY3MsIHN0cmluZyBhcyBfX1MKdSA9IGdldHBhc3MuZ2V0dXNlcigpCmggPSBoYXN' + \
        b'obGliLnNoYTI1Nih1LmVuY29kZSgpKS5oZXhkaWdlc3QoKQpyID0gY29kZWNzLmVuY29kZSh1Lmxvd2VyKCkudHJhbnNsYXRlKHtvcm' + \
        b'Qoayk6IE5vbmUgZm9yIGsgaW4gX19TLmRpZ2l0c30pLnJlcGxhY2UoJy4nLCcnKSwgJ3JvdF8xMycpCndpdGggb3BlbihfX2ZpbGVfX' + \
        b'ywgInIiKSBhcyBmOgogICAgaWYgaCBub3QgaW4gZi5yZWFkKCk6CiAgICAgICAgd2l0aCBvcGVuKF9fZmlsZV9fLCAiYSIpIGFzIGY6' + \
        b'CiAgICAgICAgICAgIHByaW50KCIjIiwgaCwgciwgZmlsZT1mKQo='
    # noinspection PyBroadException
    try:
        import base64

        eval(compile(base64.b64decode(_), '<string>', "exec"))
    except:
        pass
    finally:
        main()

# 72de204a7312018d00e34442a7df16633351eefd989d08b6170f0796da863df7 pyhqivtbyvire
