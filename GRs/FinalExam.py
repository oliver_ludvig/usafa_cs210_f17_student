#!/usr/bin/env python3
"""
Final Exam, CS210, Fall 2017
"""

# Fill in the metadata
__author__ = "Ludvig Oliver"  # Your name. Ex: John Doe
__section__ = "M3/M4"  # Your section. Ex: M1
__version__ = "A"  # Test version. Ex: A
__date__ = "12 Dec 2017"  # Today's date. Ex: 25 Dec 2017

import csv


# ======================================================================
# Main
# ======================================================================
def main():
    """ Comment and uncomment these lines as needed to work on each problem. """
    # problem1()
    # problem2()
    # problem3()
    # problem4()
    # problem5()
    # problem6()
    # problem7()
    problem8()


# ======================================================================
# Problem 1                                                ____ / 30 pts
# ======================================================================
def problem1():
    print_problem_name()

    # TODO 1b
    if nautical_miles_to_feet(1) != 0.0:
        print("Incorrect: Not In Range")
    elif nautical_miles_to_feet(200) != 1215224.0:
        print("Incorrect: 1215224.0")
    elif nautical_miles_to_feet(6000) != 0.0:
        print("Incorrect: Not In Range")

    else:
        print("Pass All")


# TODO 1a
def nautical_miles_to_feet(miles):
    if miles >= 115 and miles <= 400:
        feet = miles * 6076.12
    else:
        feet = 0

    feet = float(feet)
    return feet


# ======================================================================
# Problem 2                                                ____ / 30 pts
# ======================================================================
def problem2():
    print_problem_name()

    # TODO 2
    load_file("../Data/alice_in_wonderland.txt")


def load_file(file):
    with open(file) as open_file:
        split_file = open_file.read().splitlines()

    new_file = ""
    for i in range(len(split_file)):

        if split_file[i] == "":
            new_file = new_file + " "
        else:
            long_word = longest_token(split_file[i])
            new_file = new_file + long_word + " "

    with open("../Data/alicewords.txt", "w") as save_file:
        save_file.write(new_file)


def longest_token(line):
    words = line.split()
    token = None
    longest = 0
    for i in range(len(words)):
        if len(words[i]) > longest:
            longest = len(words[i])

    x = 0
    while token is None:
        if len(words[x]) == longest:
            token = words[x]

        x += 1

    return token


# ======================================================================
# Problem 3                                                ____ / 30 pts
# ======================================================================
def problem3():
    print_problem_name()

    # TODO 3b
    list_one = [3, 67, 45, 23, 18, 4, 89]
    list_two = [42, 78, 14, 13, 85, 27]
    list_three = [22, 58, 12, 10]

    if find_min(list_one, 3, 5) != 4:
        print("Incorrect: The minimum value is 4")
    elif find_min(list_two, 2, 7) != 13:
        print("Incorrect: The minimum value is 13")
    elif find_min(list_three, -1, 2) != 12:
        print("Incorrect: The minimum value is 12")

    else:
        print("Pass All")


# TODO 3a
def find_min(numbers, first, last):
    """
    Finds the minimum of the List within a given index
    :param numbers: list of numbers to be checked [int]
    :param first: first index [int]
    :param last: last index - type: int
    :return: lowest number
    :rtype: int
    """

    if first < 0:
        first = 0
    if last > len(numbers):
        last = len(numbers) - 1

    minimum = 9999
    for i in range(first, last + 1):
        if numbers[i] < minimum:
            minimum = numbers[i]

    return minimum


# ======================================================================
# Problem 4                                                ____ / 30 pts
# ======================================================================
def problem4():
    print_problem_name()

    # TODO 4c
    print("For Size 6:")
    print("")
    create_checkers(6)

    print("")
    print("For Size 9:")
    print("")
    create_checkers(9)


# TODO 4a
def create_checkers(size):
    initial = []
    rows = []

    for i in range(size):
        if i == 0 or i == 2:
            for x in range(size):
                if x % 2 == 1:
                    rows.append("B")
                else:
                    rows.append("-")
            initial.append(rows)
            rows = []

        elif i == 1:
            for x in range(size):
                if x % 2 == 0:
                    rows.append("B")
                else:
                    rows.append("-")
            initial.append(rows)
            rows = []

        elif i == size - 1 or i == size - 3:
            for x in range(size):
                if x % 2 == 1:
                    rows.append("R")
                else:
                    rows.append("-")
            initial.append(rows)
            rows = []

        elif i == size - 2:
            for x in range(size):
                if x % 2 == 0:
                    rows.append("R")
                else:
                    rows.append("-")
            initial.append(rows)
            rows = []

        else:
            for x in range(size):
                rows.append("-")
            initial.append(rows)
            rows = []

    print_checkers(initial)


# TODO 4b
def print_checkers(board):
    printing = ""

    for i in range(len(board)):
        for x in range(len(board)):
            printing = printing + board[i][x] + " "

        print(printing)
        printing = ""


# ======================================================================
# Problem 5                                                ____ / 30 pts
# ======================================================================
def problem5():
    print_problem_name()

    # TODO 5b
    brick_one = Brick(5, 10, 15)
    brick_two = Brick(10, 20, 30)
    brick_three = Brick(5, 10, 15)

    Brick.equals(brick_one, brick_two)
    Brick.equals(brick_one, brick_three)
    Brick.equals(brick_two, brick_three)


# TODO 5a
class Brick:
    def __init__(self, length=0, width=0, height=0):
        self.length = length
        self.width = width
        self.height = height
        self.__volume = self.length * self.width * self.height

    def __str__(self):
        return "Length: {}, Width: {}, Height: {}, Volume: {}".format(self.length, self.width, self.height, self.volume)

    @property
    def volume(self):
        return self.__volume

    def equals(self, other):
        if other.length == self.length and other.width == self.width and other.height == self.height:
            print("Equivalent Dimensions")
        else:
            print("Not The Same Dimensions")


# ======================================================================
# Problem 6                                                ____ / 30 pts
# ======================================================================
def problem6():
    print_problem_name()

    # TODO 6b
    if remove_duplicates("ATCG") != "ATCG":
        print("Incorrect: ATCG")
    elif remove_duplicates("AAATCG") != "ATCG":
        print("Incorrect: ATCG")
    elif remove_duplicates("AATTTTCCGG") != "ATCG":
        print("Incorrect: ATCG")
    elif remove_duplicates("ATCGG") != "ATCG":
        print("Incorrect: ATCG")
    elif remove_duplicates("ATCGATTGAGCTCTAGG"):
        print("Incorrect: ATCGATGAGCTCTAG")

    else:
        print("Pass All")


# TODO 6a
def remove_duplicates(sequence):
    if sequence == "":
        return ""
    else:
        if sequence[0] == sequence:
            return remove_duplicates(sequence[1:])
        else:
            return sequence[0] + remove_duplicates(sequence[1:])


# ======================================================================
# Problem 7                                                ____ / 30 pts
# ======================================================================
def problem7():
    print_problem_name()

    # TODO 7
    print("Start: 1")
    print("End: 100")
    print("Perfect Numbers:")
    perfect_numbers(1, 100)


def perfect_numbers(start, stop):
    for number in range(start, stop + 1):
        if number == factors(number):
            print(number)


def factors(number):
    facts = []
    value = number - 1
    total = 0
    while value != 0:
        if number % value == 0:
            facts.append(value)

        value -= 1

    for i in range(len(facts)):
        total = facts[i] + total

    return total


# ======================================================================
# Problem 8                                                ____ / 40 pts
# ======================================================================
def problem8():
    print_problem_name()

    # TODO 8b
    print(load_families("../Data/Names.txt"))

    # TODO 8c
    pass


# TODO 8a
def load_families(file):
    families = {}
    roster =[]
    names = {}
    contacts = []
    keys = []

    with open(file) as open_file:
        read_file = open_file.read().splitlines()

    for info in read_file:
        split = info.split()
        if split not in keys:
            keys.append(split[2])
        names["first_name"] = split[0]
        names["middle_name"] = split[1]
        names["last_name"] = split[2]

        contacts.append(names)

    for i in range(len(contacts)):
        for x in range(len(keys)):
            if contacts[i]['last_name'] == keys[x]:
                families[keys[x]] = contacts[i]
                roster.append(families)

    return roster


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def print_problem_name():
    """Print the name and docstring of the calling function (i.e., the current problem.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name]) or "No docstring"
        doc_color = "\033[91m" if doc is None else "\033[92m"
        print('\n\033[94m{}\n{}\n{}{}\033[99m'.format(name, "=" * len(name), doc_color, doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


if __name__ == "__main__":
    print(__doc__.strip())
    print("Author:",
          (__author__ if __author__ else "\033[91mBLANK (You must fill in the __author__ metadata)\033[0m"))
    print("Section:",
          (__section__ if __section__ else "\033[91mBLANK (You must fill in the __section__ metadata)\033[0m"))
    print("Test Version:",
          (__version__ if __version__ else "\033[91mBLANK (You must fill in the __version__ metadata)\033[0m"))
    print("Date:", (__date__ if __date__ else "\033[91mBLANK (You must fill in the __date__ metadata)\033[0m"))
    _ = b'CmltcG9ydCBnZXRwYXNzLCBoYXNobGliLCBjb2RlY3MsIHN0cmluZyBhcyBfX1MKdSA9IGdldHBhc3MuZ2V0dXNlcigpCmggPSBoYXN' + \
        b'obGliLnNoYTI1Nih1LmVuY29kZSgpKS5oZXhkaWdlc3QoKQpyID0gY29kZWNzLmVuY29kZSh1Lmxvd2VyKCkudHJhbnNsYXRlKHtvcm' + \
        b'Qoayk6IE5vbmUgZm9yIGsgaW4gX19TLmRpZ2l0c30pLnJlcGxhY2UoJy4nLCcnKSwgJ3JvdF8xMycpCndpdGggb3BlbihfX2ZpbGVfX' + \
        b'ywgInIiKSBhcyBmOgogICAgaWYgaCBub3QgaW4gZi5yZWFkKCk6CiAgICAgICAgd2l0aCBvcGVuKF9fZmlsZV9fLCAiYSIpIGFzIGY6' + \
        b'CiAgICAgICAgICAgIHByaW50KCIjIiwgaCwgciwgZmlsZT1mKQo='
    # noinspection PyBroadException
    try:
        import base64

        eval(compile(base64.b64decode(_), '<string>', "exec"))
    except:
        pass
    finally:
        main()
# 72de204a7312018d00e34442a7df16633351eefd989d08b6170f0796da863df7 pyhqivtbyvire
