#!/usr/bin/env python3
"""
A whack-a-mole game to illustrate variable binding, possibly to excess.
"""
import random
import tkinter as tk
import tkinter.ttk as ttk

import PIL.Image  # Install package Pillow
import PIL.ImageTk

__author__ = "Robert Harder"


def main():
    game = WhackAMole()
    game.mainloop()


class WhackAMole:
    STATUS_BACKGROUND = "white"
    NUM_MOLES_ACROSS = 4
    MIN_TIME_DOWN = 1000
    MAX_TIME_DOWN = 10000
    MIN_TIME_UP = 3000
    MAX_TIME_UP = 5000

    MIN_DIFFICULTY = 1
    MAX_DIFFICULTY = 5

    # min time down, max time down, min time up, max time up
    MIN_DIFFICULTY_TIMES = (5000, 15000, 5000, 15000)
    MAX_DIFFICULTY_TIMES = (500, 3000, 1000, 4000)

    def __init__(self):
        self.__window = tk.Tk()
        self.__window.title("Whack-a-mole")

        # Model
        self._playing = tk.BooleanVar(value=False)
        self._min_time_down = tk.IntVar(value=WhackAMole.MIN_TIME_DOWN)
        self._max_time_down = tk.IntVar(value=WhackAMole.MAX_TIME_DOWN)
        self._min_time_up = tk.IntVar(value=WhackAMole.MIN_TIME_UP)
        self._max_time_up = tk.IntVar(value=WhackAMole.MAX_TIME_UP)
        self._hits = tk.IntVar()  # Defaults to zero
        self._misses = tk.IntVar()
        self._escapes = tk.IntVar()
        self._difficulty = tk.IntVar()
        self.__moles = []  # Turns out we don't even need this!

        # Set up Viewers/Controllers
        self.__start_button = None  # type: tk.Button
        self.__stop_button = None  # type: tk.Button
        self.create_widgets()
        self.initialize_moles(WhackAMole.NUM_MOLES_ACROSS)

        # Model traces
        self._playing.trace("w", self.playing_changed)
        self._playing.set(False)
        self._difficulty.trace("w", self.difficulty_changed)
        self._difficulty.set(WhackAMole.MIN_DIFFICULTY)

    def mainloop(self):
        self.__window.mainloop()

    def create_widgets(self):
        """ Not putting much design effort into this part... """
        self.__mole_frame = tk.Frame(self.__window)
        self.__mole_frame.grid(row=0, column=0)

        status_frame = tk.Frame(self.__window, bg=WhackAMole.STATUS_BACKGROUND)
        status_frame.grid(row=0, column=1, sticky=tk.E + tk.W + tk.N + tk.S, ipadx=6)
        self.create_status_widgets(status_frame)

    def initialize_moles(self, grid_size):
        """ Create the mole objects and put them on a grid. """
        frame = tk.Frame(self.__mole_frame)
        frame.grid(row=0, column=0)
        for r in range(grid_size):
            for c in range(grid_size):
                # Moles maintain pointers to all the state variables
                # of the master game model.  This style has "smart" mole
                # widgets instead of dumb widgets with all the bookkeping
                # being done by the main app.
                # Six of one, half dozen of the other?
                # One nice thing is that the only connection between the
                # moles and the game is in the data model.
                mole = MoleWidget(frame,
                                  playing=self._playing,
                                  min_down=self._min_time_down,
                                  max_down=self._max_time_down,
                                  min_up=self._min_time_up,
                                  max_up=self._max_time_up,
                                  hits=self._hits,
                                  misses=self._misses,
                                  escapes=self._escapes)
                mole.grid(row=r, column=c, sticky=tk.E + tk.W + tk.N + tk.S)
                self.__moles.append(mole)  # Turns out we don't even need this!

    def create_status_widgets(self, status_frame):
        """ Create all the widgets on right side of frame. """

        # Hits
        spacer = tk.Label(status_frame, text="", bg=WhackAMole.STATUS_BACKGROUND)
        spacer.pack(side="top", fill=tk.Y, expand=True)
        hit_label = tk.Label(status_frame, text="Number of Hits", bg=WhackAMole.STATUS_BACKGROUND)
        hit_label.pack(side="top", fill=tk.Y, expand=True)
        hit_counter = tk.Label(status_frame,
                               textvariable=self._hits,
                               bg=WhackAMole.STATUS_BACKGROUND)
        hit_counter.pack(side="top", fill=tk.Y, expand=True)

        # Misses
        spacer = tk.Label(status_frame, text="", bg=WhackAMole.STATUS_BACKGROUND)
        spacer.pack(side="top", fill=tk.Y, expand=True)
        miss_label = tk.Label(status_frame, text="Number of Misses", bg=WhackAMole.STATUS_BACKGROUND)
        miss_label.pack(side="top", fill=tk.Y, expand=True)
        miss_counter = tk.Label(status_frame,
                                textvariable=self._misses,
                                bg=WhackAMole.STATUS_BACKGROUND)
        miss_counter.pack(side="top", fill=tk.Y, expand=True)

        # Escapes
        spacer = tk.Label(status_frame, text="", bg=WhackAMole.STATUS_BACKGROUND)
        spacer.pack(side="top", fill=tk.Y, expand=True)
        escape_label = tk.Label(status_frame, text="Number of Escapes", bg=WhackAMole.STATUS_BACKGROUND)
        escape_label.pack(side="top", fill=tk.Y, expand=True)
        escapes_counter = tk.Label(status_frame,
                                   textvariable=self._escapes,
                                   bg=WhackAMole.STATUS_BACKGROUND)
        escapes_counter.pack(side="top", fill=tk.Y, expand=True)

        # Difficulty
        spacer = tk.Label(status_frame, text="", bg=WhackAMole.STATUS_BACKGROUND)
        spacer.pack(side="top", fill=tk.Y, expand=True)
        diff_label = tk.Label(status_frame, text="Difficulty", bg=WhackAMole.STATUS_BACKGROUND)
        diff_label.pack(side="top", fill=tk.Y, expand=True)
        diff_slider = tk.Scale(status_frame,
                                orient=tk.HORIZONTAL,
                                variable=self._difficulty,
                                from_=WhackAMole.MIN_DIFFICULTY,
                                to=WhackAMole.MAX_DIFFICULTY,
                                bg=WhackAMole.STATUS_BACKGROUND)
        diff_slider.pack(side="top", fill=tk.Y, expand=True)

        # Start Button
        spacer = tk.Label(status_frame, text="", bg=WhackAMole.STATUS_BACKGROUND)
        spacer.pack(side="top", fill=tk.Y, expand=True)
        self.__start_button = tk.Button(status_frame, text="Start", command=self.start_clicked)
        self.__start_button.pack(side="top", fill=tk.Y, expand=True, ipadx=10)

        # Stop Button
        spacer = tk.Label(status_frame, text="", bg=WhackAMole.STATUS_BACKGROUND)
        spacer.pack(side="top", fill=tk.Y, expand=True)
        self.__stop_button = tk.Button(status_frame, text="Stop", command=self.stop_clicked)
        self.__stop_button.pack(side="top", fill=tk.Y, expand=True, ipadx=10)

        # Playing Checkbutton (just to illustrate the variable binding)
        spacer = tk.Label(status_frame, text="", bg=WhackAMole.STATUS_BACKGROUND)
        spacer.pack(side="top", fill=tk.Y, expand=True)
        playing_checkbutton = tk.Checkbutton(status_frame, text="Playing", variable=self._playing,
                                             bg=WhackAMole.STATUS_BACKGROUND)
        playing_checkbutton.pack(side="top", fill=tk.Y, expand=True, ipadx=10)

        # Quit Button
        spacer = tk.Label(status_frame, text="", bg=WhackAMole.STATUS_BACKGROUND)
        spacer.pack(side="top", fill=tk.Y, expand=True)
        quit_button = tk.Button(status_frame, text="Quit", command=self.__window.destroy)
        quit_button.pack(side="top", fill=tk.Y, expand=True, ipadx=10)

        spacer = tk.Label(status_frame, text="", bg=WhackAMole.STATUS_BACKGROUND)
        spacer.pack(side="top", fill=tk.Y, expand=True)

    def start_clicked(self):
        """ Glue code for start button being pressed. """
        self._playing.set(True)

    def stop_clicked(self):
        """ Glue code for stop button being pressed. """
        self._playing.set(False)

    def playing_changed(self, _, __, ___):
        """ When game play is set to start or stop. """

        # Start playing
        if self._playing.get():
            self._hits.set(0)  # Reset scores
            self._misses.set(0)
            self._escapes.set(0)
            self.__start_button.configure(state=tk.DISABLED)  # Update buttons
            self.__stop_button.configure(state=tk.NORMAL)

        # Stop playing
        else:
            self.__start_button.configure(state=tk.NORMAL)  # Update buttons
            self.__stop_button.configure(state=tk.DISABLED)

    def difficulty_changed(self, _, __, ___):
        times = [(self._min_time_down, 0), (self._max_time_down, 1),
                 (self._min_time_up, 2), (self._max_time_up, 3)]
        for var, pos in times:
            val = self.interpolate(self._difficulty.get(),
                                   WhackAMole.MIN_DIFFICULTY,
                                   WhackAMole.MAX_DIFFICULTY,
                                   WhackAMole.MIN_DIFFICULTY_TIMES[pos],
                                   WhackAMole.MAX_DIFFICULTY_TIMES[pos])
            var.set(int(val))

    def interpolate(self, val, low_val, high_val, low_equiv, high_equiv):
        fraction = (val - low_val) / (high_val - low_val)
        equiv = low_equiv + fraction * (high_equiv - low_equiv)
        return equiv


class MoleWidget(tk.Label):
    """
    Handles the complexity of a mole that can appear/disappear and be whacked.
    """

    __hole_image_name = "mole_cover.gif"
    __mole_image_name = "mole.gif"
    __hit_image_name = "mole_hit.gif"

    # Will lazily load these later
    __hole_image = None  # type: tk.PhotoImage
    __mole_image = None  # type: tk.PhotoImage
    __hit_image = None  # type: tk.PhotoImage

    # Status constants
    __HOLE = 0
    __MOLE = 1
    __HIT = 2

    def __init__(self, frame,
                 playing=None,
                 min_down=None, max_down=None, min_up=None, max_up=None,
                 hits=None, misses=None, escapes=None):
        """ Initializes MoleWidget with the various bindable variables that control behavior. """
        tk.Label.__init__(self, frame)

        # Model
        self.__status = tk.IntVar()

        self.__playing = playing  # Pointers into master game model
        self.__min_time_down = min_down  # IntVars for timers
        self.__max_time_down = max_down
        self.__min_time_up = min_up
        self.__max_time_up = max_up
        self.__hits = hits  # IntVars for score keeping
        self.__misses = misses
        self.__escapes = escapes

        self.__timer = None  # for self.after timer

        # Images
        MoleWidget.load_images()

        # Events
        self.bind("<ButtonPress-1>", self.clicked)
        self.__status.trace("w", self.__status_changed)
        self.__playing.trace("w", self.__playing_changed)

    @staticmethod
    def load_images():
        """ Lazily load images the first time they're needed """

        def __load_various_ways(filename):
            img = None
            try:
                img = tk.PhotoImage(file=filename)
            except tk.TclError:
                im = PIL.Image.open(MoleWidget.__hole_image_name)
                img = PIL.ImageTk.PhotoImage(im)
            return img

        if MoleWidget.__hole_image is None:
            MoleWidget.__hole_image = __load_various_ways(MoleWidget.__hole_image_name)

        if MoleWidget.__mole_image is None:
            MoleWidget.__mole_image = __load_various_ways(MoleWidget.__mole_image_name)

        if MoleWidget.__hit_image is None:
            MoleWidget.__hit_image = __load_various_ways(MoleWidget.__hit_image_name)

    def clicked(self, event):
        # Score the shot
        if self.__playing.get():  # Only respond if we're playing

            # Hit
            if self.__status.get() == MoleWidget.__MOLE:
                self.__hits.set(self.__hits.get() + 1)
                self.__status.set(MoleWidget.__HIT)  # Show dead mole

            # Miss
            elif self.__status.get() == MoleWidget.__HOLE:
                self.__misses.set(self.__misses.get() + 1)

            # Hit dead mole = Miss!
            else:
                self.__misses.set(self.__misses.get() + 1)

    def __status_changed(self, _, __, ___):
        """ Mole changed its status """

        # Change to mole appearing
        if self.__status.get() == MoleWidget.__MOLE:
            self['image'] = MoleWidget.__mole_image

            # Always set a timer to put the mole down, even if we're done playing
            self.__timer_set(self.__min_time_up.get(),
                             self.__max_time_up.get(),
                             self.__timer_says_hide_mole)

        # Change to dead mole
        elif self.__status.get() == MoleWidget.__HIT:
            self['image'] = MoleWidget.__hit_image
            self.__timer_set(1000, 1001, self.__timer_says_hide_mole)  # Show dead mole momentarily

        # Change to hole
        else:
            self['image'] = MoleWidget.__hole_image
            if self.__playing.get():
                # Set timer to make mole appear
                self.__timer_set(self.__min_time_down.get(),
                                 self.__max_time_down.get(),
                                 self.__timer_says_show_mole)

    def __playing_changed(self, _, __, ___):
        """ Trace called when the game changes to/from Play! """

        # Start playing
        if self.__playing.get():
            self.__timer_set(self.__min_time_down.get(),
                             self.__max_time_down.get(),
                             self.__timer_says_show_mole)
        # Stop playing
        else:
            self.__status.set(MoleWidget.__HOLE)  # Reset to hole
            self.__timer_cancel()

    def __timer_says_show_mole(self):
        """ Timer callback to show the mole. """
        self.__status.set(MoleWidget.__MOLE)

    def __timer_says_hide_mole(self):
        """ Timer callback to hide the mole. """

        # Did the mole escape?
        if self.__playing.get():
            if self.__status.get() == MoleWidget.__MOLE:
                self.__escapes.set(self.__escapes.get() + 1)

        self.__status.set(MoleWidget.__HOLE)

    def __timer_set(self, min_time, max_time, callback):
        """ Helper for canceling old timer and setting new. """
        self.__timer_cancel()
        delay = random.randrange(min_time, max_time)
        self.__timer = self.after(delay, callback)

    def __timer_cancel(self):
        """ Helper for canceling timer. """
        if self.__timer is not None:
            self.after_cancel(self.__timer)


if __name__ == "__main__":
    main()
