#!/usr/bin/env python3
"""Menu button demo."""

import tkinter as tk
from tkinter import ttk


def main():
    # Create the entire GUI program
    program = FrameExamplesApp()

    # Start the GUI event loop
    program.window.mainloop()


class FrameExamplesApp():
    def __init__(self):
        self.window = tk.Tk()
        self.window.title("Menu Button Example")

        mbutton = tk.Menubutton(self.window, text='Food')  # the pull-down stands alone
        picks = tk.Menu(mbutton)
        mbutton.config(menu=picks)
        picks.add_command(label='A', command=self.window.quit)
        picks.add_command(label='B', command=self.window.quit)
        picks.add_command(label='C', command=self.window.quit)
        mbutton.pack()
        mbutton.config(bg='white', bd=4, relief=tk.RAISED)

if __name__ == "__main__":
    main()
